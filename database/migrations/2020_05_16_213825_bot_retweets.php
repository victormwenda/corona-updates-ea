<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BotRetweets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitter_bot_retweets', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string("tweet_created_at");
            $table->bigInteger("tweet_id");
            $table->longText("tweet_id_str");
            $table->longText("tweet_text");

            $table->boolean("tweet_truncated")->default(false);
            $table->longText("tweet_entities");
            $table->longText("tweet_source");

            $table->bigInteger("in_reply_to_status_id")->nullable();
            $table->longText("in_reply_to_status_id_str")->nullable();
            $table->bigInteger("in_reply_to_user_id")->nullable();
            $table->longText("in_reply_to_user_id_str")->nullable();

            $table->longText("in_reply_to_screen_name")->nullable();

            $table->bigInteger("twitter_user_id");

            $table->text("twitter_geo")->nullable();
            $table->text("coordinates")->nullable();
            $table->text("place")->nullable();
            $table->text("contributors")->nullable();

            $table->text("retweeted_status")->nullable();

            $table->boolean("is_quote_status")->default(false);


            $table->bigInteger("retweet_count")->default(0);
            $table->bigInteger("favorite_count")->default(0);
            $table->boolean("favorited")->default(false);
            $table->boolean("retweeted")->default(false);
            $table->boolean("possibly_sensitive")->default(false);

            $table->text("lang")->nullable();

            $table->integer('version')->default(1);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletesTz();
            $table->timestampsTz();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitter_bot_retweets');
    }
}
