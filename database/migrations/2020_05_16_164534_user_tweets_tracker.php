<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserTweetsTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tweets_tracker', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger("twitter_user_id");
            $table->text("twitter_screen_name");
            $table->text("twitter_display_name");

            $table->bigInteger("last_tweet_id");
            $table->text("last_tweet_time");

            $table->integer('version')->default(1);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tweets_tracker');
    }
}
