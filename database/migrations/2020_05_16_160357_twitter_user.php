<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TwitterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('twitter_user', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->bigInteger("twitter_user_id");
            $table->longText("twitter_user_id_str");
            $table->text("display_name");
            $table->text("screen_name");
            $table->text("location");
            $table->text("description");
            $table->text("url")->nullable();
            $table->longText("entities");
            $table->boolean("protected")->default(false);
            $table->bigInteger("followers_count");
            $table->bigInteger("friends_count");
            $table->bigInteger("listed_count");
            $table->text("account_created_at");
            $table->bigInteger("favourites_count");
            $table->integer("utc_offset")->nullable();
            $table->text("time_zone")->nullable();
            $table->boolean("geo_enabled")->default(false);
            $table->boolean("verified")->default(false);
            $table->bigInteger("statuses_count");
            $table->text("lang")->nullable();

            $table->boolean("contributors_enabled")->default(false);
            $table->boolean("is_translator")->default(false);
            $table->boolean("is_translation_enabled")->default(false);
            $table->text("profile_background_color");
            $table->text("profile_background_image_url");
            $table->text("profile_background_image_url_https");
            $table->boolean("profile_background_tile")->default(false);
            $table->text("profile_image_url");
            $table->text("profile_image_url_https");
            $table->text("profile_banner_url")->nullable();
            $table->text("profile_link_color");
            $table->text("profile_sidebar_border_color");
            $table->text("profile_sidebar_fill_color");
            $table->text("profile_text_color");
            $table->boolean("profile_use_background_image")->default(false);
            $table->boolean("has_extended_profile")->default(false);
            $table->boolean("default_profile")->default(false);
            $table->boolean("default_profile_image")->default(false);
            $table->boolean("following")->default(false);
            $table->boolean("follow_request_sent")->default(false);
            $table->boolean("notifications")->default(false);
            $table->text("translator_type");


            $table->integer('version')->default(1);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->softDeletesTz();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitter_user');
    }
}
