<?php

namespace App\Jobs;

use App\Core\TweetMetadata;
use App\Http\Models\UserTweetsTracker;
use App\Utils\ModelUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CacheLastUserStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $tweetMetadata;

    /**
     * Create a new job instance.
     *
     * @param TweetMetadata $tweetMetadata
     */
    public function __construct(TweetMetadata $tweetMetadata)
    {
        $this->tweetMetadata = $tweetMetadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("CACHE-LAST-STATUS", array('start' => time()));
        $this->cacheUserLastStatus();
        Log::info("CACHE-LAST-STATUS", array('stop' => time()));
    }

    private function cacheUserLastStatus(): void
    {
        $twitter_user = $this->tweetMetadata->getTwitterUser();
        $user_status_tracker = UserTweetsTracker::withTrashed()
            ->where('twitter_user_id', $twitter_user->getId())->first();

        $tracker_attributes = ModelUtils::createUserStatusTrackerModelAttributes($this->tweetMetadata);

        if ($user_status_tracker == null) {
            $user_status_tracker = new UserTweetsTracker($tracker_attributes);
            $user_status_tracker->save();
        } else {

            //Update the last tweet id to the new tweet id on the status tracker to avoid fetching old tweets
            if ($user_status_tracker->last_tweet_id < $this->tweetMetadata->getId()) {
                $tracker_attributes['version'] = ++$user_status_tracker->version;
                $user_status_tracker->update($tracker_attributes);
            }
        }
    }
}
