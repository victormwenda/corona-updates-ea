<?php

namespace App\Jobs;

use App\Core\TweetMetadata;
use App\Core\TwitterObjectBuilder;
use App\Core\TwitterUtils;
use App\Http\Models\TwitterBotRetweets;
use App\Http\Models\TwitterStatus;
use App\Utils\ModelUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FilteredStatusRetweeterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $tweetMetadata;

    /**
     * Create a new job instance.
     *
     * @param TweetMetadata $tweetMetadata
     */
    public function __construct(TweetMetadata $tweetMetadata)
    {
        $this->tweetMetadata = $tweetMetadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("FILTERED-STATUS-RETWEETER", array('start' => time()));

        $retweetModel = TwitterBotRetweets::withTrashed()->where("tweet_id", $this->tweetMetadata->getId())->first();

        //Already posted this tweet
        if ($retweetModel != null) {
            CacheLastUserStatusJob::dispatch($this->tweetMetadata);
            return;
        }

        $retweet = TwitterUtils::retweet($this->tweetMetadata);

        $retweet = json_decode($retweet);

        if (!isset($retweet->errors)) {
            $retweetMetadata = TwitterObjectBuilder::decomposeTweet($retweet);
            $this->saveRetweet($retweetMetadata);
            CacheLastUserStatusJob::dispatch($retweetMetadata);
        } else {
            $this->saveRetweet($this->tweetMetadata);
        }

        CacheLastUserStatusJob::dispatch($this->tweetMetadata);

        Log::info("FILTERED-STATUS-RETWEETER", array('stop' => time()));
    }

    /**
     * Save the new user twitter status
     * @param $retweetMetadata
     */
    private function saveRetweet($retweetMetadata): void
    {
        $tweet_attributes = ModelUtils::createTwitterStatusModelAttributes($retweetMetadata);
        $twitter_status = new TwitterBotRetweets($tweet_attributes);
        $twitter_status->save();
    }

}
