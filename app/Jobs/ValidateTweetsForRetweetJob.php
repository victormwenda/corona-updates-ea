<?php

namespace App\Jobs;

use App\Core\TweetMetadata;
use App\Http\Models\RetweetTags;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ValidateTweetsForRetweetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $tweetMetadata;

    /**
     * Create a new job instance.
     *
     * @param TweetMetadata $tweetMetadata
     */
    public function __construct(TweetMetadata $tweetMetadata)
    {
        $this->tweetMetadata = $tweetMetadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("VALIDATE-TWEET", array('start' => time()));

        $search_tags = RetweetTags::withoutTrashed()->get();

        foreach ($search_tags as $tag) {
            $text = $this->tweetMetadata->getText();

            if ($tag->regex_pattern) {

                if (preg_match($tag->search_text, $text)) {
                    FilteredStatusRetweeterJob::dispatch($this->tweetMetadata);
                }


            } else {
                if ($tag->case_sensitive) {
                    //Search case sensitive
                    $position = strpos($text, $tag->search_text, 0);

                    if ($position) {
                        FilteredStatusRetweeterJob::dispatch($this->tweetMetadata);
                    }

                } else {

                    //Search case insensitive
                    $position = stripos($text, $tag->search_text, 0);

                    if ($position) {
                        FilteredStatusRetweeterJob::dispatch($this->tweetMetadata);
                    }

                }
            }

        }

        //Cache last user Tweet
        CacheLastUserStatusJob::dispatch($this->tweetMetadata);

        Log::info("VALIDATE-TWEET", array('stop' => time()));
    }
}
