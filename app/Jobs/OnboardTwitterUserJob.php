<?php

namespace App\Jobs;

use App\Core\TweetMetadata;
use App\Http\Models\TwitterStatus;
use App\Http\Models\TwitterUser;
use App\Http\Models\UserTweetsTracker;
use App\Utils\ModelUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class OnboardTwitterUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $tweetMetadata;

    /**
     * Create a new job instance.
     *
     * @param $tweetMetadata
     */
    public function __construct(TweetMetadata $tweetMetadata)
    {
        $this->tweetMetadata = $tweetMetadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("ONBOARD-USER", array('start' => time()));

        //Save Twitter User
        $this->saveNewTwitterUser();

        //Save User Status/Tweet
        $this->saveNewUserTwitterStatus();

        //Cache last user Tweet
        CacheLastUserStatusJob::dispatch($this->tweetMetadata);

        Log::info("ONBOARD-USER", array('stop' => time()));
    }

    /**
     * Save new twitter user
     */
    private function saveNewTwitterUser(): void
    {
        $twitter_user = $this->tweetMetadata->getTwitterUser();

        $twitter_user_model = TwitterUser::withTrashed()->where('twitter_user_id', $twitter_user->getId())->first();
        $user_attributes = ModelUtils::createTwitterUserModelAttributes($twitter_user);

        if ($twitter_user_model == null) {
            $twitter_user_model = new TwitterUser($user_attributes);
            $twitter_user_model->save();
        } else {
            $twitter_user_model->restore();
            $user_attributes['version'] = ++$twitter_user_model->version;
            $twitter_user_model->update($user_attributes);
        }
    }

    /**
     * Save the new user twitter status
     */
    private function saveNewUserTwitterStatus(): void
    {
        $twitter_status = TwitterStatus::withTrashed()->where("tweet_id", $this->tweetMetadata->getId())->first();
        $tweet_attributes = ModelUtils::createTwitterStatusModelAttributes($this->tweetMetadata);

        if ($twitter_status == null) {
            $twitter_status = new TwitterStatus($tweet_attributes);
            $twitter_status->save();
        } else {
            $tweet_attributes['version'] = ++$twitter_status->version;
            $twitter_status->update($tweet_attributes);
        }
    }


}
