<?php


namespace App\Core;


class TwitterHttpUtils
{
    /**
     * @param $url
     * @param $headerString
     * @return bool|string
     */
    public static function do_get($url, $headerString)
    {
        return self::do_http($url, $headerString, false);
    }

    /**
     * @param $url
     * @param $body
     * @return bool|string
     */
    public static function do_post($url, $headerString)
    {
        return self::do_http($url, $headerString, true);
    }

    /**
     * Do HTTP
     * @param $url
     * @param $headerString
     * @param bool $post
     * @param bool $verbose
     * @return bool|string
     */
    public static function do_http($url, $headerString, $post = false, $verbose = false)
    {
        $authorization = "Authorization: $headerString";
        $headers = array('Content-Type: application/x-www-form-urlencoded', $authorization);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, $post);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $request = $post ? "POST" : "GET";

        $log = "curl --request $request --url '$url'";

        foreach ($headers as $header) {
            $log .= " --header '$header' ";
        }

        $log .= " --verbose";

        if ($verbose) {
            echo $log;
        }


        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    /**
     * Generates the curl request for requesting for bearer token
     * @param string $api_key
     * @param string $api_secret
     * @return string
     */
    public static function generateBearerTokenCurlRequest(string $api_key, string $api_secret)
    {
        return "curl -u '$api_key':'$api_secret' --data 'grant_type=client_credentials' 'https://api.twitter.com/oauth2/token'";
    }
}
