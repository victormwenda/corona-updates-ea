<?php


namespace App\Core;


class TwitterUtils
{

    /**
     * Search user tweets
     * @param string $screen_name
     * @return bool|string
     */
    public static function getUserLastTweet($screen_name = "CovidUpdatesEA")
    {
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=$screen_name&count=1";

        $headers = TwitterAuth::getHeaderString($url, "GET");

        return TwitterHttpUtils::do_get($url, $headers);
    }

    /**
     * Returns user tweets since the specified tweet id
     * @param $user_id
     * @param $last_tweet
     * @return bool|string
     */
    public static function getUserTweets($user_id, $last_tweet)
    {
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json?user_id=$user_id&since_id=$last_tweet";

        $headers = TwitterAuth::getHeaderString($url, "GET");

        return TwitterHttpUtils::do_get($url, $headers);
    }

    /**
     * Create new tweet
     * @param $message
     * @return bool|string
     */
    public static function createTweet($message)
    {
        $url = "https://api.twitter.com/1.1/statuses/update.json?status=CoronaUpdatesEA&include_entities=true";
        $headers = TwitterAuth::getHeaderString($url, "POST");
        return TwitterHttpUtils::do_post($url, $headers);
    }

    /**
     * Retweet a tweet
     * @param TweetMetadata $tweetMetadata
     */
    public static function retweet(TweetMetadata $tweetMetadata)
    {
        $tweet_id = $tweetMetadata->getId();
        $url = "https://api.twitter.com/1.1/statuses/retweet/$tweet_id.json";
        $headers = TwitterAuth::getHeaderString($url, "POST");
        return TwitterHttpUtils::do_post($url, $headers);
    }
}
