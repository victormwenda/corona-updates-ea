<?php


namespace App\Core;


class IPWhitelist
{
    /**
     * Check if incoming remote address is whitelisted to visit this page
     * @return bool
     */
    public static function isWhitelisted()
    {
        $remote_ip = $_SERVER['REMOTE_ADDR'];

        return in_array($remote_ip, ['phoenix', '127.0.0.1', 'eleven.deepafrica.com', '69.16.239.18', 'pesarika.co.ke']);
    }
}
