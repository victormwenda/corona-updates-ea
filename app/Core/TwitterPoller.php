<?php


namespace App\Core;


use App\Jobs\ValidateTweetsForRetweetJob;
use App\Jobs\OnboardTwitterUserJob;

class TwitterPoller
{

    /**
     * Get last account tweet
     * @param $account
     * @return TweetMetadata
     */
    public static function getAccountLastTweet($account): TweetMetadata
    {
        $lastTweets = TwitterUtils::getUserLastTweet($account);

        $lastTweets = json_decode($lastTweets);

        $num_tweets = count($lastTweets);

        if ($num_tweets == 0) {
            mail('vmwenda.vm@gmail.com', 'Account Last Tweets', "The account [@$account] does not have any tweets");
        }

        if ($num_tweets == 1) {
            return TwitterObjectBuilder::decomposeTweet($lastTweets[0]);
        }

        return TwitterObjectBuilder::decomposeTweet($lastTweets[0]);
    }

    public static function pollRecentTweets($trackedUser)
    {
        $user_id = $trackedUser->twitter_user_id;
        $last_tweet = $trackedUser->last_tweet_id;

        $user_tweets = TwitterUtils::getUserTweets($user_id, $last_tweet);

        $user_tweets = json_decode($user_tweets);

        foreach ($user_tweets as $user_tweet) {
            $tweetMetaData = TwitterObjectBuilder::decomposeTweet($user_tweet);
            ValidateTweetsForRetweetJob::dispatch($tweetMetaData);
        }

    }

    public static function onboard($screen_name)
    {
        $lastTweet = self::getAccountLastTweet($screen_name);

        OnboardTwitterUserJob::dispatch($lastTweet);

    }
}
