<?php


namespace App\Core;


class TwitterObjectBuilder
{

    /**
     * Decompose decoded user tweet or status
     * @param $tweet_json
     * @return TweetMetadata
     */
    public static function decomposeTweet($tweet_json): TweetMetadata
    {
        $tweet = $tweet_json;

        $created_at = $tweet->created_at;
        $id = $tweet->id;
        $id_str = $tweet->id_str;
        $text = $tweet->text;
        $truncated = $tweet->truncated;
        $entities = json_encode($tweet->entities);
        $source = $tweet->source;
        $in_reply_to_status_id = $tweet->in_reply_to_status_id;
        $in_reply_to_status_id_str = $tweet->in_reply_to_status_id_str;
        $in_reply_to_user_id = $tweet->in_reply_to_user_id;
        $in_reply_to_user_id_str = $tweet->in_reply_to_user_id_str;
        $in_reply_to_screen_name = $tweet->in_reply_to_screen_name;
        $twitter_user = self::decomposeTwitterUser($tweet->user);
        $geo = $tweet->geo;
        $coordinates = $tweet->coordinates;
        $place = $tweet->place;
        $contributors = $tweet->contributors;
        $retweeted_status = isset($tweet->retweeted_status) ? self::decomposeTweet($tweet->retweeted_status) : null;
        $is_quote_status = $tweet->is_quote_status;
        $retweet_count = $tweet->retweet_count;
        $favorite_count = $tweet->favorite_count;
        $favorited = $tweet->favorited;
        $retweeted = $tweet->retweeted;
        $possibly_sensitive = isset($tweet->possibly_sensitive) ? $tweet->possibly_sensitive : false;
        $lang = $tweet->lang;

        return new TweetMetadata($created_at, $id, $id_str, $text, $truncated, $entities, $source, $in_reply_to_status_id,
            $in_reply_to_status_id_str, $in_reply_to_user_id, $in_reply_to_user_id_str,
            $in_reply_to_screen_name, $twitter_user, $geo, $coordinates, $place, $contributors,
            $retweeted_status, $is_quote_status, $retweet_count, $favorite_count, $favorited,
            $retweeted, $possibly_sensitive, $lang);
    }

    /**
     * Decompose Twitter user decoded json object
     * @param $user_json
     * @return TwitterUserMetadata
     */
    public static function decomposeTwitterUser($user_json): TwitterUserMetadata
    {
        $user = $user_json;

        $id = $user->id;
        $id_str = $user->id_str;
        $name = $user->name;
        $screen_name = $user->screen_name;
        $location = $user->location;
        $description = $user->description;
        $url = $user->url;
        $entities = json_encode($user->entities);
        $protected = $user->protected;
        $followers_count = $user->followers_count;
        $friends_count = $user->friends_count;
        $listed_count = $user->listed_count;
        $created_at = $user->created_at;
        $favourites_count = $user->favourites_count;
        $utc_offset = $user->utc_offset;
        $time_zone = $user->time_zone;
        $geo_enabled = $user->geo_enabled;
        $verified = $user->verified;
        $statuses_count = $user->statuses_count;
        $lang = $user->lang;
        $contributors_enabled = $user->contributors_enabled;
        $is_translator = $user->is_translator;
        $is_translation_enabled = $user->is_translation_enabled;
        $profile_background_color = $user->profile_background_color;
        $profile_background_image_url = $user->profile_background_image_url;
        $profile_background_image_url_https = $user->profile_background_image_url_https;
        $profile_background_tile = $user->profile_background_tile;
        $profile_image_url = $user->profile_image_url;
        $profile_image_url_https = $user->profile_image_url_https;
        $profile_banner_url = isset($user->profile_banner_url) ? $user->profile_banner_url : null;
        $profile_link_color = $user->profile_link_color;
        $profile_sidebar_border_color = $user->profile_sidebar_border_color;
        $profile_sidebar_fill_color = $user->profile_sidebar_fill_color;
        $profile_text_color = $user->profile_text_color;
        $profile_use_background_image = $user->profile_use_background_image;
        $has_extended_profile = $user->has_extended_profile;
        $default_profile = $user->default_profile;
        $default_profile_image = $user->default_profile_image;
        $following = $user->following;
        $follow_request_sent = $user->follow_request_sent;
        $notifications = $user->notifications;
        $translator_type = $user->translator_type;

        return new TwitterUserMetadata($id, $id_str, $name, $screen_name, $location, $description, $url, $entities, $protected,
            $followers_count, $friends_count, $listed_count, $created_at, $favourites_count, $utc_offset,
            $time_zone, $geo_enabled, $verified, $statuses_count, $lang, $contributors_enabled, $is_translator,
            $is_translation_enabled, $profile_background_color, $profile_background_image_url,
            $profile_background_image_url_https, $profile_background_tile, $profile_image_url,
            $profile_image_url_https, $profile_banner_url, $profile_link_color,
            $profile_sidebar_border_color, $profile_sidebar_fill_color, $profile_text_color,
            $profile_use_background_image, $has_extended_profile, $default_profile,
            $default_profile_image, $following, $follow_request_sent, $notifications, $translator_type);
    }
}
