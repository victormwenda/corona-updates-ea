<?php


namespace App\Core;


class HttpUtils
{

    /**
     * Do HTTP Operation
     *
     * @param $url
     * @param $headers
     * @param bool $post
     * @param array $postvalues
     * @param bool $verbose
     * @param bool $dry_run [when true, does not execute request but return the curl to be executed]
     * @return bool|string
     */
    public static function do_http($url, $headers, $post = false, $postvalues = array(), $verbose = true, $dry_run = false)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, $post);

        if ($post) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postvalues));
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $request = $post ? "POST" : "GET";

        $log = "curl --request $request --url '$url'";

        foreach ($headers as $header) {
            $log .= " --header '$header' ";
        }

        $log .= " --verbose";

        if ($verbose) {
            echo $log;
        }


        $response = $dry_run ? $log : curl_exec($curl);
        curl_close($curl);

        return $response;
    }
}
