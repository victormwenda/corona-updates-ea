<?php


namespace App\Core;


class TwitterAuth
{
    /**
     * @return array|false|string
     */
    private static function getConsumerKey()
    {
        return $oauth_consumer_key = getenv('TWITTER_CONSUMER_KEY');
    }

    /**
     * Get the consumer secret
     * @return array|false|string
     */
    private static function getConsumerSecret()
    {
        return $oauth_consumer_secret = getenv('TWITTER_CONSUMER_SECRET');
    }

    /**
     * @return array|false|string
     */
    private static function getAccessToken()
    {
        return $oauth_token = getenv('TWITTER_ACCESS_TOKEN');
    }

    /**
     * Get the token secret
     * @return array|false|string
     */
    private static function getTokenSecret()
    {
        return $oauth_token_secret = getenv('TWITTER_TOKEN_SECRET');
    }

    /**
     * Get base headers
     * @return array
     */
    private static function getBaseHeaderParams()
    {
        $oauth_consumer_key = self::getConsumerKey();
        $oauth_token = self::getAccessToken();

        $oauth_timestamp = self::generateTimestamp();
        $oauth_signature_method = "HMAC-SHA1";
        $oauth_nonce = self::generateNonce();
        $oauth_version = "1.0";

        return [
            'oauth_consumer_key' => $oauth_consumer_key,
            'oauth_nonce' => $oauth_nonce,
            'oauth_signature_method' => $oauth_signature_method,
            'oauth_timestamp' => $oauth_timestamp,
            'oauth_token' => $oauth_token,
            'oauth_version' => $oauth_version,
        ];
    }

    /**
     * Create twitter headers
     * @param $url
     * @param $method
     * @return array
     */
    private static function getHeaders($url, $method)
    {

        $headers = self::getBaseHeaderParams();

        $headers['oauth_signature'] = self::createSignature($url, $method, $headers);

        ksort($headers);

        return $headers;
    }

    /**
     * Create signature
     * @param $endpoint
     * @param $method
     * @param $headers
     * @return bool|string
     */
    private static function createSignature($endpoint, $method, $headers)
    {
        //get the base twitter url
        $base_url = self::getRequestURL($endpoint);

        //get all the request uri parameters
        $url_params = self::getRequestParameters($endpoint);

        //create array of request parameters
        $base_request_params = self::toArrayRequestParameters($url_params);

        //create base headers for twitter API
        $base_headers = $headers;

        $twitter_params = array_merge($base_headers, $base_request_params);


        //Percent encode every key and value that will be signed.
        $percented_twitter_params = array();

        foreach ($twitter_params as $key => $value) {

            $perc_enc_key = rawurlencode($key);
            $perc_enc_value = rawurlencode($value);

            $percented_twitter_params[$perc_enc_key] = $perc_enc_value;
        }


        //Sort the list of parameters alphabetically [1] by encoded key [2].
        asort($percented_twitter_params);
        ksort($percented_twitter_params);

        //For each key/value pair:

        $collected_parameter_string = "";
        $index = 0;
        $array_size = count($percented_twitter_params);

        foreach ($percented_twitter_params as $key => $value) {

            //Append the encoded key to the output string.
            $collected_parameter_string .= $key;

            //Append the ‘=’ character to the output string.
            $collected_parameter_string .= "=";

            //Append the encoded value to the output string.
            $collected_parameter_string .= $value;

            if ($index < $array_size - 1) {
                //If there are more key/value pairs remaining, append a ‘&’ character to the output string.
                $collected_parameter_string .= "&";
            }

            $index++;
        }

        //Creating the signature base string
        $signature_base_string = "";

        //Convert the HTTP Method to uppercase and set the output string equal to this value.
        $method = strtoupper($method);
        $signature_base_string = $method;


        //Append the ‘&’ character to the output string.
        $signature_base_string .= "&";

        //Percent encode the URL and append it to the output string.
        $percent_enc_url = rawurlencode($base_url);
        $signature_base_string .= $percent_enc_url;

        //Append the ‘&’ character to the output string.
        $signature_base_string .= "&";

        //Percent encode the parameter string and append it to the output string.
        $percent_enc_parameter_string = rawurlencode($collected_parameter_string);
        $signature_base_string .= $percent_enc_parameter_string;


        //Signing key
        $signing_key = rawurlencode(self::getConsumerSecret()) . "&" . rawurlencode(self::getTokenSecret());


        //signature is calculated by passing the signature base string and signing key to the HMAC-SHA1
        $sig_hash_hmac_raw = hash_hmac("sha1", $signature_base_string, $signing_key, true);


        //The output of the HMAC signing function is a binary string. This needs to be base64 encoded to produce the signature string.
        $signature = base64_encode($sig_hash_hmac_raw);

        return $signature;
    }


    public static function getHeaderString($url, $method)
    {
        //Append the string “OAuth ” (including the space at the end) to DST.
        $auth = "OAuth ";

        $headers = self::getHeaders($url, $method);

        $headerSize = count($headers); //default value is 7
        assert($headerSize == 7, "Your headers are not in shape. Size is : $headerSize");

        $index = 0;

        //For each key/value pair of the 7 parameters listed above
        foreach ($headers as $key => $value) {
            //Percent encode the key and append it to DST.
            $perc_enc_key = rawurlencode($key);
            $auth .= $perc_enc_key;

            //Append the equals character ‘=’ to DST.
            $auth .= "=";

            //Append a double quote ‘”’ to DST.
            $auth .= '"';

            //Percent encode the value and append it to DST.
            $perc_enc_value = rawurlencode($value);
            $auth .= $perc_enc_value;

            //Append a double quote ‘”’ to DST.
            $auth .= '"';

            //If there are key/value pairs remaining, append a comma ‘,’ and a space ‘ ‘ to DST.
            if ($index < $headerSize - 1) {
                $auth .= ", ";
            }

            $index++;
        }

        return $auth;
    }


    /**
     * Get request URL
     * @param $endpoint
     * @return mixed|string
     */
    private static function getRequestURL($endpoint)
    {
        if (!strpos($endpoint, "?")) {
            return $endpoint;
        }
        return explode("?", $endpoint)[0];
    }

    /**
     * Get request params
     * @param $endpoint
     * @return mixed|string
     */
    private static function getRequestParameters($endpoint)
    {
        if (!strpos($endpoint, "?")) {
            return null;
        }
        return explode("?", $endpoint)[1];
    }

    /**
     * Returns an array of all the request parameters as a array[key]=value
     * @param $request_parameters
     * @return array
     */
    private static function toArrayRequestParameters($request_parameters)
    {
        if ($request_parameters == null) {
            return array();
        }

        $assoc_params = array();

        $segments = explode("&", $request_parameters);

        foreach ($segments as $segment) {
            $parts = explode("=", $segment);
            $key = $parts[0];
            $value = $parts[1];
            $assoc_params[$key] = $value;
        }
        return $assoc_params;
    }

    /**
     * Create basic authorization string
     *  base64encoded ("$api_key:$api_secret")
     * @return string
     */
    private static function createBasicAuthorizationString(): string
    {
        $api_key = self::getConsumerKey();
        $api_secret = self::getConsumerSecret();

        $enc_api_key = urlencode($api_key);
        $enc_api_secret = urlencode($api_secret);

        $auth_str = "$enc_api_key:$enc_api_secret";
        $enc_auth_str = base64_encode($auth_str);

        return $enc_auth_str;
    }

    /**
     * Create base64encoded ("email:password")
     * @param $email
     * @param $password
     * @return string
     */
    private static function createBasicAuthenticationString($email, $password): string
    {
        return base64_encode("$email:$password");
    }

    /**
     * Generate a nonce
     * @return string
     */
    private static function generateNonce(): ?string
    {
        $nonce = mt_rand();
        return str_shuffle($nonce);
    }

    /**
     * Get current timestamp
     * @return int
     */
    private static function generateTimestamp(): int
    {
        return time();
    }

    /**
     * Requesat token
     * @param $callback_url
     */
    private static function requestToken($callback_url)
    {
        $request_token_url = 'https://api.twitter.com/oauth/request_token';
        $oauth_callback = $callback_url;
        $request_token_url .= "?x_auth_access_type=write&oauth_callback=$oauth_callback";

        $authorization = self::getHeaderString($request_token_url, "POST");

        $headers = array('Authorization: ' . $authorization, 'Content-Type: application/x-www-form-urlencoded');
        $response = HttpUtils::do_http($request_token_url, $headers, true, array(), false, false);

        var_dump($response);
    }

    /**
     * Authenticate URL
     * @param $callback_url
     * @return bool|string
     */
    private static function authenticateURL($callback_url)
    {
        $authenticate_url = 'https://api.twitter.com/oauth/authenticate';

        $oauth_callback = $callback_url;
        $oauth_consumer_key = self::getConsumerKey();

        $authenticate_url .= "?oauth_callback=$oauth_callback&oauth_consumer_key=$oauth_consumer_key";
        $headerString = self::getHeaderString($authenticate_url, "POST");
        return $response = TwitterHttpUtils::do_post($authenticate_url, $headerString);
    }

    /**
     * Authorize URL
     * @return bool|string
     */
    public static function authorizeURL()
    {
        $callback_url = urlencode("https://pesarika.co.ke/");
        $request_token_url = 'https://api.twitter.com/oauth/request_token';
        $access_token_url = 'https://api.twitter.com/oauth/access_token';
        $authorize_url = 'https://api.twitter.com/oauth/authorize';

        $oauth_consumer_key = self::getConsumerKey();
        $oauth_consumer_secret = self::getConsumerSecret();
        $oauth_callback = $callback_url;

        //$postfields = array("oauth_callback" => $oauth_callback, "oauth_consumer_key" => $oauth_consumer_key);
        //$response = HttpUtils::do_http($request_token_url, array('Content-Type: x-www-url-encoded'), true, $postfields, true);

        $request_token_url .= "?oauth_callback=$authorize_url";
        $headerString = self::getHeaderString($request_token_url, "POST");
        return $response = TwitterHttpUtils::do_post($request_token_url, $headerString);
    }

    /**
     * Verify authorization
     */
    public static function verifyAuthorization()
    {

    }

    /**
     * Returns a curl generator command for bearer token
     * @return string
     */
    public static function getBearerTokenCurlGenerator()
    {
        $api_key = self::getConsumerKey();
        $api_secret = self::getConsumerSecret();

        return TwitterHttpUtils::generateBearerTokenCurlRequest($api_key, $api_secret);
    }

    /**
     * Generate bearer token
     * @return bool|string
     */
    public static function generateBearerToken()
    {
        $url = "https://api.twitter.com/oauth2/token";
        $basic_auth_str = self::createBasicAuthorizationString();

        $headers = array("Authorization: Basic $basic_auth_str", "Content-Type: application/x-www-form-urlencoded;charset=UTF-8");
        $post_fields = array("grant_type" => "client_credentials");

        return $response = HttpUtils::do_http($url, $headers, true, $post_fields, false, false);
    }

    /**
     * Invalidate a token
     * @param $token
     * @return bool|string
     */
    public static function invalidateBearerToken($token)
    {
        $url = "https://api.twitter.com/oauth2/invalidate_token";

        $basic_auth_str = self::createBasicAuthorizationString();


        $headers = array("Authorization: Basic $basic_auth_str", "Content-Type: application/x-www-form-urlencoded;charset=UTF-8");
        $post_fields = array("access_token" => $token);

        $response = HttpUtils::do_http($url, $headers, true, $post_fields, false, false);

        return $response;
    }

    /**
     * Twitter basic authentication
     * Used by Enterprise APIs
     * @param string $email
     * @param string $password
     * @return bool|string
     */
    public static function basicAuthentication(string $email, string $password)
    {
        $url = "https://gnip-api.twitter.com";

        $basic_auth_str = self::createBasicAuthenticationString($email, $password);

        $headers = array("Authentication: Basic $basic_auth_str", "Content-Type: application/x-www-form-urlencoded;charset=UTF-8");

        $post_fields = array();

        $response = HttpUtils::do_http($url, $headers, true, $post_fields, false, false);

        return $response;
    }

    /**
     * Pin Based Oauth
     */
    public static function pinBasedOauth()
    {
    }
}
