<?php


namespace App\Core;


class TweetMetadata
{

    private $created_at;
    private $id;
    private $id_str;
    private $text;
    private $truncated;
    private $entities;
    private $source;
    private $in_reply_to_status_id;
    private $in_reply_to_status_id_str;
    private $in_reply_to_user_id;
    private $in_reply_to_user_id_str;
    private $in_reply_to_screen_name;
    private $twitter_user;
    private $geo;
    private $coordinates;
    private $place;
    private $contributors;
    private $retweeted_status;
    private $is_quote_status;
    private $retweet_count;
    private $favorite_count;
    private $favorited;
    private $retweeted;
    private $possibly_sensitive;
    private $lang;

    /**
     * TweetMetadata constructor.
     * @param $created_at
     * @param $id
     * @param $id_str
     * @param $text
     * @param $truncated
     * @param $entities
     * @param $source
     * @param $in_reply_to_status_id
     * @param $in_reply_to_status_id_str
     * @param $in_reply_to_user_id
     * @param $in_reply_to_user_id_str
     * @param $in_reply_to_screen_name
     * @param $twitter_user
     * @param $geo
     * @param $coordinates
     * @param $place
     * @param $contributors
     * @param $retweeted_status
     * @param $is_quote_status
     * @param $retweet_count
     * @param $favorite_count
     * @param $favorited
     * @param $retweeted
     * @param $possibly_sensitive
     * @param $lang
     */
    public function __construct($created_at, $id, $id_str, $text, $truncated, $entities, $source, $in_reply_to_status_id,
                                $in_reply_to_status_id_str, $in_reply_to_user_id, $in_reply_to_user_id_str,
                                $in_reply_to_screen_name, $twitter_user, $geo, $coordinates, $place, $contributors,
                                $retweeted_status, $is_quote_status, $retweet_count, $favorite_count, $favorited,
                                $retweeted, $possibly_sensitive, $lang)
    {
        $this->created_at = $created_at;
        $this->id = $id;
        $this->id_str = $id_str;
        $this->text = $text;
        $this->truncated = $truncated;
        $this->entities = $entities;
        $this->source = $source;
        $this->in_reply_to_status_id = $in_reply_to_status_id;
        $this->in_reply_to_status_id_str = $in_reply_to_status_id_str;
        $this->in_reply_to_user_id = $in_reply_to_user_id;
        $this->in_reply_to_user_id_str = $in_reply_to_user_id_str;
        $this->in_reply_to_screen_name = $in_reply_to_screen_name;
        $this->twitter_user = $twitter_user;
        $this->geo = $geo;
        $this->coordinates = $coordinates;
        $this->place = $place;
        $this->contributors = $contributors;
        $this->retweeted_status = $retweeted_status;
        $this->is_quote_status = $is_quote_status;
        $this->retweet_count = $retweet_count;
        $this->favorite_count = $favorite_count;
        $this->favorited = $favorited;
        $this->retweeted = $retweeted;
        $this->possibly_sensitive = $possibly_sensitive;
        $this->lang = $lang;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return TweetMetadata
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TweetMetadata
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdStr()
    {
        return $this->id_str;
    }

    /**
     * @param mixed $id_str
     * @return TweetMetadata
     */
    public function setIdStr($id_str)
    {
        $this->id_str = $id_str;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return TweetMetadata
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTruncated()
    {
        return $this->truncated;
    }

    /**
     * @param mixed $truncated
     * @return TweetMetadata
     */
    public function setTruncated($truncated)
    {
        $this->truncated = $truncated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * @param mixed $entities
     * @return TweetMetadata
     */
    public function setEntities($entities)
    {
        $this->entities = $entities;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param mixed $source
     * @return TweetMetadata
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInReplyToStatusId()
    {
        return $this->in_reply_to_status_id;
    }

    /**
     * @param mixed $in_reply_to_status_id
     * @return TweetMetadata
     */
    public function setInReplyToStatusId($in_reply_to_status_id)
    {
        $this->in_reply_to_status_id = $in_reply_to_status_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInReplyToStatusIdStr()
    {
        return $this->in_reply_to_status_id_str;
    }

    /**
     * @param mixed $in_reply_to_status_id_str
     * @return TweetMetadata
     */
    public function setInReplyToStatusIdStr($in_reply_to_status_id_str)
    {
        $this->in_reply_to_status_id_str = $in_reply_to_status_id_str;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInReplyToUserId()
    {
        return $this->in_reply_to_user_id;
    }

    /**
     * @param mixed $in_reply_to_user_id
     * @return TweetMetadata
     */
    public function setInReplyToUserId($in_reply_to_user_id)
    {
        $this->in_reply_to_user_id = $in_reply_to_user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInReplyToUserIdStr()
    {
        return $this->in_reply_to_user_id_str;
    }

    /**
     * @param mixed $in_reply_to_user_id_str
     * @return TweetMetadata
     */
    public function setInReplyToUserIdStr($in_reply_to_user_id_str)
    {
        $this->in_reply_to_user_id_str = $in_reply_to_user_id_str;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInReplyToScreenName()
    {
        return $this->in_reply_to_screen_name;
    }

    /**
     * @param mixed $in_reply_to_screen_name
     * @return TweetMetadata
     */
    public function setInReplyToScreenName($in_reply_to_screen_name)
    {
        $this->in_reply_to_screen_name = $in_reply_to_screen_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwitterUser(): TwitterUserMetadata
    {
        return $this->twitter_user;
    }

    /**
     * @param mixed $twitter_user
     * @return TweetMetadata
     */
    public function setTwitterUser($twitter_user)
    {
        $this->twitter_user = $twitter_user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGeo()
    {
        return $this->geo;
    }

    /**
     * @param mixed $geo
     * @return TweetMetadata
     */
    public function setGeo($geo)
    {
        $this->geo = $geo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param mixed $coordinates
     * @return TweetMetadata
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     * @return TweetMetadata
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContributors()
    {
        return $this->contributors;
    }

    /**
     * @param mixed $contributors
     * @return TweetMetadata
     */
    public function setContributors($contributors)
    {
        $this->contributors = $contributors;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetweetedStatus(): ?TweetMetadata
    {
        return $this->retweeted_status;
    }

    /**
     * @param mixed $retweeted_status
     * @return TweetMetadata
     */
    public function setRetweetedStatus($retweeted_status)
    {
        $this->retweeted_status = $retweeted_status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsQuoteStatus()
    {
        return $this->is_quote_status;
    }

    /**
     * @param mixed $is_quote_status
     * @return TweetMetadata
     */
    public function setIsQuoteStatus($is_quote_status)
    {
        $this->is_quote_status = $is_quote_status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetweetCount()
    {
        return $this->retweet_count;
    }

    /**
     * @param mixed $retweet_count
     * @return TweetMetadata
     */
    public function setRetweetCount($retweet_count)
    {
        $this->retweet_count = $retweet_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavoriteCount()
    {
        return $this->favorite_count;
    }

    /**
     * @param mixed $favorite_count
     * @return TweetMetadata
     */
    public function setFavoriteCount($favorite_count)
    {
        $this->favorite_count = $favorite_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavorited()
    {
        return $this->favorited;
    }

    /**
     * @param mixed $favorited
     * @return TweetMetadata
     */
    public function setFavorited($favorited)
    {
        $this->favorited = $favorited;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetweeted()
    {
        return $this->retweeted;
    }

    /**
     * @param mixed $retweeted
     * @return TweetMetadata
     */
    public function setRetweeted($retweeted)
    {
        $this->retweeted = $retweeted;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPossiblySensitive()
    {
        return $this->possibly_sensitive;
    }

    /**
     * @param mixed $possibly_sensitive
     * @return TweetMetadata
     */
    public function setPossiblySensitive($possibly_sensitive)
    {
        $this->possibly_sensitive = $possibly_sensitive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     * @return TweetMetadata
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

}
