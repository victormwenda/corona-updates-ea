<?php


namespace App\Core;


class TwitterUserMetadata
{
    private $id;
    private $id_str;
    private $name;
    private $screen_name;
    private $location;
    private $description;
    private $url;
    private $entities;
    private $protected;
    private $followers_count;
    private $friends_count;
    private $listed_count;
    private $created_at;
    private $favourites_count;
    private $utc_offset;
    private $time_zone;
    private $geo_enabled;
    private $verified;
    private $statuses_count;
    private $lang;
    private $contributors_enabled;
    private $is_translator;
    private $is_translation_enabled;
    private $profile_background_color;
    private $profile_background_image_url;
    private $profile_background_image_url_https;
    private $profile_background_tile;
    private $profile_image_url;
    private $profile_image_url_https;
    private $profile_banner_url;
    private $profile_link_color;
    private $profile_sidebar_border_color;
    private $profile_sidebar_fill_color;
    private $profile_text_color;
    private $profile_use_background_image;
    private $has_extended_profile;
    private $default_profile;
    private $default_profile_image;
    private $following;
    private $follow_request_sent;
    private $notifications;
    private $translator_type;

    /**
     * TwitterUserMetadata constructor.
     * @param $id
     * @param $id_str
     * @param $name
     * @param $screen_name
     * @param $location
     * @param $description
     * @param $url
     * @param $entities
     * @param $protected
     * @param $followers_count
     * @param $friends_count
     * @param $listed_count
     * @param $created_at
     * @param $favourites_count
     * @param $utc_offset
     * @param $time_zone
     * @param $geo_enabled
     * @param $verified
     * @param $statuses_count
     * @param $lang
     * @param $contributors_enabled
     * @param $is_translator
     * @param $is_translation_enabled
     * @param $profile_background_color
     * @param $profile_background_image_url
     * @param $profile_background_image_url_https
     * @param $profile_background_tile
     * @param $profile_image_url
     * @param $profile_image_url_https
     * @param $profile_banner_url
     * @param $profile_link_color
     * @param $profile_sidebar_border_color
     * @param $profile_sidebar_fill_color
     * @param $profile_text_color
     * @param $profile_use_background_image
     * @param $has_extended_profile
     * @param $default_profile
     * @param $default_profile_image
     * @param $following
     * @param $follow_request_sent
     * @param $notifications
     * @param $translator_type
     */
    public function __construct($id, $id_str, $name, $screen_name, $location, $description, $url, $entities, $protected,
                                $followers_count, $friends_count, $listed_count, $created_at, $favourites_count, $utc_offset,
                                $time_zone, $geo_enabled, $verified, $statuses_count, $lang, $contributors_enabled, $is_translator,
                                $is_translation_enabled, $profile_background_color, $profile_background_image_url,
                                $profile_background_image_url_https, $profile_background_tile, $profile_image_url,
                                $profile_image_url_https, $profile_banner_url, $profile_link_color,
                                $profile_sidebar_border_color, $profile_sidebar_fill_color, $profile_text_color,
                                $profile_use_background_image, $has_extended_profile, $default_profile,
                                $default_profile_image, $following, $follow_request_sent, $notifications, $translator_type)
    {
        $this->id = $id;
        $this->id_str = $id_str;
        $this->name = $name;
        $this->screen_name = $screen_name;
        $this->location = $location;
        $this->description = $description;
        $this->url = $url;
        $this->entities = $entities;
        $this->protected = $protected;
        $this->followers_count = $followers_count;
        $this->friends_count = $friends_count;
        $this->listed_count = $listed_count;
        $this->created_at = $created_at;
        $this->favourites_count = $favourites_count;
        $this->utc_offset = $utc_offset;
        $this->time_zone = $time_zone;
        $this->geo_enabled = $geo_enabled;
        $this->verified = $verified;
        $this->statuses_count = $statuses_count;
        $this->lang = $lang;
        $this->contributors_enabled = $contributors_enabled;
        $this->is_translator = $is_translator;
        $this->is_translation_enabled = $is_translation_enabled;
        $this->profile_background_color = $profile_background_color;
        $this->profile_background_image_url = $profile_background_image_url;
        $this->profile_background_image_url_https = $profile_background_image_url_https;
        $this->profile_background_tile = $profile_background_tile;
        $this->profile_image_url = $profile_image_url;
        $this->profile_image_url_https = $profile_image_url_https;
        $this->profile_banner_url = $profile_banner_url;
        $this->profile_link_color = $profile_link_color;
        $this->profile_sidebar_border_color = $profile_sidebar_border_color;
        $this->profile_sidebar_fill_color = $profile_sidebar_fill_color;
        $this->profile_text_color = $profile_text_color;
        $this->profile_use_background_image = $profile_use_background_image;
        $this->has_extended_profile = $has_extended_profile;
        $this->default_profile = $default_profile;
        $this->default_profile_image = $default_profile_image;
        $this->following = $following;
        $this->follow_request_sent = $follow_request_sent;
        $this->notifications = $notifications;
        $this->translator_type = $translator_type;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TwitterUserMetadata
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdStr()
    {
        return $this->id_str;
    }

    /**
     * @param mixed $id_str
     * @return TwitterUserMetadata
     */
    public function setIdStr($id_str)
    {
        $this->id_str = $id_str;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return TwitterUserMetadata
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getScreenName()
    {
        return $this->screen_name;
    }

    /**
     * @param mixed $screen_name
     * @return TwitterUserMetadata
     */
    public function setScreenName($screen_name)
    {
        $this->screen_name = $screen_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return TwitterUserMetadata
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return TwitterUserMetadata
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return TwitterUserMetadata
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * @param mixed $entities
     * @return TwitterUserMetadata
     */
    public function setEntities($entities)
    {
        $this->entities = $entities;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProtected()
    {
        return $this->protected;
    }

    /**
     * @param mixed $protected
     * @return TwitterUserMetadata
     */
    public function setProtected($protected)
    {
        $this->protected = $protected;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowersCount()
    {
        return $this->followers_count;
    }

    /**
     * @param mixed $followers_count
     * @return TwitterUserMetadata
     */
    public function setFollowersCount($followers_count)
    {
        $this->followers_count = $followers_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFriendsCount()
    {
        return $this->friends_count;
    }

    /**
     * @param mixed $friends_count
     * @return TwitterUserMetadata
     */
    public function setFriendsCount($friends_count)
    {
        $this->friends_count = $friends_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getListedCount()
    {
        return $this->listed_count;
    }

    /**
     * @param mixed $listed_count
     * @return TwitterUserMetadata
     */
    public function setListedCount($listed_count)
    {
        $this->listed_count = $listed_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     * @return TwitterUserMetadata
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavouritesCount()
    {
        return $this->favourites_count;
    }

    /**
     * @param mixed $favourites_count
     * @return TwitterUserMetadata
     */
    public function setFavouritesCount($favourites_count)
    {
        $this->favourites_count = $favourites_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUtcOffset()
    {
        return $this->utc_offset;
    }

    /**
     * @param mixed $utc_offset
     * @return TwitterUserMetadata
     */
    public function setUtcOffset($utc_offset)
    {
        $this->utc_offset = $utc_offset;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeZone()
    {
        return $this->time_zone;
    }

    /**
     * @param mixed $time_zone
     * @return TwitterUserMetadata
     */
    public function setTimeZone($time_zone)
    {
        $this->time_zone = $time_zone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGeoEnabled()
    {
        return $this->geo_enabled;
    }

    /**
     * @param mixed $geo_enabled
     * @return TwitterUserMetadata
     */
    public function setGeoEnabled($geo_enabled)
    {
        $this->geo_enabled = $geo_enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * @param mixed $verified
     * @return TwitterUserMetadata
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusesCount()
    {
        return $this->statuses_count;
    }

    /**
     * @param mixed $statuses_count
     * @return TwitterUserMetadata
     */
    public function setStatusesCount($statuses_count)
    {
        $this->statuses_count = $statuses_count;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     * @return TwitterUserMetadata
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContributorsEnabled()
    {
        return $this->contributors_enabled;
    }

    /**
     * @param mixed $contributors_enabled
     * @return TwitterUserMetadata
     */
    public function setContributorsEnabled($contributors_enabled)
    {
        $this->contributors_enabled = $contributors_enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTranslator()
    {
        return $this->is_translator;
    }

    /**
     * @param mixed $is_translator
     * @return TwitterUserMetadata
     */
    public function setIsTranslator($is_translator)
    {
        $this->is_translator = $is_translator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsTranslationEnabled()
    {
        return $this->is_translation_enabled;
    }

    /**
     * @param mixed $is_translation_enabled
     * @return TwitterUserMetadata
     */
    public function setIsTranslationEnabled($is_translation_enabled)
    {
        $this->is_translation_enabled = $is_translation_enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileBackgroundColor()
    {
        return $this->profile_background_color;
    }

    /**
     * @param mixed $profile_background_color
     * @return TwitterUserMetadata
     */
    public function setProfileBackgroundColor($profile_background_color)
    {
        $this->profile_background_color = $profile_background_color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileBackgroundImageUrl()
    {
        return $this->profile_background_image_url;
    }

    /**
     * @param mixed $profile_background_image_url
     * @return TwitterUserMetadata
     */
    public function setProfileBackgroundImageUrl($profile_background_image_url)
    {
        $this->profile_background_image_url = $profile_background_image_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileBackgroundImageUrlHttps()
    {
        return $this->profile_background_image_url_https;
    }

    /**
     * @param mixed $profile_background_image_url_https
     * @return TwitterUserMetadata
     */
    public function setProfileBackgroundImageUrlHttps($profile_background_image_url_https)
    {
        $this->profile_background_image_url_https = $profile_background_image_url_https;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileBackgroundTile()
    {
        return $this->profile_background_tile;
    }

    /**
     * @param mixed $profile_background_tile
     * @return TwitterUserMetadata
     */
    public function setProfileBackgroundTile($profile_background_tile)
    {
        $this->profile_background_tile = $profile_background_tile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileImageUrl()
    {
        return $this->profile_image_url;
    }

    /**
     * @param mixed $profile_image_url
     * @return TwitterUserMetadata
     */
    public function setProfileImageUrl($profile_image_url)
    {
        $this->profile_image_url = $profile_image_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileImageUrlHttps()
    {
        return $this->profile_image_url_https;
    }

    /**
     * @param mixed $profile_image_url_https
     * @return TwitterUserMetadata
     */
    public function setProfileImageUrlHttps($profile_image_url_https)
    {
        $this->profile_image_url_https = $profile_image_url_https;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileBannerUrl()
    {
        return $this->profile_banner_url;
    }

    /**
     * @param mixed $profile_banner_url
     * @return TwitterUserMetadata
     */
    public function setProfileBannerUrl($profile_banner_url)
    {
        $this->profile_banner_url = $profile_banner_url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileLinkColor()
    {
        return $this->profile_link_color;
    }

    /**
     * @param mixed $profile_link_color
     * @return TwitterUserMetadata
     */
    public function setProfileLinkColor($profile_link_color)
    {
        $this->profile_link_color = $profile_link_color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileSidebarBorderColor()
    {
        return $this->profile_sidebar_border_color;
    }

    /**
     * @param mixed $profile_sidebar_border_color
     * @return TwitterUserMetadata
     */
    public function setProfileSidebarBorderColor($profile_sidebar_border_color)
    {
        $this->profile_sidebar_border_color = $profile_sidebar_border_color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileSidebarFillColor()
    {
        return $this->profile_sidebar_fill_color;
    }

    /**
     * @param mixed $profile_sidebar_fill_color
     * @return TwitterUserMetadata
     */
    public function setProfileSidebarFillColor($profile_sidebar_fill_color)
    {
        $this->profile_sidebar_fill_color = $profile_sidebar_fill_color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileTextColor()
    {
        return $this->profile_text_color;
    }

    /**
     * @param mixed $profile_text_color
     * @return TwitterUserMetadata
     */
    public function setProfileTextColor($profile_text_color)
    {
        $this->profile_text_color = $profile_text_color;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfileUseBackgroundImage()
    {
        return $this->profile_use_background_image;
    }

    /**
     * @param mixed $profile_use_background_image
     * @return TwitterUserMetadata
     */
    public function setProfileUseBackgroundImage($profile_use_background_image)
    {
        $this->profile_use_background_image = $profile_use_background_image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHasExtendedProfile()
    {
        return $this->has_extended_profile;
    }

    /**
     * @param mixed $has_extended_profile
     * @return TwitterUserMetadata
     */
    public function setHasExtendedProfile($has_extended_profile)
    {
        $this->has_extended_profile = $has_extended_profile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultProfile()
    {
        return $this->default_profile;
    }

    /**
     * @param mixed $default_profile
     * @return TwitterUserMetadata
     */
    public function setDefaultProfile($default_profile)
    {
        $this->default_profile = $default_profile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultProfileImage()
    {
        return $this->default_profile_image;
    }

    /**
     * @param mixed $default_profile_image
     * @return TwitterUserMetadata
     */
    public function setDefaultProfileImage($default_profile_image)
    {
        $this->default_profile_image = $default_profile_image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param mixed $following
     * @return TwitterUserMetadata
     */
    public function setFollowing($following)
    {
        $this->following = $following;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFollowRequestSent()
    {
        return $this->follow_request_sent;
    }

    /**
     * @param mixed $follow_request_sent
     * @return TwitterUserMetadata
     */
    public function setFollowRequestSent($follow_request_sent)
    {
        $this->follow_request_sent = $follow_request_sent;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param mixed $notifications
     * @return TwitterUserMetadata
     */
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTranslatorType()
    {
        return $this->translator_type;
    }

    /**
     * @param mixed $translator_type
     * @return TwitterUserMetadata
     */
    public function setTranslatorType($translator_type)
    {
        $this->translator_type = $translator_type;
        return $this;
    }

}
