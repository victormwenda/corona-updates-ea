<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TwitterStatus extends Model
{
    use SoftDeletes;

    protected $table = "twitter_status";

    protected $fillable = ['contributors', 'coordinates', 'created_at', 'created_by', 'deleted_at', 'deleted_by',
        'favorite_count', 'favorited', 'id', 'in_reply_to_screen_name', 'in_reply_to_status_id', 'in_reply_to_status_id_str',
        'in_reply_to_user_id', 'in_reply_to_user_id_str', 'is_quote_status', 'lang', 'place', 'possibly_sensitive',
        'retweet_count', 'retweeted', 'retweeted_status', 'tweet_created_at', 'tweet_entities', 'tweet_id', 'tweet_id_str',
        'tweet_source', 'tweet_text', 'tweet_truncated', 'twitter_geo', 'twitter_user_id', 'updated_at', 'updated_by',
        'version'];


}
