<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TwitterUser extends Model
{
    use SoftDeletes;

    protected $table = "twitter_user";

    protected $fillable = ['account_created_at', 'contributors_enabled', 'created_at', 'created_by', 'default_profile',
        'default_profile_image', 'deleted_at', 'deleted_by', 'description', 'display_name', 'entities', 'favourites_count',
        'follow_request_sent', 'followers_count', 'following', 'friends_count', 'geo_enabled', 'has_extended_profile', 'id',
        'is_translation_enabled', 'is_translator', 'lang', 'listed_count', 'location', 'notifications', 'profile_background_color',
        'profile_background_image_url', 'profile_background_image_url_https', 'profile_background_tile', 'profile_banner_url',
        'profile_image_url', 'profile_image_url_https', 'profile_link_color', 'profile_sidebar_border_color',
        'profile_sidebar_fill_color', 'profile_text_color', 'profile_use_background_image', 'protected', 'screen_name',
        'statuses_count', 'time_zone', 'translator_type', 'twitter_user_id', 'twitter_user_id_str', 'updated_at', 'updated_by',
        'url', 'utc_offset', 'verified', 'version'];


}
