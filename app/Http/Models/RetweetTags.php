<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RetweetTags extends Model
{
    use SoftDeletes;

    protected $table = "retweet_tags";

    protected $fillable = ['case_sensitive', 'created_at', 'created_by', 'deleted_at', 'deleted_by', 'id', 'regex_pattern',
        'search_text', 'updated_at', 'updated_by', 'version'];


}
