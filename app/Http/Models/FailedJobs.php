<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class FailedJobs extends Model
{
    protected $table = "failed_jobs";

    protected $fillable = ['connection', 'exception', 'failed_at', 'id', 'payload', 'queue'];


}
