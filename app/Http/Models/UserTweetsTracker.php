<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTweetsTracker extends Model
{
    use SoftDeletes;

    protected $table = "user_tweets_tracker";

    protected $fillable = ['created_at', 'created_by', 'deleted_at', 'deleted_by', 'id', 'last_tweet_id', 'last_tweet_time',
        'twitter_display_name', 'twitter_screen_name', 'twitter_user_id', 'updated_at', 'updated_by', 'version'];


}
