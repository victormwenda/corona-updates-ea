<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Migrations extends Model
{
    protected $table = "migrations";

    protected $fillable = ['batch', 'id', 'migration'];


}
