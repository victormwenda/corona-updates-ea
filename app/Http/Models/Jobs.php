<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = "jobs";

    protected $fillable = ['attempts', 'available_at', 'created_at', 'id', 'payload', 'queue', 'reserved_at'];


}
