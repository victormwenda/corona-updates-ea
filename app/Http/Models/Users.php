<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = "users";

    protected $fillable = ['created_at', 'email', 'email_verified_at', 'id', 'name', 'password', 'remember_token', 'updated_at'];


}
