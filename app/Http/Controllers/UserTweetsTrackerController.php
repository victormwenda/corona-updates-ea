<?php

namespace App\Http\Controllers;

use App\Http\Models\UserTweetsTracker;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UserTweetsTrackerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | UserTweetsTracker Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userTweetsTracker = UserTweetsTracker::all();

        return view('user-tweets-tracker/index')->with('userTweetsTracker', $userTweetsTracker);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $id = $request->get('id');
        $lastTweetId = $request->get('lastTweetId');
        $lastTweetTime = $request->get('lastTweetTime');
        $twitterDisplayName = $request->get('twitterDisplayName');
        $twitterScreenName = $request->get('twitterScreenName');
        $twitterUserId = $request->get('twitterUserId');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $version = $request->get('version');

        $formData = ['created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version);
        $userTweetsTracker = new UserTweetsTracker($attributes);

        if ($userTweetsTracker->save()) {
            $content = "UserTweetsTracker created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "UserTweetsTracker not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\UserTweetsTracker $userTweetsTracker
     * @return \Illuminate\Http\Response
     */
    public function show(UserTweetsTracker $userTweetsTracker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $userTweetsTrackerId
     * @return \Illuminate\Http\Response
     */
    public function edit($userTweetsTrackerId)
    {
        $userTweetsTracker = UserTweetsTracker::find($userTweetsTrackerId);

        if ($userTweetsTracker != null) {
            return response($userTweetsTracker, Response::HTTP_OK);
        }

        $content = "UserTweetsTracker not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $userTweetsTrackerId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userTweetsTrackerId)
    {
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $id = $request->get('id');
        $lastTweetId = $request->get('lastTweetId');
        $lastTweetTime = $request->get('lastTweetTime');
        $twitterDisplayName = $request->get('twitterDisplayName');
        $twitterScreenName = $request->get('twitterScreenName');
        $twitterUserId = $request->get('twitterUserId');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $version = $request->get('version');

        $formData = ['created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userTweetsTracker = UserTweetsTracker::find($userTweetsTrackerId);

        if ($userTweetsTracker == null) {
            $content = "UserTweetsTracker not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version);

        $userTweetsTracker->update($attributes);

        return response($userTweetsTracker, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $userTweetsTrackerId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($userTweetsTrackerId)
    {
        $userTweetsTracker = UserTweetsTracker::find($userTweetsTrackerId);

        if ($userTweetsTracker != null) {
            $userTweetsTracker->delete();
            return response($userTweetsTracker, Response::HTTP_OK);
        }

        $content = "UserTweetsTracker not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $userTweetsTracker = UserTweetsTracker::all();
        return view('user-tweets-tracker.table')->with('userTweetsTracker', $userTweetsTracker);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'id' => ['required', 'nullable',], 'last_tweet_id' => ['required', 'nullable',], 'last_tweet_time' => ['required', 'string', 'max:65535',], 'twitter_display_name' => ['required', 'string', 'max:65535',], 'twitter_screen_name' => ['required', 'string', 'max:65535',], 'twitter_user_id' => ['required', 'nullable',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'id' => ['required', 'nullable',], 'last_tweet_id' => ['required', 'nullable',], 'last_tweet_time' => ['required', 'string', 'max:65535',], 'twitter_display_name' => ['required', 'string', 'max:65535',], 'twitter_screen_name' => ['required', 'string', 'max:65535',], 'twitter_user_id' => ['required', 'nullable',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }
}
