<?php

namespace App\Http\Controllers;

use App\Http\Models\RetweetTags;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RetweetTagsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | RetweetTags Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retweetTags = RetweetTags::all();

        return view('retweet-tags/index')->with('retweetTags', $retweetTags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $caseSensitive = $request->get('caseSensitive');
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $id = $request->get('id');
        $regexPattern = $request->get('regexPattern');
        $searchText = $request->get('searchText');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $version = $request->get('version');

        $formData = ['case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version);
        $retweetTags = new RetweetTags($attributes);

        if ($retweetTags->save()) {
            $content = "RetweetTags created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "RetweetTags not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\RetweetTags $retweetTags
     * @return \Illuminate\Http\Response
     */
    public function show(RetweetTags $retweetTags)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $retweetTagsId
     * @return \Illuminate\Http\Response
     */
    public function edit($retweetTagsId)
    {
        $retweetTags = RetweetTags::find($retweetTagsId);

        if ($retweetTags != null) {
            return response($retweetTags, Response::HTTP_OK);
        }

        $content = "RetweetTags not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $retweetTagsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $retweetTagsId)
    {
        $caseSensitive = $request->get('caseSensitive');
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $id = $request->get('id');
        $regexPattern = $request->get('regexPattern');
        $searchText = $request->get('searchText');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $version = $request->get('version');

        $formData = ['case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $retweetTags = RetweetTags::find($retweetTagsId);

        if ($retweetTags == null) {
            $content = "RetweetTags not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version);

        $retweetTags->update($attributes);

        return response($retweetTags, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $retweetTagsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($retweetTagsId)
    {
        $retweetTags = RetweetTags::find($retweetTagsId);

        if ($retweetTags != null) {
            $retweetTags->delete();
            return response($retweetTags, Response::HTTP_OK);
        }

        $content = "RetweetTags not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $retweetTags = RetweetTags::all();
        return view('retweet-tags.table')->with('retweetTags', $retweetTags);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'case_sensitive' => ['required', 'nullable',], 'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'id' => ['required', 'nullable',], 'regex_pattern' => ['required', 'nullable',], 'search_text' => ['required', 'string', 'max:65535',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'case_sensitive' => ['required', 'nullable',], 'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'id' => ['required', 'nullable',], 'regex_pattern' => ['required', 'nullable',], 'search_text' => ['required', 'string', 'max:65535',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }
}
