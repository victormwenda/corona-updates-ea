<?php

namespace App\Http\Controllers;

use App\Http\Models\Users;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Users Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::all();

        return view('users/index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createdAt = $request->get('createdAt');
        $email = $request->get('email');
        $emailVerifiedAt = $request->get('emailVerifiedAt');
        $id = $request->get('id');
        $name = $request->get('name');
        $password = $request->get('password');
        $rememberToken = $request->get('rememberToken');
        $updatedAt = $request->get('updatedAt');

        $formData = ['created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt);
        $users = new Users($attributes);

        if ($users->save()) {
            $content = "Users created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "Users not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Users $users
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $usersId
     * @return \Illuminate\Http\Response
     */
    public function edit($usersId)
    {
        $users = Users::find($usersId);

        if ($users != null) {
            return response($users, Response::HTTP_OK);
        }

        $content = "Users not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $usersId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $usersId)
    {
        $createdAt = $request->get('createdAt');
        $email = $request->get('email');
        $emailVerifiedAt = $request->get('emailVerifiedAt');
        $id = $request->get('id');
        $name = $request->get('name');
        $password = $request->get('password');
        $rememberToken = $request->get('rememberToken');
        $updatedAt = $request->get('updatedAt');

        $formData = ['created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $users = Users::find($usersId);

        if ($users == null) {
            $content = "Users not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt);

        $users->update($attributes);

        return response($users, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $usersId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($usersId)
    {
        $users = Users::find($usersId);

        if ($users != null) {
            $users->delete();
            return response($users, Response::HTTP_OK);
        }

        $content = "Users not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $users = Users::all();
        return view('users.table')->with('users', $users);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'email' => ['required', 'string', 'max:255',], 'email_verified_at' => ['nullable', 'string',], 'id' => ['required', 'nullable',], 'name' => ['required', 'string', 'max:255',], 'password' => ['required', 'string', 'max:255',], 'remember_token' => ['nullable', 'string', 'max:100',], 'updated_at' => ['nullable', 'string',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'email' => ['required', 'string', 'max:255',], 'email_verified_at' => ['nullable', 'string',], 'id' => ['required', 'nullable',], 'name' => ['required', 'string', 'max:255',], 'password' => ['required', 'string', 'max:255',], 'remember_token' => ['nullable', 'string', 'max:100',], 'updated_at' => ['nullable', 'string',]
        ]);
    }
}
