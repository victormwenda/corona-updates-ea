<?php

namespace App\Http\Controllers;

use App\Http\Models\FailedJobs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class FailedJobsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | FailedJobs Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $failedJobs = FailedJobs::all();

        return view('failed-jobs/index')->with('failedJobs', $failedJobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $connection = $request->get('connection');
        $exception = $request->get('exception');
        $failedAt = $request->get('failedAt');
        $id = $request->get('id');
        $payload = $request->get('payload');
        $queue = $request->get('queue');

        $formData = ['connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue);
        $failedJobs = new FailedJobs($attributes);

        if ($failedJobs->save()) {
            $content = "FailedJobs created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "FailedJobs not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\FailedJobs $failedJobs
     * @return \Illuminate\Http\Response
     */
    public function show(FailedJobs $failedJobs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $failedJobsId
     * @return \Illuminate\Http\Response
     */
    public function edit($failedJobsId)
    {
        $failedJobs = FailedJobs::find($failedJobsId);

        if ($failedJobs != null) {
            return response($failedJobs, Response::HTTP_OK);
        }

        $content = "FailedJobs not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $failedJobsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $failedJobsId)
    {
        $connection = $request->get('connection');
        $exception = $request->get('exception');
        $failedAt = $request->get('failedAt');
        $id = $request->get('id');
        $payload = $request->get('payload');
        $queue = $request->get('queue');

        $formData = ['connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $failedJobs = FailedJobs::find($failedJobsId);

        if ($failedJobs == null) {
            $content = "FailedJobs not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue);

        $failedJobs->update($attributes);

        return response($failedJobs, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $failedJobsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($failedJobsId)
    {
        $failedJobs = FailedJobs::find($failedJobsId);

        if ($failedJobs != null) {
            $failedJobs->delete();
            return response($failedJobs, Response::HTTP_OK);
        }

        $content = "FailedJobs not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $failedJobs = FailedJobs::all();
        return view('failed-jobs.table')->with('failedJobs', $failedJobs);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'connection' => ['required', 'string', 'max:65535',], 'exception' => ['required', 'string', 'max:4294967295',], 'failed_at' => ['required', 'string',], 'id' => ['required', 'nullable',], 'payload' => ['required', 'string', 'max:4294967295',], 'queue' => ['required', 'string', 'max:65535',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'connection' => ['required', 'string', 'max:65535',], 'exception' => ['required', 'string', 'max:4294967295',], 'failed_at' => ['required', 'string',], 'id' => ['required', 'nullable',], 'payload' => ['required', 'string', 'max:4294967295',], 'queue' => ['required', 'string', 'max:65535',]
        ]);
    }
}
