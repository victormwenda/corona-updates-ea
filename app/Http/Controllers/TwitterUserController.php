<?php

namespace App\Http\Controllers;

use App\Http\Models\TwitterUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TwitterUserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | TwitterUser Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $twitterUser = TwitterUser::all();

        return view('twitter-user/index')->with('twitterUser', $twitterUser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $accountCreatedAt = $request->get('accountCreatedAt');
        $contributorsEnabled = $request->get('contributorsEnabled');
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $defaultProfile = $request->get('defaultProfile');
        $defaultProfileImage = $request->get('defaultProfileImage');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $description = $request->get('description');
        $displayName = $request->get('displayName');
        $entities = $request->get('entities');
        $favouritesCount = $request->get('favouritesCount');
        $followRequestSent = $request->get('followRequestSent');
        $followersCount = $request->get('followersCount');
        $following = $request->get('following');
        $friendsCount = $request->get('friendsCount');
        $geoEnabled = $request->get('geoEnabled');
        $hasExtendedProfile = $request->get('hasExtendedProfile');
        $id = $request->get('id');
        $isTranslationEnabled = $request->get('isTranslationEnabled');
        $isTranslator = $request->get('isTranslator');
        $lang = $request->get('lang');
        $listedCount = $request->get('listedCount');
        $location = $request->get('location');
        $notifications = $request->get('notifications');
        $profileBackgroundColor = $request->get('profileBackgroundColor');
        $profileBackgroundImageUrl = $request->get('profileBackgroundImageUrl');
        $profileBackgroundImageUrlHttps = $request->get('profileBackgroundImageUrlHttps');
        $profileBackgroundTile = $request->get('profileBackgroundTile');
        $profileBannerUrl = $request->get('profileBannerUrl');
        $profileImageUrl = $request->get('profileImageUrl');
        $profileImageUrlHttps = $request->get('profileImageUrlHttps');
        $profileLinkColor = $request->get('profileLinkColor');
        $profileSidebarBorderColor = $request->get('profileSidebarBorderColor');
        $profileSidebarFillColor = $request->get('profileSidebarFillColor');
        $profileTextColor = $request->get('profileTextColor');
        $profileUseBackgroundImage = $request->get('profileUseBackgroundImage');
        $protected = $request->get('protected');
        $screenName = $request->get('screenName');
        $statusesCount = $request->get('statusesCount');
        $timeZone = $request->get('timeZone');
        $translatorType = $request->get('translatorType');
        $twitterUserId = $request->get('twitterUserId');
        $twitterUserIdStr = $request->get('twitterUserIdStr');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $url = $request->get('url');
        $utcOffset = $request->get('utcOffset');
        $verified = $request->get('verified');
        $version = $request->get('version');

        $formData = ['account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version);
        $twitterUser = new TwitterUser($attributes);

        if ($twitterUser->save()) {
            $content = "TwitterUser created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "TwitterUser not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TwitterUser $twitterUser
     * @return \Illuminate\Http\Response
     */
    public function show(TwitterUser $twitterUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $twitterUserId
     * @return \Illuminate\Http\Response
     */
    public function edit($twitterUserId)
    {
        $twitterUser = TwitterUser::find($twitterUserId);

        if ($twitterUser != null) {
            return response($twitterUser, Response::HTTP_OK);
        }

        $content = "TwitterUser not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $twitterUserId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $twitterUserId)
    {
        $accountCreatedAt = $request->get('accountCreatedAt');
        $contributorsEnabled = $request->get('contributorsEnabled');
        $createdAt = $request->get('createdAt');
        $createdBy = $request->get('createdBy');
        $defaultProfile = $request->get('defaultProfile');
        $defaultProfileImage = $request->get('defaultProfileImage');
        $deletedAt = $request->get('deletedAt');
        $deletedBy = $request->get('deletedBy');
        $description = $request->get('description');
        $displayName = $request->get('displayName');
        $entities = $request->get('entities');
        $favouritesCount = $request->get('favouritesCount');
        $followRequestSent = $request->get('followRequestSent');
        $followersCount = $request->get('followersCount');
        $following = $request->get('following');
        $friendsCount = $request->get('friendsCount');
        $geoEnabled = $request->get('geoEnabled');
        $hasExtendedProfile = $request->get('hasExtendedProfile');
        $id = $request->get('id');
        $isTranslationEnabled = $request->get('isTranslationEnabled');
        $isTranslator = $request->get('isTranslator');
        $lang = $request->get('lang');
        $listedCount = $request->get('listedCount');
        $location = $request->get('location');
        $notifications = $request->get('notifications');
        $profileBackgroundColor = $request->get('profileBackgroundColor');
        $profileBackgroundImageUrl = $request->get('profileBackgroundImageUrl');
        $profileBackgroundImageUrlHttps = $request->get('profileBackgroundImageUrlHttps');
        $profileBackgroundTile = $request->get('profileBackgroundTile');
        $profileBannerUrl = $request->get('profileBannerUrl');
        $profileImageUrl = $request->get('profileImageUrl');
        $profileImageUrlHttps = $request->get('profileImageUrlHttps');
        $profileLinkColor = $request->get('profileLinkColor');
        $profileSidebarBorderColor = $request->get('profileSidebarBorderColor');
        $profileSidebarFillColor = $request->get('profileSidebarFillColor');
        $profileTextColor = $request->get('profileTextColor');
        $profileUseBackgroundImage = $request->get('profileUseBackgroundImage');
        $protected = $request->get('protected');
        $screenName = $request->get('screenName');
        $statusesCount = $request->get('statusesCount');
        $timeZone = $request->get('timeZone');
        $translatorType = $request->get('translatorType');
        $twitterUserId = $request->get('twitterUserId');
        $twitterUserIdStr = $request->get('twitterUserIdStr');
        $updatedAt = $request->get('updatedAt');
        $updatedBy = $request->get('updatedBy');
        $url = $request->get('url');
        $utcOffset = $request->get('utcOffset');
        $verified = $request->get('verified');
        $version = $request->get('version');

        $formData = ['account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $twitterUser = TwitterUser::find($twitterUserId);

        if ($twitterUser == null) {
            $content = "TwitterUser not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version);

        $twitterUser->update($attributes);

        return response($twitterUser, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $twitterUserId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($twitterUserId)
    {
        $twitterUser = TwitterUser::find($twitterUserId);

        if ($twitterUser != null) {
            $twitterUser->delete();
            return response($twitterUser, Response::HTTP_OK);
        }

        $content = "TwitterUser not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $twitterUser = TwitterUser::all();
        return view('twitter-user.table')->with('twitterUser', $twitterUser);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'account_created_at' => ['required', 'string', 'max:65535',], 'contributors_enabled' => ['required', 'nullable',], 'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'default_profile' => ['required', 'nullable',], 'default_profile_image' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'description' => ['required', 'string', 'max:65535',], 'display_name' => ['required', 'string', 'max:65535',], 'entities' => ['required', 'string', 'max:4294967295',], 'favourites_count' => ['required', 'nullable',], 'follow_request_sent' => ['required', 'nullable',], 'followers_count' => ['required', 'nullable',], 'following' => ['required', 'nullable',], 'friends_count' => ['required', 'nullable',], 'geo_enabled' => ['required', 'nullable',], 'has_extended_profile' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'is_translation_enabled' => ['required', 'nullable',], 'is_translator' => ['required', 'nullable',], 'lang' => ['nullable', 'string', 'max:65535',], 'listed_count' => ['required', 'nullable',], 'location' => ['required', 'string', 'max:65535',], 'notifications' => ['required', 'nullable',], 'profile_background_color' => ['required', 'string', 'max:65535',], 'profile_background_image_url' => ['required', 'string', 'max:65535',], 'profile_background_image_url_https' => ['required', 'string', 'max:65535',], 'profile_background_tile' => ['required', 'nullable',], 'profile_banner_url' => ['required', 'string', 'max:65535',], 'profile_image_url' => ['required', 'string', 'max:65535',], 'profile_image_url_https' => ['required', 'string', 'max:65535',], 'profile_link_color' => ['required', 'string', 'max:65535',], 'profile_sidebar_border_color' => ['required', 'string', 'max:65535',], 'profile_sidebar_fill_color' => ['required', 'string', 'max:65535',], 'profile_text_color' => ['required', 'string', 'max:65535',], 'profile_use_background_image' => ['required', 'nullable',], 'protected' => ['required', 'nullable',], 'screen_name' => ['required', 'string', 'max:65535',], 'statuses_count' => ['required', 'nullable',], 'time_zone' => ['nullable', 'string', 'max:65535',], 'translator_type' => ['required', 'string', 'max:65535',], 'twitter_user_id' => ['required', 'nullable',], 'twitter_user_id_str' => ['required', 'string', 'max:4294967295',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'url' => ['nullable', 'string', 'max:65535',], 'utc_offset' => ['nullable', 'nullable',], 'verified' => ['required', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'account_created_at' => ['required', 'string', 'max:65535',], 'contributors_enabled' => ['required', 'nullable',], 'created_at' => ['nullable', 'string',], 'created_by' => ['required', 'nullable',], 'default_profile' => ['required', 'nullable',], 'default_profile_image' => ['required', 'nullable',], 'deleted_at' => ['nullable', 'string',], 'deleted_by' => ['nullable', 'nullable',], 'description' => ['required', 'string', 'max:65535',], 'display_name' => ['required', 'string', 'max:65535',], 'entities' => ['required', 'string', 'max:4294967295',], 'favourites_count' => ['required', 'nullable',], 'follow_request_sent' => ['required', 'nullable',], 'followers_count' => ['required', 'nullable',], 'following' => ['required', 'nullable',], 'friends_count' => ['required', 'nullable',], 'geo_enabled' => ['required', 'nullable',], 'has_extended_profile' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'is_translation_enabled' => ['required', 'nullable',], 'is_translator' => ['required', 'nullable',], 'lang' => ['nullable', 'string', 'max:65535',], 'listed_count' => ['required', 'nullable',], 'location' => ['required', 'string', 'max:65535',], 'notifications' => ['required', 'nullable',], 'profile_background_color' => ['required', 'string', 'max:65535',], 'profile_background_image_url' => ['required', 'string', 'max:65535',], 'profile_background_image_url_https' => ['required', 'string', 'max:65535',], 'profile_background_tile' => ['required', 'nullable',], 'profile_banner_url' => ['required', 'string', 'max:65535',], 'profile_image_url' => ['required', 'string', 'max:65535',], 'profile_image_url_https' => ['required', 'string', 'max:65535',], 'profile_link_color' => ['required', 'string', 'max:65535',], 'profile_sidebar_border_color' => ['required', 'string', 'max:65535',], 'profile_sidebar_fill_color' => ['required', 'string', 'max:65535',], 'profile_text_color' => ['required', 'string', 'max:65535',], 'profile_use_background_image' => ['required', 'nullable',], 'protected' => ['required', 'nullable',], 'screen_name' => ['required', 'string', 'max:65535',], 'statuses_count' => ['required', 'nullable',], 'time_zone' => ['nullable', 'string', 'max:65535',], 'translator_type' => ['required', 'string', 'max:65535',], 'twitter_user_id' => ['required', 'nullable',], 'twitter_user_id_str' => ['required', 'string', 'max:4294967295',], 'updated_at' => ['nullable', 'string',], 'updated_by' => ['nullable', 'nullable',], 'url' => ['nullable', 'string', 'max:65535',], 'utc_offset' => ['nullable', 'nullable',], 'verified' => ['required', 'nullable',], 'version' => ['required', 'nullable',]
        ]);
    }
}
