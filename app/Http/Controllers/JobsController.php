<?php

namespace App\Http\Controllers;

use App\Http\Models\Jobs;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class JobsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Jobs Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Jobs::all();

        return view('jobs/index')->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attempts = $request->get('attempts');
        $availableAt = $request->get('availableAt');
        $createdAt = $request->get('createdAt');
        $id = $request->get('id');
        $payload = $request->get('payload');
        $queue = $request->get('queue');
        $reservedAt = $request->get('reservedAt');

        $formData = ['attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt);
        $jobs = new Jobs($attributes);

        if ($jobs->save()) {
            $content = "Jobs created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "Jobs not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Jobs $jobs
     * @return \Illuminate\Http\Response
     */
    public function show(Jobs $jobs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $jobsId
     * @return \Illuminate\Http\Response
     */
    public function edit($jobsId)
    {
        $jobs = Jobs::find($jobsId);

        if ($jobs != null) {
            return response($jobs, Response::HTTP_OK);
        }

        $content = "Jobs not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $jobsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jobsId)
    {
        $attempts = $request->get('attempts');
        $availableAt = $request->get('availableAt');
        $createdAt = $request->get('createdAt');
        $id = $request->get('id');
        $payload = $request->get('payload');
        $queue = $request->get('queue');
        $reservedAt = $request->get('reservedAt');

        $formData = ['attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $jobs = Jobs::find($jobsId);

        if ($jobs == null) {
            $content = "Jobs not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt);

        $jobs->update($attributes);

        return response($jobs, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $jobsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($jobsId)
    {
        $jobs = Jobs::find($jobsId);

        if ($jobs != null) {
            $jobs->delete();
            return response($jobs, Response::HTTP_OK);
        }

        $content = "Jobs not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $jobs = Jobs::all();
        return view('jobs.table')->with('jobs', $jobs);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'attempts' => ['required', 'nullable',], 'available_at' => ['required', 'nullable',], 'created_at' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'payload' => ['required', 'string', 'max:4294967295',], 'queue' => ['required', 'string', 'max:255',], 'reserved_at' => ['nullable', 'nullable',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'attempts' => ['required', 'nullable',], 'available_at' => ['required', 'nullable',], 'created_at' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'payload' => ['required', 'string', 'max:4294967295',], 'queue' => ['required', 'string', 'max:255',], 'reserved_at' => ['nullable', 'nullable',]
        ]);
    }
}
