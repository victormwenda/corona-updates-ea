<?php

namespace App\Http\Controllers;

use App\Http\Models\Migrations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class MigrationsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Migrations Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $migrations = Migrations::all();

        return view('migrations/index')->with('migrations', $migrations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $batch = $request->get('batch');
        $id = $request->get('id');
        $migration = $request->get('migration');

        $formData = ['batch' => $batch, 'id' => $id, 'migration' => $migration];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('batch' => $batch, 'id' => $id, 'migration' => $migration);
        $migrations = new Migrations($attributes);

        if ($migrations->save()) {
            $content = "Migrations created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "Migrations not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Migrations $migrations
     * @return \Illuminate\Http\Response
     */
    public function show(Migrations $migrations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $migrationsId
     * @return \Illuminate\Http\Response
     */
    public function edit($migrationsId)
    {
        $migrations = Migrations::find($migrationsId);

        if ($migrations != null) {
            return response($migrations, Response::HTTP_OK);
        }

        $content = "Migrations not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $migrationsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $migrationsId)
    {
        $batch = $request->get('batch');
        $id = $request->get('id');
        $migration = $request->get('migration');

        $formData = ['batch' => $batch, 'id' => $id, 'migration' => $migration];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $migrations = Migrations::find($migrationsId);

        if ($migrations == null) {
            $content = "Migrations not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('batch' => $batch, 'id' => $id, 'migration' => $migration);

        $migrations->update($attributes);

        return response($migrations, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $migrationsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($migrationsId)
    {
        $migrations = Migrations::find($migrationsId);

        if ($migrations != null) {
            $migrations->delete();
            return response($migrations, Response::HTTP_OK);
        }

        $content = "Migrations not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $migrations = Migrations::all();
        return view('migrations.table')->with('migrations', $migrations);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'batch' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'migration' => ['required', 'string', 'max:255',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'batch' => ['required', 'nullable',], 'id' => ['required', 'nullable',], 'migration' => ['required', 'string', 'max:255',]
        ]);
    }
}
