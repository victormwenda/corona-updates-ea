<?php

namespace App\Http\Controllers;

use App\Http\Models\TwitterBotRetweets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TwitterBotRetweetsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | TwitterBotRetweets Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $twitterBotRetweets = TwitterBotRetweets::all();
        
        return view('twitter-bot-retweets/index')->with('twitterBotRetweets', $twitterBotRetweets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

        $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {
            
            $errors = $validator->errors();

            $content =  "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );
        $twitterBotRetweets = new TwitterBotRetweets($attributes);

        if ($twitterBotRetweets->save()) {
            $content = "TwitterBotRetweets created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "TwitterBotRetweets not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TwitterBotRetweets $twitterBotRetweets
     * @return \Illuminate\Http\Response
     */
    public function show(TwitterBotRetweets $twitterBotRetweets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $twitterBotRetweetsId
     * @return \Illuminate\Http\Response
     */
    public function edit($twitterBotRetweetsId)
    {
        $twitterBotRetweets = TwitterBotRetweets::find($twitterBotRetweetsId);

        if ($twitterBotRetweets != null) {
            return response($twitterBotRetweets, Response::HTTP_OK);
        }

        $content = "TwitterBotRetweets not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $twitterBotRetweetsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $twitterBotRetweetsId)
    {
        $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

        $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $twitterBotRetweets = TwitterBotRetweets::find($twitterBotRetweetsId);

        if ($twitterBotRetweets == null) {
            $content = "TwitterBotRetweets not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );

        $twitterBotRetweets->update($attributes);

        return response($twitterBotRetweets, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $twitterBotRetweetsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($twitterBotRetweetsId)
    {
        $twitterBotRetweets = TwitterBotRetweets::find($twitterBotRetweetsId);

        if ($twitterBotRetweets != null) {
            $twitterBotRetweets->delete();
            return response($twitterBotRetweets, Response::HTTP_OK);
        }

        $content = "TwitterBotRetweets not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $twitterBotRetweets = TwitterBotRetweets::all();
        return view('twitter-bot-retweets.table')->with('twitterBotRetweets', $twitterBotRetweets);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
             'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
             'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);
    }
}