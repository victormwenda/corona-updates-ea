<?php

namespace App\Http\Controllers;

use App\Http\Models\PasswordResets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class PasswordResetsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | PasswordResets Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the SCRUD functions of the table '${TABLE_NAME}'
    |
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passwordResets = PasswordResets::all();

        return view('password-resets/index')->with('passwordResets', $passwordResets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createdAt = $request->get('createdAt');
        $email = $request->get('email');
        $token = $request->get('token');

        $formData = ['created_at' => $createdAt, 'email' => $email, 'token' => $token];

        $validator = $this->insertValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'email' => $email, 'token' => $token);
        $passwordResets = new PasswordResets($attributes);

        if ($passwordResets->save()) {
            $content = "PasswordResets created";
            return response($content, Response::HTTP_CREATED);
        }

        $content = "PasswordResets not created";

        return response($content, Response::HTTP_I_AM_A_TEAPOT);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PasswordResets $passwordResets
     * @return \Illuminate\Http\Response
     */
    public function show(PasswordResets $passwordResets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $passwordResetsId
     * @return \Illuminate\Http\Response
     */
    public function edit($passwordResetsId)
    {
        $passwordResets = PasswordResets::find($passwordResetsId);

        if ($passwordResets != null) {
            return response($passwordResets, Response::HTTP_OK);
        }

        $content = "PasswordResets not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $passwordResetsId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $passwordResetsId)
    {
        $createdAt = $request->get('createdAt');
        $email = $request->get('email');
        $token = $request->get('token');

        $formData = ['created_at' => $createdAt, 'email' => $email, 'token' => $token];

        $validator = $this->updateValidator($formData);

        if ($validator->fails()) {

            $errors = $validator->errors();

            $content = "Sorry! You provided incorrect data";

            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $passwordResets = PasswordResets::find($passwordResetsId);

        if ($passwordResets == null) {
            $content = "PasswordResets not found ";
            return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $attributes = array('created_at' => $createdAt, 'email' => $email, 'token' => $token);

        $passwordResets->update($attributes);

        return response($passwordResets, Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from the records.
     *
     * @param int $passwordResetsId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($passwordResetsId)
    {
        $passwordResets = PasswordResets::find($passwordResetsId);

        if ($passwordResets != null) {
            $passwordResets->delete();
            return response($passwordResets, Response::HTTP_OK);
        }

        $content = "PasswordResets not found";
        return response($content, Response::HTTP_GONE);
    }

    /**
     * Refreshes the list of records
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh()
    {
        $passwordResets = PasswordResets::all();
        return view('password-resets.table')->with('passwordResets', $passwordResets);
    }

    /**
     * Get a validator for an incoming insert request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function insertValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'email' => ['required', 'string', 'max:255',], 'token' => ['required', 'string', 'max:255',]
        ]);
    }

    /**
     * Get a validator for an incoming update request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function updateValidator(array $data)
    {
        return Validator::make($data, [
            'created_at' => ['nullable', 'string',], 'email' => ['required', 'string', 'max:255',], 'token' => ['required', 'string', 'max:255',]
        ]);
    }
}
