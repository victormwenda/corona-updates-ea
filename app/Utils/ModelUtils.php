<?php


namespace App\Utils;


use App\Core\TweetMetadata;
use App\Core\TwitterUserMetadata;
use App\Http\Models\TwitterStatus;
use App\Http\Models\TwitterUser;

class ModelUtils
{
    /**
     * Creates the attributes of a user model
     * @param TwitterUserMetadata $twitter_user
     * @return array
     */
    public static function createTwitterUserModelAttributes(TwitterUserMetadata $twitter_user)
    {
        $attributes = [
            'twitter_user_id' => $twitter_user->getId(),
            'twitter_user_id_str' => $twitter_user->getIdStr(),
            'display_name' => $twitter_user->getName(),
            'screen_name' => $twitter_user->getScreenName(),
            'location' => $twitter_user->getLocation(),
            'description' => $twitter_user->getDescription(),
            'url' => $twitter_user->getUrl(),
            'entities' => $twitter_user->getEntities(),
            'protected' => $twitter_user->getProtected(),
            'followers_count' => $twitter_user->getFollowersCount(),
            'friends_count' => $twitter_user->getFriendsCount(),
            'listed_count' => $twitter_user->getListedCount(),
            'account_created_at' => $twitter_user->getCreatedAt(),
            'favourites_count' => $twitter_user->getFavouritesCount(),
            'utc_offset' => $twitter_user->getUtcOffset(),
            'time_zone' => $twitter_user->getTimeZone(),
            'geo_enabled' => $twitter_user->getGeoEnabled(),
            'verified' => $twitter_user->getVerified(),
            'statuses_count' => $twitter_user->getStatusesCount(),
            'lang' => $twitter_user->getLang(),
            'contributors_enabled' => $twitter_user->getContributorsEnabled(),
            'is_translator' => $twitter_user->getIsTranslator(),
            'is_translation_enabled' => $twitter_user->getIsTranslationEnabled(),
            'profile_background_color' => $twitter_user->getProfileBackgroundColor(),
            'profile_background_image_url' => $twitter_user->getProfileBackgroundImageUrl(),
            'profile_background_image_url_https' => $twitter_user->getProfileBackgroundImageUrlHttps(),
            'profile_background_tile' => $twitter_user->getProfileBackgroundTile(),
            'profile_image_url' => $twitter_user->getProfileImageUrl(),
            'profile_image_url_https' => $twitter_user->getProfileImageUrlHttps(),
            'profile_banner_url' => $twitter_user->getProfileBannerUrl(),
            'profile_link_color' => $twitter_user->getProfileLinkColor(),
            'profile_sidebar_border_color' => $twitter_user->getProfileSidebarBorderColor(),
            'profile_sidebar_fill_color' => $twitter_user->getProfileSidebarFillColor(),
            'profile_text_color' => $twitter_user->getProfileTextColor(),
            'profile_use_background_image' => $twitter_user->getProfileUseBackgroundImage(),
            'has_extended_profile' => $twitter_user->getHasExtendedProfile(),
            'default_profile' => $twitter_user->getDefaultProfile(),
            'default_profile_image' => $twitter_user->getDefaultProfileImage(),
            'following' => $twitter_user->getFollowing(),
            'follow_request_sent' => $twitter_user->getFollowRequestSent(),
            'notifications' => $twitter_user->getNotifications(),
            'translator_type' => $twitter_user->getTranslatorType(),
            'version' => 1,
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => 0,
            'deleted_at' => null];

        return $attributes;
    }

    /**
     * Creates the attributes of a twitter tweet/status
     * @param TweetMetadata $tweet_json
     * @return array
     */
    public static function createTwitterStatusModelAttributes(TweetMetadata $tweet_json)
    {
        $attributes = [
            'tweet_created_at' => $tweet_json->getCreatedAt(),
            'tweet_id' => $tweet_json->getId(),
            'tweet_id_str' => $tweet_json->getIdStr(),
            'tweet_text' => $tweet_json->getText(),
            'tweet_truncated' => $tweet_json->getTruncated(),
            'tweet_entities' => $tweet_json->getEntities(),
            'tweet_source' => $tweet_json->getSource(),
            'in_reply_to_status_id' => $tweet_json->getInReplyToStatusId(),
            'in_reply_to_status_id_str' => $tweet_json->getInReplyToStatusIdStr(),
            'in_reply_to_user_id' => $tweet_json->getInReplyToUserId(),
            'in_reply_to_user_id_str' => $tweet_json->getInReplyToUserIdStr(),
            'in_reply_to_screen_name' => $tweet_json->getInReplyToScreenName(),
            'twitter_user_id' => $tweet_json->getTwitterUser()->getId(),
            'twitter_geo' => $tweet_json->getGeo(),
            'coordinates' => $tweet_json->getCoordinates(),
            'place' => $tweet_json->getPlace(),
            'contributors' => $tweet_json->getContributors(),
            'retweeted_status' => $tweet_json->getRetweetedStatus() != null ? $tweet_json->getRetweetedStatus()->getId() : null,
            'is_quote_status' => $tweet_json->getIsQuoteStatus(),
            'retweet_count' => $tweet_json->getRetweetCount(),
            'favorite_count' => $tweet_json->getFavoriteCount(),
            'favorited' => $tweet_json->getFavorited(),
            'retweeted' => $tweet_json->getRetweeted(),
            'possibly_sensitive' => $tweet_json->getPossiblySensitive(),
            'lang' => $tweet_json->getLang(),
            'version' => 1,
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => null,
            'deleted_at' => null,
        ];

        return $attributes;
    }

    /**
     * Create the attributes for a user status/tweet tracker
     * @param TweetMetadata $tweetMetadata
     * @return array
     */
    public static function createUserStatusTrackerModelAttributes(TweetMetadata $tweetMetadata)
    {
        $twitterUser = $tweetMetadata->getTwitterUser();

        $attributes = ['twitter_user_id' => $twitterUser->getId(),
            'twitter_screen_name' => $twitterUser->getScreenName(),
            'twitter_display_name' => $twitterUser->getName(),
            'last_tweet_id' => $tweetMetadata->getId(),
            'last_tweet_time' => $tweetMetadata->getCreatedAt(),
            'version' => 1,
            'created_by' => 0,
            'updated_by' => 0,
            'deleted_by' => null,
            'deleted_at' => null,
        ];

        return $attributes;
    }

}
