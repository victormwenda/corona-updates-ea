<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Page Title-->
    <title>Corona Updates Ea Bot | Twitter Status</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>

<nav class="navbar"></nav>

<section></section>

<section class="content">
    <div class="clear row">
        <div class="col-lg-12">

            <!-- Button trigger modal -->
            <div class="mx-my-5 px-lg-5 col-lg-12 d-flex justify-content-end">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal"
                        data-target="#twitter_status">Create New Item
                </button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="twitter_status" tabindex="-1" role="dialog"
                 aria-labelledby="#twitter_status-label"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Twitter Status</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="create-item-form" class="form">
                                <div class="form-group"><label for="contributors">Contributors</label><input
                                        class="form-control" id="contributors" placeholder="Contributors"></div>
                                <div class="form-group"><label for="coordinates">Coordinates</label><input
                                        class="form-control" id="coordinates" placeholder="Coordinates"></div>
                                <div class="form-group"><label for="created-at">Created At</label><input
                                        class="form-control" id="created-at" placeholder="Created At"></div>
                                <div class="form-group"><label for="created-by">Created By</label><input
                                        class="form-control" id="created-by" placeholder="Created By"></div>
                                <div class="form-group"><label for="deleted-at">Deleted At</label><input
                                        class="form-control" id="deleted-at" placeholder="Deleted At"></div>
                                <div class="form-group"><label for="deleted-by">Deleted By</label><input
                                        class="form-control" id="deleted-by" placeholder="Deleted By"></div>
                                <div class="form-group"><label for="favorite-count">Favorite Count</label><input
                                        class="form-control" id="favorite-count" placeholder="Favorite Count"></div>
                                <div class="form-group"><label for="favorited">Favorited</label><input
                                        class="form-control" id="favorited" placeholder="Favorited"></div>
                                <div class="form-group"><label for="id">Id</label><input class="form-control" id="id"
                                                                                         placeholder="Id"></div>
                                <div class="form-group"><label for="in-reply-to-screen-name">In Reply To Screen
                                        Name</label><input class="form-control" id="in-reply-to-screen-name"
                                                           placeholder="In Reply To Screen Name"></div>
                                <div class="form-group"><label for="in-reply-to-status-id">In Reply To Status Id</label><input
                                        class="form-control" id="in-reply-to-status-id"
                                        placeholder="In Reply To Status Id"></div>
                                <div class="form-group"><label for="in-reply-to-status-id-str">In Reply To Status Id
                                        Str</label><input class="form-control" id="in-reply-to-status-id-str"
                                                          placeholder="In Reply To Status Id Str"></div>
                                <div class="form-group"><label for="in-reply-to-user-id">In Reply To User
                                        Id</label><input class="form-control" id="in-reply-to-user-id"
                                                         placeholder="In Reply To User Id"></div>
                                <div class="form-group"><label for="in-reply-to-user-id-str">In Reply To User Id
                                        Str</label><input class="form-control" id="in-reply-to-user-id-str"
                                                          placeholder="In Reply To User Id Str"></div>
                                <div class="form-group"><label for="is-quote-status">Is Quote Status</label><input
                                        class="form-control" id="is-quote-status" placeholder="Is Quote Status"></div>
                                <div class="form-group"><label for="lang">Lang</label><input class="form-control"
                                                                                             id="lang"
                                                                                             placeholder="Lang"></div>
                                <div class="form-group"><label for="place">Place</label><input class="form-control"
                                                                                               id="place"
                                                                                               placeholder="Place">
                                </div>
                                <div class="form-group"><label for="possibly-sensitive">Possibly Sensitive</label><input
                                        class="form-control" id="possibly-sensitive" placeholder="Possibly Sensitive">
                                </div>
                                <div class="form-group"><label for="retweet-count">Retweet Count</label><input
                                        class="form-control" id="retweet-count" placeholder="Retweet Count"></div>
                                <div class="form-group"><label for="retweeted">Retweeted</label><input
                                        class="form-control" id="retweeted" placeholder="Retweeted"></div>
                                <div class="form-group"><label for="retweeted-status">Retweeted Status</label><input
                                        class="form-control" id="retweeted-status" placeholder="Retweeted Status"></div>
                                <div class="form-group"><label for="tweet-created-at">Tweet Created At</label><input
                                        class="form-control" id="tweet-created-at" placeholder="Tweet Created At"></div>
                                <div class="form-group"><label for="tweet-entities">Tweet Entities</label><input
                                        class="form-control" id="tweet-entities" placeholder="Tweet Entities"></div>
                                <div class="form-group"><label for="tweet-id">Tweet Id</label><input
                                        class="form-control" id="tweet-id" placeholder="Tweet Id"></div>
                                <div class="form-group"><label for="tweet-id-str">Tweet Id Str</label><input
                                        class="form-control" id="tweet-id-str" placeholder="Tweet Id Str"></div>
                                <div class="form-group"><label for="tweet-source">Tweet Source</label><input
                                        class="form-control" id="tweet-source" placeholder="Tweet Source"></div>
                                <div class="form-group"><label for="tweet-text">Tweet Text</label><input
                                        class="form-control" id="tweet-text" placeholder="Tweet Text"></div>
                                <div class="form-group"><label for="tweet-truncated">Tweet Truncated</label><input
                                        class="form-control" id="tweet-truncated" placeholder="Tweet Truncated"></div>
                                <div class="form-group"><label for="twitter-geo">Twitter Geo</label><input
                                        class="form-control" id="twitter-geo" placeholder="Twitter Geo"></div>
                                <div class="form-group"><label for="twitter-user-id">Twitter User Id</label><input
                                        class="form-control" id="twitter-user-id" placeholder="Twitter User Id"></div>
                                <div class="form-group"><label for="updated-at">Updated At</label><input
                                        class="form-control" id="updated-at" placeholder="Updated At"></div>
                                <div class="form-group"><label for="updated-by">Updated By</label><input
                                        class="form-control" id="updated-by" placeholder="Updated By"></div>
                                <div class="form-group"><label for="version">Version</label><input class="form-control"
                                                                                                   id="version"
                                                                                                   placeholder="Version">
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-warning update-item">Update changes</button>
                            <button type="button" class="btn btn-primary save-item">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clear row mt-5">

        <div class="mx-my-5 px-5 col-lg-12">
            <div class="col-md-offset-1 mt-3 table-container" id="container-table-contents">
                @include('twitter-status.table')
            </div>

        </div>
    </div>

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="/js/twitter-status.js"></script>

</body>

</html>
