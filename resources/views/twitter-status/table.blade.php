<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Contributors</th>
        <th>Coordinates</th>
        <th>Created At</th>
        <th>Created By</th>
        <th>Deleted At</th>
        <th>Deleted By</th>
        <th>Favorite Count</th>
        <th>Favorited</th>
        <th>Id</th>
        <th>In Reply To Screen Name</th>
        <th>In Reply To Status Id</th>
        <th>In Reply To Status Id Str</th>
        <th>In Reply To User Id</th>
        <th>In Reply To User Id Str</th>
        <th>Is Quote Status</th>
        <th>Lang</th>
        <th>Place</th>
        <th>Possibly Sensitive</th>
        <th>Retweet Count</th>
        <th>Retweeted</th>
        <th>Retweeted Status</th>
        <th>Tweet Created At</th>
        <th>Tweet Entities</th>
        <th>Tweet Id</th>
        <th>Tweet Id Str</th>
        <th>Tweet Source</th>
        <th>Tweet Text</th>
        <th>Tweet Truncated</th>
        <th>Twitter Geo</th>
        <th>Twitter User Id</th>
        <th>Updated At</th>
        <th>Updated By</th>
        <th>Version</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($twitterStatus as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->contributors}}</td>
            <td>{{$item->coordinates}}</td>
            <td>{{$item->created_at}}</td>
            <td>{{$item->created_by}}</td>
            <td>{{$item->deleted_at}}</td>
            <td>{{$item->deleted_by}}</td>
            <td>{{$item->favorite_count}}</td>
            <td>{{$item->favorited}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->in_reply_to_screen_name}}</td>
            <td>{{$item->in_reply_to_status_id}}</td>
            <td>{{$item->in_reply_to_status_id_str}}</td>
            <td>{{$item->in_reply_to_user_id}}</td>
            <td>{{$item->in_reply_to_user_id_str}}</td>
            <td>{{$item->is_quote_status}}</td>
            <td>{{$item->lang}}</td>
            <td>{{$item->place}}</td>
            <td>{{$item->possibly_sensitive}}</td>
            <td>{{$item->retweet_count}}</td>
            <td>{{$item->retweeted}}</td>
            <td>{{$item->retweeted_status}}</td>
            <td>{{$item->tweet_created_at}}</td>
            <td>{{$item->tweet_entities}}</td>
            <td>{{$item->tweet_id}}</td>
            <td>{{$item->tweet_id_str}}</td>
            <td>{{$item->tweet_source}}</td>
            <td>{{$item->tweet_text}}</td>
            <td>{{$item->tweet_truncated}}</td>
            <td>{{$item->twitter_geo}}</td>
            <td>{{$item->twitter_user_id}}</td>
            <td>{{$item->updated_at}}</td>
            <td>{{$item->updated_by}}</td>
            <td>{{$item->version}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
