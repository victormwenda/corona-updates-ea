<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Created At</th>
        <th>Email</th>
        <th>Email Verified At</th>
        <th>Id</th>
        <th>Name</th>
        <th>Password</th>
        <th>Remember Token</th>
        <th>Updated At</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->created_at}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->email_verified_at}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->password}}</td>
            <td>{{$item->remember_token}}</td>
            <td>{{$item->updated_at}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
