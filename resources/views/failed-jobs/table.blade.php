<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Connection</th>
        <th>Exception</th>
        <th>Failed At</th>
        <th>Id</th>
        <th>Payload</th>
        <th>Queue</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($failedJobs as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->connection}}</td>
            <td>{{$item->exception}}</td>
            <td>{{$item->failed_at}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->payload}}</td>
            <td>{{$item->queue}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
