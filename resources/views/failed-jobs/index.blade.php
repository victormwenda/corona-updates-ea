<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Page Title-->
    <title>Corona Updates Ea Bot | Failed Jobs</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>

<nav class="navbar"></nav>

<section></section>

<section class="content">
    <div class="clear row">
        <div class="col-lg-12">

            <!-- Button trigger modal -->
            <div class="mx-my-5 px-lg-5 col-lg-12 d-flex justify-content-end">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#failed_jobs">
                    Create New Item
                </button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="failed_jobs" tabindex="-1" role="dialog" aria-labelledby="#failed_jobs-label"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Failed Jobs</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="create-item-form" class="form">
                                <div class="form-group"><label for="connection">Connection</label><input
                                        class="form-control" id="connection" placeholder="Connection"></div>
                                <div class="form-group"><label for="exception">Exception</label><input
                                        class="form-control" id="exception" placeholder="Exception"></div>
                                <div class="form-group"><label for="failed-at">Failed At</label><input
                                        class="form-control" id="failed-at" placeholder="Failed At"></div>
                                <div class="form-group"><label for="id">Id</label><input class="form-control" id="id"
                                                                                         placeholder="Id"></div>
                                <div class="form-group"><label for="payload">Payload</label><input class="form-control"
                                                                                                   id="payload"
                                                                                                   placeholder="Payload">
                                </div>
                                <div class="form-group"><label for="queue">Queue</label><input class="form-control"
                                                                                               id="queue"
                                                                                               placeholder="Queue">
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-warning update-item">Update changes</button>
                            <button type="button" class="btn btn-primary save-item">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clear row mt-5">

        <div class="mx-my-5 px-5 col-lg-12">
            <div class="col-md-offset-1 mt-3 table-container" id="container-table-contents">
                @include('failed-jobs.table')
            </div>

        </div>
    </div>

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="/js/failed-jobs.js"></script>

</body>

</html>
