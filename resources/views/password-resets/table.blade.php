<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Created At</th>
        <th>Email</th>
        <th>Token</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($passwordResets as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->created_at}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->token}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
