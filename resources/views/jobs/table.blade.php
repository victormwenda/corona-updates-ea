<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Attempts</th>
        <th>Available At</th>
        <th>Created At</th>
        <th>Id</th>
        <th>Payload</th>
        <th>Queue</th>
        <th>Reserved At</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($jobs as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->attempts}}</td>
            <td>{{$item->available_at}}</td>
            <td>{{$item->created_at}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->payload}}</td>
            <td>{{$item->queue}}</td>
            <td>{{$item->reserved_at}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
