<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
            <th>Account Created At</th><th>Contributors Enabled</th><th>Created At</th><th>Created By</th><th>Default Profile</th><th>Default Profile Image</th><th>Deleted At</th><th>Deleted By</th><th>Description</th><th>Display Name</th><th>Entities</th><th>Favourites Count</th><th>Follow Request Sent</th><th>Followers Count</th><th>Following</th><th>Friends Count</th><th>Geo Enabled</th><th>Has Extended Profile</th><th>Id</th><th>Is Translation Enabled</th><th>Is Translator</th><th>Lang</th><th>Listed Count</th><th>Location</th><th>Notifications</th><th>Profile Background Color</th><th>Profile Background Image Url</th><th>Profile Background Image Url Https</th><th>Profile Background Tile</th><th>Profile Banner Url</th><th>Profile Image Url</th><th>Profile Image Url Https</th><th>Profile Link Color</th><th>Profile Sidebar Border Color</th><th>Profile Sidebar Fill Color</th><th>Profile Text Color</th><th>Profile Use Background Image</th><th>Protected</th><th>Screen Name</th><th>Statuses Count</th><th>Time Zone</th><th>Translator Type</th><th>Twitter User Id</th><th>Twitter User Id Str</th><th>Updated At</th><th>Updated By</th><th>Url</th><th>Utc Offset</th><th>Verified</th><th>Version</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($twitterUser as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->account_created_at}}</td><td>{{$item->contributors_enabled}}</td><td>{{$item->created_at}}</td><td>{{$item->created_by}}</td><td>{{$item->default_profile}}</td><td>{{$item->default_profile_image}}</td><td>{{$item->deleted_at}}</td><td>{{$item->deleted_by}}</td><td>{{$item->description}}</td><td>{{$item->display_name}}</td><td>{{$item->entities}}</td><td>{{$item->favourites_count}}</td><td>{{$item->follow_request_sent}}</td><td>{{$item->followers_count}}</td><td>{{$item->following}}</td><td>{{$item->friends_count}}</td><td>{{$item->geo_enabled}}</td><td>{{$item->has_extended_profile}}</td><td>{{$item->id}}</td><td>{{$item->is_translation_enabled}}</td><td>{{$item->is_translator}}</td><td>{{$item->lang}}</td><td>{{$item->listed_count}}</td><td>{{$item->location}}</td><td>{{$item->notifications}}</td><td>{{$item->profile_background_color}}</td><td>{{$item->profile_background_image_url}}</td><td>{{$item->profile_background_image_url_https}}</td><td>{{$item->profile_background_tile}}</td><td>{{$item->profile_banner_url}}</td><td>{{$item->profile_image_url}}</td><td>{{$item->profile_image_url_https}}</td><td>{{$item->profile_link_color}}</td><td>{{$item->profile_sidebar_border_color}}</td><td>{{$item->profile_sidebar_fill_color}}</td><td>{{$item->profile_text_color}}</td><td>{{$item->profile_use_background_image}}</td><td>{{$item->protected}}</td><td>{{$item->screen_name}}</td><td>{{$item->statuses_count}}</td><td>{{$item->time_zone}}</td><td>{{$item->translator_type}}</td><td>{{$item->twitter_user_id}}</td><td>{{$item->twitter_user_id_str}}</td><td>{{$item->updated_at}}</td><td>{{$item->updated_by}}</td><td>{{$item->url}}</td><td>{{$item->utc_offset}}</td><td>{{$item->verified}}</td><td>{{$item->version}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>