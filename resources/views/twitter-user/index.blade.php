<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Page Title-->
    <title>Corona Updates Ea Bot | Twitter User</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>

<nav class="navbar"></nav>

<section></section>

<section class="content">
    <div class="clear row">
        <div class="col-lg-12">

            <!-- Button trigger modal -->
            <div class="mx-my-5 px-lg-5 col-lg-12 d-flex justify-content-end">
                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#twitter_user">Create New Item </button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="twitter_user" tabindex="-1" role="dialog" aria-labelledby="#twitter_user-label"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Twitter User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="create-item-form" class="form">
                                <div class="form-group"><label for="account-created-at">Account Created At</label><input class="form-control" id="account-created-at" placeholder="Account Created At"></div>
<div class="form-group"><label for="contributors-enabled">Contributors Enabled</label><input class="form-control" id="contributors-enabled" placeholder="Contributors Enabled"></div>
<div class="form-group"><label for="created-at">Created At</label><input class="form-control" id="created-at" placeholder="Created At"></div>
<div class="form-group"><label for="created-by">Created By</label><input class="form-control" id="created-by" placeholder="Created By"></div>
<div class="form-group"><label for="default-profile">Default Profile</label><input class="form-control" id="default-profile" placeholder="Default Profile"></div>
<div class="form-group"><label for="default-profile-image">Default Profile Image</label><input class="form-control" id="default-profile-image" placeholder="Default Profile Image"></div>
<div class="form-group"><label for="deleted-at">Deleted At</label><input class="form-control" id="deleted-at" placeholder="Deleted At"></div>
<div class="form-group"><label for="deleted-by">Deleted By</label><input class="form-control" id="deleted-by" placeholder="Deleted By"></div>
<div class="form-group"><label for="description">Description</label><input class="form-control" id="description" placeholder="Description"></div>
<div class="form-group"><label for="display-name">Display Name</label><input class="form-control" id="display-name" placeholder="Display Name"></div>
<div class="form-group"><label for="entities">Entities</label><input class="form-control" id="entities" placeholder="Entities"></div>
<div class="form-group"><label for="favourites-count">Favourites Count</label><input class="form-control" id="favourites-count" placeholder="Favourites Count"></div>
<div class="form-group"><label for="follow-request-sent">Follow Request Sent</label><input class="form-control" id="follow-request-sent" placeholder="Follow Request Sent"></div>
<div class="form-group"><label for="followers-count">Followers Count</label><input class="form-control" id="followers-count" placeholder="Followers Count"></div>
<div class="form-group"><label for="following">Following</label><input class="form-control" id="following" placeholder="Following"></div>
<div class="form-group"><label for="friends-count">Friends Count</label><input class="form-control" id="friends-count" placeholder="Friends Count"></div>
<div class="form-group"><label for="geo-enabled">Geo Enabled</label><input class="form-control" id="geo-enabled" placeholder="Geo Enabled"></div>
<div class="form-group"><label for="has-extended-profile">Has Extended Profile</label><input class="form-control" id="has-extended-profile" placeholder="Has Extended Profile"></div>
<div class="form-group"><label for="id">Id</label><input class="form-control" id="id" placeholder="Id"></div>
<div class="form-group"><label for="is-translation-enabled">Is Translation Enabled</label><input class="form-control" id="is-translation-enabled" placeholder="Is Translation Enabled"></div>
<div class="form-group"><label for="is-translator">Is Translator</label><input class="form-control" id="is-translator" placeholder="Is Translator"></div>
<div class="form-group"><label for="lang">Lang</label><input class="form-control" id="lang" placeholder="Lang"></div>
<div class="form-group"><label for="listed-count">Listed Count</label><input class="form-control" id="listed-count" placeholder="Listed Count"></div>
<div class="form-group"><label for="location">Location</label><input class="form-control" id="location" placeholder="Location"></div>
<div class="form-group"><label for="notifications">Notifications</label><input class="form-control" id="notifications" placeholder="Notifications"></div>
<div class="form-group"><label for="profile-background-color">Profile Background Color</label><input class="form-control" id="profile-background-color" placeholder="Profile Background Color"></div>
<div class="form-group"><label for="profile-background-image-url">Profile Background Image Url</label><input class="form-control" id="profile-background-image-url" placeholder="Profile Background Image Url"></div>
<div class="form-group"><label for="profile-background-image-url-https">Profile Background Image Url Https</label><input class="form-control" id="profile-background-image-url-https" placeholder="Profile Background Image Url Https"></div>
<div class="form-group"><label for="profile-background-tile">Profile Background Tile</label><input class="form-control" id="profile-background-tile" placeholder="Profile Background Tile"></div>
<div class="form-group"><label for="profile-banner-url">Profile Banner Url</label><input class="form-control" id="profile-banner-url" placeholder="Profile Banner Url"></div>
<div class="form-group"><label for="profile-image-url">Profile Image Url</label><input class="form-control" id="profile-image-url" placeholder="Profile Image Url"></div>
<div class="form-group"><label for="profile-image-url-https">Profile Image Url Https</label><input class="form-control" id="profile-image-url-https" placeholder="Profile Image Url Https"></div>
<div class="form-group"><label for="profile-link-color">Profile Link Color</label><input class="form-control" id="profile-link-color" placeholder="Profile Link Color"></div>
<div class="form-group"><label for="profile-sidebar-border-color">Profile Sidebar Border Color</label><input class="form-control" id="profile-sidebar-border-color" placeholder="Profile Sidebar Border Color"></div>
<div class="form-group"><label for="profile-sidebar-fill-color">Profile Sidebar Fill Color</label><input class="form-control" id="profile-sidebar-fill-color" placeholder="Profile Sidebar Fill Color"></div>
<div class="form-group"><label for="profile-text-color">Profile Text Color</label><input class="form-control" id="profile-text-color" placeholder="Profile Text Color"></div>
<div class="form-group"><label for="profile-use-background-image">Profile Use Background Image</label><input class="form-control" id="profile-use-background-image" placeholder="Profile Use Background Image"></div>
<div class="form-group"><label for="protected">Protected</label><input class="form-control" id="protected" placeholder="Protected"></div>
<div class="form-group"><label for="screen-name">Screen Name</label><input class="form-control" id="screen-name" placeholder="Screen Name"></div>
<div class="form-group"><label for="statuses-count">Statuses Count</label><input class="form-control" id="statuses-count" placeholder="Statuses Count"></div>
<div class="form-group"><label for="time-zone">Time Zone</label><input class="form-control" id="time-zone" placeholder="Time Zone"></div>
<div class="form-group"><label for="translator-type">Translator Type</label><input class="form-control" id="translator-type" placeholder="Translator Type"></div>
<div class="form-group"><label for="twitter-user-id">Twitter User Id</label><input class="form-control" id="twitter-user-id" placeholder="Twitter User Id"></div>
<div class="form-group"><label for="twitter-user-id-str">Twitter User Id Str</label><input class="form-control" id="twitter-user-id-str" placeholder="Twitter User Id Str"></div>
<div class="form-group"><label for="updated-at">Updated At</label><input class="form-control" id="updated-at" placeholder="Updated At"></div>
<div class="form-group"><label for="updated-by">Updated By</label><input class="form-control" id="updated-by" placeholder="Updated By"></div>
<div class="form-group"><label for="url">Url</label><input class="form-control" id="url" placeholder="Url"></div>
<div class="form-group"><label for="utc-offset">Utc Offset</label><input class="form-control" id="utc-offset" placeholder="Utc Offset"></div>
<div class="form-group"><label for="verified">Verified</label><input class="form-control" id="verified" placeholder="Verified"></div>
<div class="form-group"><label for="version">Version</label><input class="form-control" id="version" placeholder="Version"></div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-warning update-item">Update changes</button>
                            <button type="button" class="btn btn-primary save-item">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clear row mt-5">

        <div class="mx-my-5 px-5 col-lg-12">
            <div class="col-md-offset-1 mt-3 table-container" id="container-table-contents">
                @include('twitter-user.table')
            </div>

        </div>
    </div>

</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

<script src="/js/twitter-user.js"></script>

</body>

</html>