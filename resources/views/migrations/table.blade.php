<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Batch</th>
        <th>Id</th>
        <th>Migration</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($migrations as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->batch}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->migration}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
