<table id="table-contents" class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th>Case Sensitive</th>
        <th>Created At</th>
        <th>Created By</th>
        <th>Deleted At</th>
        <th>Deleted By</th>
        <th>Id</th>
        <th>Regex Pattern</th>
        <th>Search Text</th>
        <th>Updated At</th>
        <th>Updated By</th>
        <th>Version</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($retweetTags as $item)
        <tr>
            <th scope="row">{{$loop->iteration}}</th>
            <td>{{$item->case_sensitive}}</td>
            <td>{{$item->created_at}}</td>
            <td>{{$item->created_by}}</td>
            <td>{{$item->deleted_at}}</td>
            <td>{{$item->deleted_by}}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->regex_pattern}}</td>
            <td>{{$item->search_text}}</td>
            <td>{{$item->updated_at}}</td>
            <td>{{$item->updated_by}}</td>
            <td>{{$item->version}}</td>
            <td>
                <a type="button"><i class="material-icons edit-item" data-id="{{$item->id}}">edit</i></a>
                <a type="button"><i class="material-icons delete-item" data-id="{{$item->id}}">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
