<?php

use App\Http\Models\FailedJobs;
use App\Http\Models\Jobs;
use App\Http\Models\Migrations;
use App\Http\Models\PasswordResets;
use App\Http\Models\RetweetTags;
use App\Http\Models\TwitterBotRetweets;
use App\Http\Models\TwitterStatus;
use App\Http\Models\TwitterUser;
use App\Http\Models\UserTweetsTracker;
use App\Http\Models\Users;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/failed-jobs/store', function (Request $request) {

    $connection = $request->get('connection');$exception = $request->get('exception');$failedAt = $request->get('failedAt');$id = $request->get('id');$payload = $request->get('payload');$queue = $request->get('queue');

    $formData = ['connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue ];

    $validator = Validator::make($formData, [
                'connection' => ['required','string','max:65535',] ,'exception' => ['required','string','max:4294967295',] ,'failed_at' => ['required','string',] ,'id' => ['required','nullable',] ,'payload' => ['required','string','max:4294967295',] ,'queue' => ['required','string','max:65535',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue );
    $failedJobs = new FailedJobs($attributes);

    if ($failedJobs->save()) {
        $content = "FailedJobs created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $failedJobs);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/failed-jobs/list', function (Request $request) {

    $failedJobs = FailedJobs::get();

    if ($failedJobs == null) {
        $content = "FailedJobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $failedJobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/failed-jobs/search', function (Request $request) {

    $failedJobs = FailedJobs::get();

    if ($failedJobs == null) {
        $content = "FailedJobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $failedJobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/failed-jobs/get/{id}', function (Request $request, $id) {

    $failedJobs = FailedJobs::find($id);

    if ($failedJobs == null) {
        $content = "FailedJobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $failedJobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/failed-jobs/update/{id}', function (Request $request, $id) {

    $connection = $request->get('connection');$exception = $request->get('exception');$failedAt = $request->get('failedAt');$id = $request->get('id');$payload = $request->get('payload');$queue = $request->get('queue');

    $formData = ['connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue ];

    $validator = Validator::make($formData, [
        'connection' => ['required','string','max:65535',] ,'exception' => ['required','string','max:4294967295',] ,'failed_at' => ['required','string',] ,'id' => ['required','nullable',] ,'payload' => ['required','string','max:4294967295',] ,'queue' => ['required','string','max:65535',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $failedJobs = FailedJobs::find($id);

    if ($failedJobs == null) {
        $content = "FailedJobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('connection' => $connection, 'exception' => $exception, 'failed_at' => $failedAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue );

    if ($failedJobs->update($attributes)) {
        $content = "FailedJobs created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $failedJobs);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'failed_jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/failed-jobs/delete/{id}', function (Request $request, $id) {

    $failedJobs = FailedJobs::find($id);

    if ($failedJobs == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $failedJobs->delete();

    return array("status_code" => 202, "deleted" => $failedJobs);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/jobs/store', function (Request $request) {

    $attempts = $request->get('attempts');$availableAt = $request->get('availableAt');$createdAt = $request->get('createdAt');$id = $request->get('id');$payload = $request->get('payload');$queue = $request->get('queue');$reservedAt = $request->get('reservedAt');

    $formData = ['attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt ];

    $validator = Validator::make($formData, [
                'attempts' => ['required','nullable',] ,'available_at' => ['required','nullable',] ,'created_at' => ['required','nullable',] ,'id' => ['required','nullable',] ,'payload' => ['required','string','max:4294967295',] ,'queue' => ['required','string','max:255',] ,'reserved_at' => ['nullable','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt );
    $jobs = new Jobs($attributes);

    if ($jobs->save()) {
        $content = "Jobs created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $jobs);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/jobs/list', function (Request $request) {

    $jobs = Jobs::get();

    if ($jobs == null) {
        $content = "Jobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $jobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/jobs/search', function (Request $request) {

    $jobs = Jobs::get();

    if ($jobs == null) {
        $content = "Jobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $jobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/jobs/get/{id}', function (Request $request, $id) {

    $jobs = Jobs::find($id);

    if ($jobs == null) {
        $content = "Jobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $jobs);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/jobs/update/{id}', function (Request $request, $id) {

    $attempts = $request->get('attempts');$availableAt = $request->get('availableAt');$createdAt = $request->get('createdAt');$id = $request->get('id');$payload = $request->get('payload');$queue = $request->get('queue');$reservedAt = $request->get('reservedAt');

    $formData = ['attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt ];

    $validator = Validator::make($formData, [
        'attempts' => ['required','nullable',] ,'available_at' => ['required','nullable',] ,'created_at' => ['required','nullable',] ,'id' => ['required','nullable',] ,'payload' => ['required','string','max:4294967295',] ,'queue' => ['required','string','max:255',] ,'reserved_at' => ['nullable','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $jobs = Jobs::find($id);

    if ($jobs == null) {
        $content = "Jobs not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('attempts' => $attempts, 'available_at' => $availableAt, 'created_at' => $createdAt, 'id' => $id, 'payload' => $payload, 'queue' => $queue, 'reserved_at' => $reservedAt );

    if ($jobs->update($attributes)) {
        $content = "Jobs created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $jobs);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'jobs'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/jobs/delete/{id}', function (Request $request, $id) {

    $jobs = Jobs::find($id);

    if ($jobs == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $jobs->delete();

    return array("status_code" => 202, "deleted" => $jobs);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/migrations/store', function (Request $request) {

    $batch = $request->get('batch');$id = $request->get('id');$migration = $request->get('migration');

    $formData = ['batch' => $batch, 'id' => $id, 'migration' => $migration ];

    $validator = Validator::make($formData, [
                'batch' => ['required','nullable',] ,'id' => ['required','nullable',] ,'migration' => ['required','string','max:255',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('batch' => $batch, 'id' => $id, 'migration' => $migration );
    $migrations = new Migrations($attributes);

    if ($migrations->save()) {
        $content = "Migrations created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $migrations);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/migrations/list', function (Request $request) {

    $migrations = Migrations::get();

    if ($migrations == null) {
        $content = "Migrations not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $migrations);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/migrations/search', function (Request $request) {

    $migrations = Migrations::get();

    if ($migrations == null) {
        $content = "Migrations not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $migrations);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/migrations/get/{id}', function (Request $request, $id) {

    $migrations = Migrations::find($id);

    if ($migrations == null) {
        $content = "Migrations not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $migrations);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/migrations/update/{id}', function (Request $request, $id) {

    $batch = $request->get('batch');$id = $request->get('id');$migration = $request->get('migration');

    $formData = ['batch' => $batch, 'id' => $id, 'migration' => $migration ];

    $validator = Validator::make($formData, [
        'batch' => ['required','nullable',] ,'id' => ['required','nullable',] ,'migration' => ['required','string','max:255',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $migrations = Migrations::find($id);

    if ($migrations == null) {
        $content = "Migrations not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('batch' => $batch, 'id' => $id, 'migration' => $migration );

    if ($migrations->update($attributes)) {
        $content = "Migrations created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $migrations);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'migrations'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/migrations/delete/{id}', function (Request $request, $id) {

    $migrations = Migrations::find($id);

    if ($migrations == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $migrations->delete();

    return array("status_code" => 202, "deleted" => $migrations);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/password-resets/store', function (Request $request) {

    $createdAt = $request->get('createdAt');$email = $request->get('email');$token = $request->get('token');

    $formData = ['created_at' => $createdAt, 'email' => $email, 'token' => $token ];

    $validator = Validator::make($formData, [
                'created_at' => ['nullable','string',] ,'email' => ['required','string','max:255',] ,'token' => ['required','string','max:255',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('created_at' => $createdAt, 'email' => $email, 'token' => $token );
    $passwordResets = new PasswordResets($attributes);

    if ($passwordResets->save()) {
        $content = "PasswordResets created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $passwordResets);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/password-resets/list', function (Request $request) {

    $passwordResets = PasswordResets::get();

    if ($passwordResets == null) {
        $content = "PasswordResets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $passwordResets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/password-resets/search', function (Request $request) {

    $passwordResets = PasswordResets::get();

    if ($passwordResets == null) {
        $content = "PasswordResets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $passwordResets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/password-resets/get/{id}', function (Request $request, $id) {

    $passwordResets = PasswordResets::find($id);

    if ($passwordResets == null) {
        $content = "PasswordResets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $passwordResets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/password-resets/update/{id}', function (Request $request, $id) {

    $createdAt = $request->get('createdAt');$email = $request->get('email');$token = $request->get('token');

    $formData = ['created_at' => $createdAt, 'email' => $email, 'token' => $token ];

    $validator = Validator::make($formData, [
        'created_at' => ['nullable','string',] ,'email' => ['required','string','max:255',] ,'token' => ['required','string','max:255',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $passwordResets = PasswordResets::find($id);

    if ($passwordResets == null) {
        $content = "PasswordResets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('created_at' => $createdAt, 'email' => $email, 'token' => $token );

    if ($passwordResets->update($attributes)) {
        $content = "PasswordResets created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $passwordResets);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'password_resets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/password-resets/delete/{id}', function (Request $request, $id) {

    $passwordResets = PasswordResets::find($id);

    if ($passwordResets == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $passwordResets->delete();

    return array("status_code" => 202, "deleted" => $passwordResets);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/retweet-tags/store', function (Request $request) {

    $caseSensitive = $request->get('caseSensitive');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$id = $request->get('id');$regexPattern = $request->get('regexPattern');$searchText = $request->get('searchText');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
                'case_sensitive' => ['required','nullable',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'id' => ['required','nullable',] ,'regex_pattern' => ['required','nullable',] ,'search_text' => ['required','string','max:65535',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );
    $retweetTags = new RetweetTags($attributes);

    if ($retweetTags->save()) {
        $content = "RetweetTags created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $retweetTags);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/retweet-tags/list', function (Request $request) {

    $retweetTags = RetweetTags::get();

    if ($retweetTags == null) {
        $content = "RetweetTags not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $retweetTags);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/retweet-tags/search', function (Request $request) {

    $retweetTags = RetweetTags::get();

    if ($retweetTags == null) {
        $content = "RetweetTags not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $retweetTags);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/retweet-tags/get/{id}', function (Request $request, $id) {

    $retweetTags = RetweetTags::find($id);

    if ($retweetTags == null) {
        $content = "RetweetTags not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $retweetTags);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/retweet-tags/update/{id}', function (Request $request, $id) {

    $caseSensitive = $request->get('caseSensitive');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$id = $request->get('id');$regexPattern = $request->get('regexPattern');$searchText = $request->get('searchText');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
        'case_sensitive' => ['required','nullable',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'id' => ['required','nullable',] ,'regex_pattern' => ['required','nullable',] ,'search_text' => ['required','string','max:65535',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $retweetTags = RetweetTags::find($id);

    if ($retweetTags == null) {
        $content = "RetweetTags not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('case_sensitive' => $caseSensitive, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'regex_pattern' => $regexPattern, 'search_text' => $searchText, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );

    if ($retweetTags->update($attributes)) {
        $content = "RetweetTags created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $retweetTags);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'retweet_tags'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/retweet-tags/delete/{id}', function (Request $request, $id) {

    $retweetTags = RetweetTags::find($id);

    if ($retweetTags == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $retweetTags->delete();

    return array("status_code" => 202, "deleted" => $retweetTags);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/twitter-bot-retweets/store', function (Request $request) {

    $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
                'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );
    $twitterBotRetweets = new TwitterBotRetweets($attributes);

    if ($twitterBotRetweets->save()) {
        $content = "TwitterBotRetweets created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterBotRetweets);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-bot-retweets/list', function (Request $request) {

    $twitterBotRetweets = TwitterBotRetweets::get();

    if ($twitterBotRetweets == null) {
        $content = "TwitterBotRetweets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterBotRetweets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-bot-retweets/search', function (Request $request) {

    $twitterBotRetweets = TwitterBotRetweets::get();

    if ($twitterBotRetweets == null) {
        $content = "TwitterBotRetweets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterBotRetweets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-bot-retweets/get/{id}', function (Request $request, $id) {

    $twitterBotRetweets = TwitterBotRetweets::find($id);

    if ($twitterBotRetweets == null) {
        $content = "TwitterBotRetweets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterBotRetweets);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/twitter-bot-retweets/update/{id}', function (Request $request, $id) {

    $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
        'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $twitterBotRetweets = TwitterBotRetweets::find($id);

    if ($twitterBotRetweets == null) {
        $content = "TwitterBotRetweets not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );

    if ($twitterBotRetweets->update($attributes)) {
        $content = "TwitterBotRetweets created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterBotRetweets);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'twitter_bot_retweets'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/twitter-bot-retweets/delete/{id}', function (Request $request, $id) {

    $twitterBotRetweets = TwitterBotRetweets::find($id);

    if ($twitterBotRetweets == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $twitterBotRetweets->delete();

    return array("status_code" => 202, "deleted" => $twitterBotRetweets);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/twitter-status/store', function (Request $request) {

    $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
                'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );
    $twitterStatus = new TwitterStatus($attributes);

    if ($twitterStatus->save()) {
        $content = "TwitterStatus created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterStatus);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-status/list', function (Request $request) {

    $twitterStatus = TwitterStatus::get();

    if ($twitterStatus == null) {
        $content = "TwitterStatus not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterStatus);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-status/search', function (Request $request) {

    $twitterStatus = TwitterStatus::get();

    if ($twitterStatus == null) {
        $content = "TwitterStatus not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterStatus);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-status/get/{id}', function (Request $request, $id) {

    $twitterStatus = TwitterStatus::find($id);

    if ($twitterStatus == null) {
        $content = "TwitterStatus not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterStatus);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/twitter-status/update/{id}', function (Request $request, $id) {

    $contributors = $request->get('contributors');$coordinates = $request->get('coordinates');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$favoriteCount = $request->get('favoriteCount');$favorited = $request->get('favorited');$id = $request->get('id');$inReplyToScreenName = $request->get('inReplyToScreenName');$inReplyToStatusId = $request->get('inReplyToStatusId');$inReplyToStatusIdStr = $request->get('inReplyToStatusIdStr');$inReplyToUserId = $request->get('inReplyToUserId');$inReplyToUserIdStr = $request->get('inReplyToUserIdStr');$isQuoteStatus = $request->get('isQuoteStatus');$lang = $request->get('lang');$place = $request->get('place');$possiblySensitive = $request->get('possiblySensitive');$retweetCount = $request->get('retweetCount');$retweeted = $request->get('retweeted');$retweetedStatus = $request->get('retweetedStatus');$tweetCreatedAt = $request->get('tweetCreatedAt');$tweetEntities = $request->get('tweetEntities');$tweetId = $request->get('tweetId');$tweetIdStr = $request->get('tweetIdStr');$tweetSource = $request->get('tweetSource');$tweetText = $request->get('tweetText');$tweetTruncated = $request->get('tweetTruncated');$twitterGeo = $request->get('twitterGeo');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
        'contributors' => ['nullable','string','max:65535',] ,'coordinates' => ['nullable','string','max:65535',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'favorite_count' => ['required','nullable',] ,'favorited' => ['required','nullable',] ,'id' => ['required','nullable',] ,'in_reply_to_screen_name' => ['nullable','string','max:4294967295',] ,'in_reply_to_status_id' => ['nullable','nullable',] ,'in_reply_to_status_id_str' => ['nullable','string','max:4294967295',] ,'in_reply_to_user_id' => ['nullable','nullable',] ,'in_reply_to_user_id_str' => ['nullable','string','max:4294967295',] ,'is_quote_status' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'place' => ['nullable','string','max:65535',] ,'possibly_sensitive' => ['required','nullable',] ,'retweet_count' => ['required','nullable',] ,'retweeted' => ['required','nullable',] ,'retweeted_status' => ['nullable','string','max:65535',] ,'tweet_created_at' => ['required','string','max:255',] ,'tweet_entities' => ['required','string','max:4294967295',] ,'tweet_id' => ['required','nullable',] ,'tweet_id_str' => ['required','string','max:4294967295',] ,'tweet_source' => ['required','string','max:4294967295',] ,'tweet_text' => ['required','string','max:4294967295',] ,'tweet_truncated' => ['required','nullable',] ,'twitter_geo' => ['nullable','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $twitterStatus = TwitterStatus::find($id);

    if ($twitterStatus == null) {
        $content = "TwitterStatus not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('contributors' => $contributors, 'coordinates' => $coordinates, 'created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'favorite_count' => $favoriteCount, 'favorited' => $favorited, 'id' => $id, 'in_reply_to_screen_name' => $inReplyToScreenName, 'in_reply_to_status_id' => $inReplyToStatusId, 'in_reply_to_status_id_str' => $inReplyToStatusIdStr, 'in_reply_to_user_id' => $inReplyToUserId, 'in_reply_to_user_id_str' => $inReplyToUserIdStr, 'is_quote_status' => $isQuoteStatus, 'lang' => $lang, 'place' => $place, 'possibly_sensitive' => $possiblySensitive, 'retweet_count' => $retweetCount, 'retweeted' => $retweeted, 'retweeted_status' => $retweetedStatus, 'tweet_created_at' => $tweetCreatedAt, 'tweet_entities' => $tweetEntities, 'tweet_id' => $tweetId, 'tweet_id_str' => $tweetIdStr, 'tweet_source' => $tweetSource, 'tweet_text' => $tweetText, 'tweet_truncated' => $tweetTruncated, 'twitter_geo' => $twitterGeo, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );

    if ($twitterStatus->update($attributes)) {
        $content = "TwitterStatus created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterStatus);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'twitter_status'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/twitter-status/delete/{id}', function (Request $request, $id) {

    $twitterStatus = TwitterStatus::find($id);

    if ($twitterStatus == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $twitterStatus->delete();

    return array("status_code" => 202, "deleted" => $twitterStatus);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/twitter-user/store', function (Request $request) {

    $accountCreatedAt = $request->get('accountCreatedAt');$contributorsEnabled = $request->get('contributorsEnabled');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$defaultProfile = $request->get('defaultProfile');$defaultProfileImage = $request->get('defaultProfileImage');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$description = $request->get('description');$displayName = $request->get('displayName');$entities = $request->get('entities');$favouritesCount = $request->get('favouritesCount');$followRequestSent = $request->get('followRequestSent');$followersCount = $request->get('followersCount');$following = $request->get('following');$friendsCount = $request->get('friendsCount');$geoEnabled = $request->get('geoEnabled');$hasExtendedProfile = $request->get('hasExtendedProfile');$id = $request->get('id');$isTranslationEnabled = $request->get('isTranslationEnabled');$isTranslator = $request->get('isTranslator');$lang = $request->get('lang');$listedCount = $request->get('listedCount');$location = $request->get('location');$notifications = $request->get('notifications');$profileBackgroundColor = $request->get('profileBackgroundColor');$profileBackgroundImageUrl = $request->get('profileBackgroundImageUrl');$profileBackgroundImageUrlHttps = $request->get('profileBackgroundImageUrlHttps');$profileBackgroundTile = $request->get('profileBackgroundTile');$profileBannerUrl = $request->get('profileBannerUrl');$profileImageUrl = $request->get('profileImageUrl');$profileImageUrlHttps = $request->get('profileImageUrlHttps');$profileLinkColor = $request->get('profileLinkColor');$profileSidebarBorderColor = $request->get('profileSidebarBorderColor');$profileSidebarFillColor = $request->get('profileSidebarFillColor');$profileTextColor = $request->get('profileTextColor');$profileUseBackgroundImage = $request->get('profileUseBackgroundImage');$protected = $request->get('protected');$screenName = $request->get('screenName');$statusesCount = $request->get('statusesCount');$timeZone = $request->get('timeZone');$translatorType = $request->get('translatorType');$twitterUserId = $request->get('twitterUserId');$twitterUserIdStr = $request->get('twitterUserIdStr');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$url = $request->get('url');$utcOffset = $request->get('utcOffset');$verified = $request->get('verified');$version = $request->get('version');

    $formData = ['account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version ];

    $validator = Validator::make($formData, [
                'account_created_at' => ['required','string','max:65535',] ,'contributors_enabled' => ['required','nullable',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'default_profile' => ['required','nullable',] ,'default_profile_image' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'description' => ['required','string','max:65535',] ,'display_name' => ['required','string','max:65535',] ,'entities' => ['required','string','max:4294967295',] ,'favourites_count' => ['required','nullable',] ,'follow_request_sent' => ['required','nullable',] ,'followers_count' => ['required','nullable',] ,'following' => ['required','nullable',] ,'friends_count' => ['required','nullable',] ,'geo_enabled' => ['required','nullable',] ,'has_extended_profile' => ['required','nullable',] ,'id' => ['required','nullable',] ,'is_translation_enabled' => ['required','nullable',] ,'is_translator' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'listed_count' => ['required','nullable',] ,'location' => ['required','string','max:65535',] ,'notifications' => ['required','nullable',] ,'profile_background_color' => ['required','string','max:65535',] ,'profile_background_image_url' => ['required','string','max:65535',] ,'profile_background_image_url_https' => ['required','string','max:65535',] ,'profile_background_tile' => ['required','nullable',] ,'profile_banner_url' => ['required','string','max:65535',] ,'profile_image_url' => ['required','string','max:65535',] ,'profile_image_url_https' => ['required','string','max:65535',] ,'profile_link_color' => ['required','string','max:65535',] ,'profile_sidebar_border_color' => ['required','string','max:65535',] ,'profile_sidebar_fill_color' => ['required','string','max:65535',] ,'profile_text_color' => ['required','string','max:65535',] ,'profile_use_background_image' => ['required','nullable',] ,'protected' => ['required','nullable',] ,'screen_name' => ['required','string','max:65535',] ,'statuses_count' => ['required','nullable',] ,'time_zone' => ['nullable','string','max:65535',] ,'translator_type' => ['required','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'twitter_user_id_str' => ['required','string','max:4294967295',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'url' => ['nullable','string','max:65535',] ,'utc_offset' => ['nullable','nullable',] ,'verified' => ['required','nullable',] ,'version' => ['required','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version );
    $twitterUser = new TwitterUser($attributes);

    if ($twitterUser->save()) {
        $content = "TwitterUser created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterUser);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-user/list', function (Request $request) {

    $twitterUser = TwitterUser::get();

    if ($twitterUser == null) {
        $content = "TwitterUser not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterUser);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-user/search', function (Request $request) {

    $twitterUser = TwitterUser::get();

    if ($twitterUser == null) {
        $content = "TwitterUser not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterUser);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/twitter-user/get/{id}', function (Request $request, $id) {

    $twitterUser = TwitterUser::find($id);

    if ($twitterUser == null) {
        $content = "TwitterUser not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $twitterUser);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/twitter-user/update/{id}', function (Request $request, $id) {

    $accountCreatedAt = $request->get('accountCreatedAt');$contributorsEnabled = $request->get('contributorsEnabled');$createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$defaultProfile = $request->get('defaultProfile');$defaultProfileImage = $request->get('defaultProfileImage');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$description = $request->get('description');$displayName = $request->get('displayName');$entities = $request->get('entities');$favouritesCount = $request->get('favouritesCount');$followRequestSent = $request->get('followRequestSent');$followersCount = $request->get('followersCount');$following = $request->get('following');$friendsCount = $request->get('friendsCount');$geoEnabled = $request->get('geoEnabled');$hasExtendedProfile = $request->get('hasExtendedProfile');$id = $request->get('id');$isTranslationEnabled = $request->get('isTranslationEnabled');$isTranslator = $request->get('isTranslator');$lang = $request->get('lang');$listedCount = $request->get('listedCount');$location = $request->get('location');$notifications = $request->get('notifications');$profileBackgroundColor = $request->get('profileBackgroundColor');$profileBackgroundImageUrl = $request->get('profileBackgroundImageUrl');$profileBackgroundImageUrlHttps = $request->get('profileBackgroundImageUrlHttps');$profileBackgroundTile = $request->get('profileBackgroundTile');$profileBannerUrl = $request->get('profileBannerUrl');$profileImageUrl = $request->get('profileImageUrl');$profileImageUrlHttps = $request->get('profileImageUrlHttps');$profileLinkColor = $request->get('profileLinkColor');$profileSidebarBorderColor = $request->get('profileSidebarBorderColor');$profileSidebarFillColor = $request->get('profileSidebarFillColor');$profileTextColor = $request->get('profileTextColor');$profileUseBackgroundImage = $request->get('profileUseBackgroundImage');$protected = $request->get('protected');$screenName = $request->get('screenName');$statusesCount = $request->get('statusesCount');$timeZone = $request->get('timeZone');$translatorType = $request->get('translatorType');$twitterUserId = $request->get('twitterUserId');$twitterUserIdStr = $request->get('twitterUserIdStr');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$url = $request->get('url');$utcOffset = $request->get('utcOffset');$verified = $request->get('verified');$version = $request->get('version');

    $formData = ['account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version ];

    $validator = Validator::make($formData, [
        'account_created_at' => ['required','string','max:65535',] ,'contributors_enabled' => ['required','nullable',] ,'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'default_profile' => ['required','nullable',] ,'default_profile_image' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'description' => ['required','string','max:65535',] ,'display_name' => ['required','string','max:65535',] ,'entities' => ['required','string','max:4294967295',] ,'favourites_count' => ['required','nullable',] ,'follow_request_sent' => ['required','nullable',] ,'followers_count' => ['required','nullable',] ,'following' => ['required','nullable',] ,'friends_count' => ['required','nullable',] ,'geo_enabled' => ['required','nullable',] ,'has_extended_profile' => ['required','nullable',] ,'id' => ['required','nullable',] ,'is_translation_enabled' => ['required','nullable',] ,'is_translator' => ['required','nullable',] ,'lang' => ['nullable','string','max:65535',] ,'listed_count' => ['required','nullable',] ,'location' => ['required','string','max:65535',] ,'notifications' => ['required','nullable',] ,'profile_background_color' => ['required','string','max:65535',] ,'profile_background_image_url' => ['required','string','max:65535',] ,'profile_background_image_url_https' => ['required','string','max:65535',] ,'profile_background_tile' => ['required','nullable',] ,'profile_banner_url' => ['required','string','max:65535',] ,'profile_image_url' => ['required','string','max:65535',] ,'profile_image_url_https' => ['required','string','max:65535',] ,'profile_link_color' => ['required','string','max:65535',] ,'profile_sidebar_border_color' => ['required','string','max:65535',] ,'profile_sidebar_fill_color' => ['required','string','max:65535',] ,'profile_text_color' => ['required','string','max:65535',] ,'profile_use_background_image' => ['required','nullable',] ,'protected' => ['required','nullable',] ,'screen_name' => ['required','string','max:65535',] ,'statuses_count' => ['required','nullable',] ,'time_zone' => ['nullable','string','max:65535',] ,'translator_type' => ['required','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'twitter_user_id_str' => ['required','string','max:4294967295',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'url' => ['nullable','string','max:65535',] ,'utc_offset' => ['nullable','nullable',] ,'verified' => ['required','nullable',] ,'version' => ['required','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $twitterUser = TwitterUser::find($id);

    if ($twitterUser == null) {
        $content = "TwitterUser not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('account_created_at' => $accountCreatedAt, 'contributors_enabled' => $contributorsEnabled, 'created_at' => $createdAt, 'created_by' => $createdBy, 'default_profile' => $defaultProfile, 'default_profile_image' => $defaultProfileImage, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'description' => $description, 'display_name' => $displayName, 'entities' => $entities, 'favourites_count' => $favouritesCount, 'follow_request_sent' => $followRequestSent, 'followers_count' => $followersCount, 'following' => $following, 'friends_count' => $friendsCount, 'geo_enabled' => $geoEnabled, 'has_extended_profile' => $hasExtendedProfile, 'id' => $id, 'is_translation_enabled' => $isTranslationEnabled, 'is_translator' => $isTranslator, 'lang' => $lang, 'listed_count' => $listedCount, 'location' => $location, 'notifications' => $notifications, 'profile_background_color' => $profileBackgroundColor, 'profile_background_image_url' => $profileBackgroundImageUrl, 'profile_background_image_url_https' => $profileBackgroundImageUrlHttps, 'profile_background_tile' => $profileBackgroundTile, 'profile_banner_url' => $profileBannerUrl, 'profile_image_url' => $profileImageUrl, 'profile_image_url_https' => $profileImageUrlHttps, 'profile_link_color' => $profileLinkColor, 'profile_sidebar_border_color' => $profileSidebarBorderColor, 'profile_sidebar_fill_color' => $profileSidebarFillColor, 'profile_text_color' => $profileTextColor, 'profile_use_background_image' => $profileUseBackgroundImage, 'protected' => $protected, 'screen_name' => $screenName, 'statuses_count' => $statusesCount, 'time_zone' => $timeZone, 'translator_type' => $translatorType, 'twitter_user_id' => $twitterUserId, 'twitter_user_id_str' => $twitterUserIdStr, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'url' => $url, 'utc_offset' => $utcOffset, 'verified' => $verified, 'version' => $version );

    if ($twitterUser->update($attributes)) {
        $content = "TwitterUser created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $twitterUser);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'twitter_user'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/twitter-user/delete/{id}', function (Request $request, $id) {

    $twitterUser = TwitterUser::find($id);

    if ($twitterUser == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $twitterUser->delete();

    return array("status_code" => 202, "deleted" => $twitterUser);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/user-tweets-tracker/store', function (Request $request) {

    $createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$id = $request->get('id');$lastTweetId = $request->get('lastTweetId');$lastTweetTime = $request->get('lastTweetTime');$twitterDisplayName = $request->get('twitterDisplayName');$twitterScreenName = $request->get('twitterScreenName');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
                'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'id' => ['required','nullable',] ,'last_tweet_id' => ['required','nullable',] ,'last_tweet_time' => ['required','string','max:65535',] ,'twitter_display_name' => ['required','string','max:65535',] ,'twitter_screen_name' => ['required','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );
    $userTweetsTracker = new UserTweetsTracker($attributes);

    if ($userTweetsTracker->save()) {
        $content = "UserTweetsTracker created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $userTweetsTracker);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/user-tweets-tracker/list', function (Request $request) {

    $userTweetsTracker = UserTweetsTracker::get();

    if ($userTweetsTracker == null) {
        $content = "UserTweetsTracker not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $userTweetsTracker);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/user-tweets-tracker/search', function (Request $request) {

    $userTweetsTracker = UserTweetsTracker::get();

    if ($userTweetsTracker == null) {
        $content = "UserTweetsTracker not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $userTweetsTracker);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/user-tweets-tracker/get/{id}', function (Request $request, $id) {

    $userTweetsTracker = UserTweetsTracker::find($id);

    if ($userTweetsTracker == null) {
        $content = "UserTweetsTracker not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $userTweetsTracker);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/user-tweets-tracker/update/{id}', function (Request $request, $id) {

    $createdAt = $request->get('createdAt');$createdBy = $request->get('createdBy');$deletedAt = $request->get('deletedAt');$deletedBy = $request->get('deletedBy');$id = $request->get('id');$lastTweetId = $request->get('lastTweetId');$lastTweetTime = $request->get('lastTweetTime');$twitterDisplayName = $request->get('twitterDisplayName');$twitterScreenName = $request->get('twitterScreenName');$twitterUserId = $request->get('twitterUserId');$updatedAt = $request->get('updatedAt');$updatedBy = $request->get('updatedBy');$version = $request->get('version');

    $formData = ['created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version ];

    $validator = Validator::make($formData, [
        'created_at' => ['nullable','string',] ,'created_by' => ['required','nullable',] ,'deleted_at' => ['nullable','string',] ,'deleted_by' => ['nullable','nullable',] ,'id' => ['required','nullable',] ,'last_tweet_id' => ['required','nullable',] ,'last_tweet_time' => ['required','string','max:65535',] ,'twitter_display_name' => ['required','string','max:65535',] ,'twitter_screen_name' => ['required','string','max:65535',] ,'twitter_user_id' => ['required','nullable',] ,'updated_at' => ['nullable','string',] ,'updated_by' => ['nullable','nullable',] ,'version' => ['required','nullable',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $userTweetsTracker = UserTweetsTracker::find($id);

    if ($userTweetsTracker == null) {
        $content = "UserTweetsTracker not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('created_at' => $createdAt, 'created_by' => $createdBy, 'deleted_at' => $deletedAt, 'deleted_by' => $deletedBy, 'id' => $id, 'last_tweet_id' => $lastTweetId, 'last_tweet_time' => $lastTweetTime, 'twitter_display_name' => $twitterDisplayName, 'twitter_screen_name' => $twitterScreenName, 'twitter_user_id' => $twitterUserId, 'updated_at' => $updatedAt, 'updated_by' => $updatedBy, 'version' => $version );

    if ($userTweetsTracker->update($attributes)) {
        $content = "UserTweetsTracker created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $userTweetsTracker);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'user_tweets_tracker'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/user-tweets-tracker/delete/{id}', function (Request $request, $id) {

    $userTweetsTracker = UserTweetsTracker::find($id);

    if ($userTweetsTracker == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $userTweetsTracker->delete();

    return array("status_code" => 202, "deleted" => $userTweetsTracker);

});/**
|-----------------------------------------------------------------------------------------------------------------------
| - INSERTS DATA INTO TABLE 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::post('v1/users/store', function (Request $request) {

    $createdAt = $request->get('createdAt');$email = $request->get('email');$emailVerifiedAt = $request->get('emailVerifiedAt');$id = $request->get('id');$name = $request->get('name');$password = $request->get('password');$rememberToken = $request->get('rememberToken');$updatedAt = $request->get('updatedAt');

    $formData = ['created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt ];

    $validator = Validator::make($formData, [
                'created_at' => ['nullable','string',] ,'email' => ['required','string','max:255',] ,'email_verified_at' => ['nullable','string',] ,'id' => ['required','nullable',] ,'name' => ['required','string','max:255',] ,'password' => ['required','string','max:255',] ,'remember_token' => ['nullable','string','max:100',] ,'updated_at' => ['nullable','string',] 
        ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $attributes = array('created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt );
    $users = new Users($attributes);

    if ($users->save()) {
        $content = "Users created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $users);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ALL RECORDS IN TABLE 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/users/list', function (Request $request) {

    $users = Users::get();

    if ($users == null) {
        $content = "Users not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $users);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - SEARCHES ALL RECORDS IN TABLE 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/users/search', function (Request $request) {

    $users = Users::get();

    if ($users == null) {
        $content = "Users not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $users);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - RETURNS ONE ITEM FROM 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::get('v1/users/get/{id}', function (Request $request, $id) {

    $users = Users::find($id);

    if ($users == null) {
        $content = "Users not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    return array ('status_code' => Response::HTTP_OK, 'errors' => null, 'data' => $users);
});

/**
|-----------------------------------------------------------------------------------------------------------------------
| - UPDATES TABLE 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::put('v1/users/update/{id}', function (Request $request, $id) {

    $createdAt = $request->get('createdAt');$email = $request->get('email');$emailVerifiedAt = $request->get('emailVerifiedAt');$id = $request->get('id');$name = $request->get('name');$password = $request->get('password');$rememberToken = $request->get('rememberToken');$updatedAt = $request->get('updatedAt');

    $formData = ['created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt ];

    $validator = Validator::make($formData, [
        'created_at' => ['nullable','string',] ,'email' => ['required','string','max:255',] ,'email_verified_at' => ['nullable','string',] ,'id' => ['required','nullable',] ,'name' => ['required','string','max:255',] ,'password' => ['required','string','max:255',] ,'remember_token' => ['nullable','string','max:100',] ,'updated_at' => ['nullable','string',] 
    ]);

    if ($validator->fails()) {

        $errors = $validator->errors();

        $content =  "Sorry! You provided incorrect data";

        return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => $errors, 'data' => null);
    }

    $users = Users::find($id);

    if ($users == null) {
        $content = "Users not found ";
        return response($content, Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    $attributes = array('created_at' => $createdAt, 'email' => $email, 'email_verified_at' => $emailVerifiedAt, 'id' => $id, 'name' => $name, 'password' => $password, 'remember_token' => $rememberToken, 'updated_at' => $updatedAt );

    if ($users->update($attributes)) {
        $content = "Users created";
        return array ('status_code' => Response::HTTP_CREATED, 'errors' => null, 'data' => $users);
    }

    return array ('status_code' => Response::HTTP_UNPROCESSABLE_ENTITY, 'errors' => null, 'data' => null);

});


/**
|-----------------------------------------------------------------------------------------------------------------------
| - DELETES ITEM FROM TABLE 'users'
|-----------------------------------------------------------------------------------------------------------------------
*/
Route::delete('v1/users/delete/{id}', function (Request $request, $id) {

    $users = Users::find($id);

    if ($users == null) {
        return array("status_code" => 404, 'item' => $id);
    }

    $users->delete();

    return array("status_code" => 202, "deleted" => $users);

});