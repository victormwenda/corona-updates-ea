<?php

use App\Http\Models\FailedJobs;
use App\Http\Models\Jobs;
use App\Http\Models\Migrations;
use App\Http\Models\PasswordResets;
use App\Http\Models\RetweetTags;
use App\Http\Models\TwitterBotRetweets;
use App\Http\Models\TwitterStatus;
use App\Http\Models\TwitterUser;
use App\Http\Models\UserTweetsTracker;
use App\Http\Models\Users;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'failed_jobs'
|----------------------------------------------------------------------------------------------
*/
Route::get('failed-jobs', 'FailedJobsController@index');
Route::get('failed-jobs/index', 'FailedJobsController@index');
Route::get('failed-jobs/create', 'FailedJobsController@index');
Route::get('failed-jobs/show', 'FailedJobsController@index');
Route::get('failed-jobs/refresh', 'FailedJobsController@refresh');
Route::get('failed-jobs/edit/{id}', 'FailedJobsController@edit');
Route::put('failed-jobs/update/{id}', 'FailedJobsController@update');
Route::post('failed-jobs/store', 'FailedJobsController@store');
Route::delete('failed-jobs/destroy/{id}', 'FailedJobsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'jobs'
|----------------------------------------------------------------------------------------------
*/
Route::get('jobs', 'JobsController@index');
Route::get('jobs/index', 'JobsController@index');
Route::get('jobs/create', 'JobsController@index');
Route::get('jobs/show', 'JobsController@index');
Route::get('jobs/refresh', 'JobsController@refresh');
Route::get('jobs/edit/{id}', 'JobsController@edit');
Route::put('jobs/update/{id}', 'JobsController@update');
Route::post('jobs/store', 'JobsController@store');
Route::delete('jobs/destroy/{id}', 'JobsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'migrations'
|----------------------------------------------------------------------------------------------
*/
Route::get('migrations', 'MigrationsController@index');
Route::get('migrations/index', 'MigrationsController@index');
Route::get('migrations/create', 'MigrationsController@index');
Route::get('migrations/show', 'MigrationsController@index');
Route::get('migrations/refresh', 'MigrationsController@refresh');
Route::get('migrations/edit/{id}', 'MigrationsController@edit');
Route::put('migrations/update/{id}', 'MigrationsController@update');
Route::post('migrations/store', 'MigrationsController@store');
Route::delete('migrations/destroy/{id}', 'MigrationsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'password_resets'
|----------------------------------------------------------------------------------------------
*/
Route::get('password-resets', 'PasswordResetsController@index');
Route::get('password-resets/index', 'PasswordResetsController@index');
Route::get('password-resets/create', 'PasswordResetsController@index');
Route::get('password-resets/show', 'PasswordResetsController@index');
Route::get('password-resets/refresh', 'PasswordResetsController@refresh');
Route::get('password-resets/edit/{id}', 'PasswordResetsController@edit');
Route::put('password-resets/update/{id}', 'PasswordResetsController@update');
Route::post('password-resets/store', 'PasswordResetsController@store');
Route::delete('password-resets/destroy/{id}', 'PasswordResetsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'retweet_tags'
|----------------------------------------------------------------------------------------------
*/
Route::get('retweet-tags', 'RetweetTagsController@index');
Route::get('retweet-tags/index', 'RetweetTagsController@index');
Route::get('retweet-tags/create', 'RetweetTagsController@index');
Route::get('retweet-tags/show', 'RetweetTagsController@index');
Route::get('retweet-tags/refresh', 'RetweetTagsController@refresh');
Route::get('retweet-tags/edit/{id}', 'RetweetTagsController@edit');
Route::put('retweet-tags/update/{id}', 'RetweetTagsController@update');
Route::post('retweet-tags/store', 'RetweetTagsController@store');
Route::delete('retweet-tags/destroy/{id}', 'RetweetTagsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'twitter_bot_retweets'
|----------------------------------------------------------------------------------------------
*/
Route::get('twitter-bot-retweets', 'TwitterBotRetweetsController@index');
Route::get('twitter-bot-retweets/index', 'TwitterBotRetweetsController@index');
Route::get('twitter-bot-retweets/create', 'TwitterBotRetweetsController@index');
Route::get('twitter-bot-retweets/show', 'TwitterBotRetweetsController@index');
Route::get('twitter-bot-retweets/refresh', 'TwitterBotRetweetsController@refresh');
Route::get('twitter-bot-retweets/edit/{id}', 'TwitterBotRetweetsController@edit');
Route::put('twitter-bot-retweets/update/{id}', 'TwitterBotRetweetsController@update');
Route::post('twitter-bot-retweets/store', 'TwitterBotRetweetsController@store');
Route::delete('twitter-bot-retweets/destroy/{id}', 'TwitterBotRetweetsController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'twitter_status'
|----------------------------------------------------------------------------------------------
*/
Route::get('twitter-status', 'TwitterStatusController@index');
Route::get('twitter-status/index', 'TwitterStatusController@index');
Route::get('twitter-status/create', 'TwitterStatusController@index');
Route::get('twitter-status/show', 'TwitterStatusController@index');
Route::get('twitter-status/refresh', 'TwitterStatusController@refresh');
Route::get('twitter-status/edit/{id}', 'TwitterStatusController@edit');
Route::put('twitter-status/update/{id}', 'TwitterStatusController@update');
Route::post('twitter-status/store', 'TwitterStatusController@store');
Route::delete('twitter-status/destroy/{id}', 'TwitterStatusController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'twitter_user'
|----------------------------------------------------------------------------------------------
*/
Route::get('twitter-user', 'TwitterUserController@index');
Route::get('twitter-user/index', 'TwitterUserController@index');
Route::get('twitter-user/create', 'TwitterUserController@index');
Route::get('twitter-user/show', 'TwitterUserController@index');
Route::get('twitter-user/refresh', 'TwitterUserController@refresh');
Route::get('twitter-user/edit/{id}', 'TwitterUserController@edit');
Route::put('twitter-user/update/{id}', 'TwitterUserController@update');
Route::post('twitter-user/store', 'TwitterUserController@store');
Route::delete('twitter-user/destroy/{id}', 'TwitterUserController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'user_tweets_tracker'
|----------------------------------------------------------------------------------------------
*/
Route::get('user-tweets-tracker', 'UserTweetsTrackerController@index');
Route::get('user-tweets-tracker/index', 'UserTweetsTrackerController@index');
Route::get('user-tweets-tracker/create', 'UserTweetsTrackerController@index');
Route::get('user-tweets-tracker/show', 'UserTweetsTrackerController@index');
Route::get('user-tweets-tracker/refresh', 'UserTweetsTrackerController@refresh');
Route::get('user-tweets-tracker/edit/{id}', 'UserTweetsTrackerController@edit');
Route::put('user-tweets-tracker/update/{id}', 'UserTweetsTrackerController@update');
Route::post('user-tweets-tracker/store', 'UserTweetsTrackerController@store');
Route::delete('user-tweets-tracker/destroy/{id}', 'UserTweetsTrackerController@destroy');
/**
|----------------------------------------------------------------------------------------------
| - EXPOSES THE WEB ROUTES TO MANIPULATE THE TABLE 'users'
|----------------------------------------------------------------------------------------------
*/
Route::get('users', 'UsersController@index');
Route::get('users/index', 'UsersController@index');
Route::get('users/create', 'UsersController@index');
Route::get('users/show', 'UsersController@index');
Route::get('users/refresh', 'UsersController@refresh');
Route::get('users/edit/{id}', 'UsersController@edit');
Route::put('users/update/{id}', 'UsersController@update');
Route::post('users/store', 'UsersController@store');
Route::delete('users/destroy/{id}', 'UsersController@destroy');
