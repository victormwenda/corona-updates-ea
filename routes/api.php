<?php

use App\Core\IPWhitelist;
use App\Core\TwitterPoller;
use App\Http\Models\UserTweetsTracker;
use App\Http\Models\TwitterUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/accounts/enroll/', function (Request $request) {

    $user = $request->get('user');

    if ($user != "daemon") {
        if (!IPWhitelist::isWhitelisted()) {
            return redirect('https://i.pinimg.com/originals/2d/d6/3c/2dd63c308cc056db844d90a88fc937c1.jpg');
        }
    }


    $screen_name = $request->get('screen_name', 'CovidUpdatesEA');

    if (empty($screen_name)) {

        $message = array("Message" => "Could not enroll Account <@$screen_name>!", "meta-data" => null);

        return response($message, Response::HTTP_EXPECTATION_FAILED);
    }

    TwitterPoller::onboard($screen_name);

    $message = array("Message" => "Account @$screen_name Enrolled", "meta-data" => null);

    return response($message, Response::HTTP_CREATED);

});

Route::get('/accounts/rebase', function (Request $request) {

    if (!IPWhitelist::isWhitelisted()) {
        return redirect('https://i.pinimg.com/originals/2d/d6/3c/2dd63c308cc056db844d90a88fc937c1.jpg');
    }

    $trackedUsers = TwitterUser::withoutTrashed()->get();


    foreach ($trackedUsers as $trackedUser) {
        TwitterPoller::onboard($trackedUser->screen_name);
    }

    $message = array("Message" => "Accounts rebased", "accounts" => $trackedUsers);

    return response($message, Response::HTTP_ACCEPTED);
});

Route::get('/daemon/poll', function (Request $request) {

    if (!IPWhitelist::isWhitelisted()) {
        return redirect('https://i.pinimg.com/originals/2d/d6/3c/2dd63c308cc056db844d90a88fc937c1.jpg');
    }

    $trackedLastTweets = UserTweetsTracker::withoutTrashed()->get();

    foreach ($trackedLastTweets as $trackedTweet) {
        TwitterPoller::pollRecentTweets($trackedTweet);
    }

    $message = array("Message" => "Accounts rebased", "tracker" => $trackedLastTweets);

    return response($message, Response::HTTP_ACCEPTED);

});




