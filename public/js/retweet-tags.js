$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let caseSensitive = $('#case-sensitive').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let id = $('#id').val();let regexPattern = $('#regex-pattern').val();let searchText = $('#search-text').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'retweet-tags/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'caseSensitive':caseSensitive ,'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'id':id ,'regexPattern':regexPattern ,'searchText':searchText ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#retweet_tags').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'retweet-tags/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let caseSensitive = jqXHRData.case_sensitive;let createdAt = jqXHRData.created_at;let createdBy = jqXHRData.created_by;let deletedAt = jqXHRData.deleted_at;let deletedBy = jqXHRData.deleted_by;let id = jqXHRData.id;let regexPattern = jqXHRData.regex_pattern;let searchText = jqXHRData.search_text;let updatedAt = jqXHRData.updated_at;let updatedBy = jqXHRData.updated_by;let version = jqXHRData.version;

            $('#case-sensitive').val(caseSensitive);$('#created-at').val(createdAt);$('#created-by').val(createdBy);$('#deleted-at').val(deletedAt);$('#deleted-by').val(deletedBy);$('#id').val(id);$('#regex-pattern').val(regexPattern);$('#search-text').val(searchText);$('#updated-at').val(updatedAt);$('#updated-by').val(updatedBy);$('#version').val(version);

            $("#retweet_tags").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let caseSensitive = $('#case-sensitive').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let id = $('#id').val();let regexPattern = $('#regex-pattern').val();let searchText = $('#search-text').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'retweet-tags/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'caseSensitive':caseSensitive ,'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'id':id ,'regexPattern':regexPattern ,'searchText':searchText ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#retweet_tags').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'retweet-tags/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('retweet-tags/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}