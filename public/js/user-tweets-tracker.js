$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let id = $('#id').val();let lastTweetId = $('#last-tweet-id').val();let lastTweetTime = $('#last-tweet-time').val();let twitterDisplayName = $('#twitter-display-name').val();let twitterScreenName = $('#twitter-screen-name').val();let twitterUserId = $('#twitter-user-id').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'user-tweets-tracker/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'id':id ,'lastTweetId':lastTweetId ,'lastTweetTime':lastTweetTime ,'twitterDisplayName':twitterDisplayName ,'twitterScreenName':twitterScreenName ,'twitterUserId':twitterUserId ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#user_tweets_tracker').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'user-tweets-tracker/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let createdAt = jqXHRData.created_at;let createdBy = jqXHRData.created_by;let deletedAt = jqXHRData.deleted_at;let deletedBy = jqXHRData.deleted_by;let id = jqXHRData.id;let lastTweetId = jqXHRData.last_tweet_id;let lastTweetTime = jqXHRData.last_tweet_time;let twitterDisplayName = jqXHRData.twitter_display_name;let twitterScreenName = jqXHRData.twitter_screen_name;let twitterUserId = jqXHRData.twitter_user_id;let updatedAt = jqXHRData.updated_at;let updatedBy = jqXHRData.updated_by;let version = jqXHRData.version;

            $('#created-at').val(createdAt);$('#created-by').val(createdBy);$('#deleted-at').val(deletedAt);$('#deleted-by').val(deletedBy);$('#id').val(id);$('#last-tweet-id').val(lastTweetId);$('#last-tweet-time').val(lastTweetTime);$('#twitter-display-name').val(twitterDisplayName);$('#twitter-screen-name').val(twitterScreenName);$('#twitter-user-id').val(twitterUserId);$('#updated-at').val(updatedAt);$('#updated-by').val(updatedBy);$('#version').val(version);

            $("#user_tweets_tracker").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let id = $('#id').val();let lastTweetId = $('#last-tweet-id').val();let lastTweetTime = $('#last-tweet-time').val();let twitterDisplayName = $('#twitter-display-name').val();let twitterScreenName = $('#twitter-screen-name').val();let twitterUserId = $('#twitter-user-id').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'user-tweets-tracker/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'id':id ,'lastTweetId':lastTweetId ,'lastTweetTime':lastTweetTime ,'twitterDisplayName':twitterDisplayName ,'twitterScreenName':twitterScreenName ,'twitterUserId':twitterUserId ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#user_tweets_tracker').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'user-tweets-tracker/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('user-tweets-tracker/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}