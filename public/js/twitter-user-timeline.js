$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let id = $('#id').val();let tweetCreatedAt = $('#tweet-created-at').val();let tweetId = $('#tweet-id').val();let tweetIdStr = $('#tweet-id-str').val();let tweetText = $('#tweet-text').val();let tweetTruncated = $('#tweet-truncated').val();let tweetEntities = $('#tweet-entities').val();let tweetSource = $('#tweet-source').val();let inReplyToStatusId = $('#in-reply-to-status-id').val();let inReplyToStatusIdStr = $('#in-reply-to-status-id-str').val();let inReplyToUserId = $('#in-reply-to-user-id').val();let inReplyToUserIdStr = $('#in-reply-to-user-id-str').val();let inReplyToScreenName = $('#in-reply-to-screen-name').val();let twitterUserId = $('#twitter-user-id').val();let twitterGeo = $('#twitter-geo').val();let coordinates = $('#coordinates').val();let place = $('#place').val();let contributors = $('#contributors').val();let retweetedStatus = $('#retweeted-status').val();let isQuoteStatus = $('#is-quote-status').val();let retweetCount = $('#retweet-count').val();let favoriteCount = $('#favorite-count').val();let favorited = $('#favorited').val();let retweeted = $('#retweeted').val();let possiblySensitive = $('#possibly-sensitive').val();let lang = $('#lang').val();let version = $('#version').val();let createdBy = $('#created-by').val();let updatedBy = $('#updated-by').val();let deletedBy = $('#deleted-by').val();let deletedAt = $('#deleted-at').val();let createdAt = $('#created-at').val();let updatedAt = $('#updated-at').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'twitter-user-timeline/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'id':id ,'tweetCreatedAt':tweetCreatedAt ,'tweetId':tweetId ,'tweetIdStr':tweetIdStr ,'tweetText':tweetText ,'tweetTruncated':tweetTruncated ,'tweetEntities':tweetEntities ,'tweetSource':tweetSource ,'inReplyToStatusId':inReplyToStatusId ,'inReplyToStatusIdStr':inReplyToStatusIdStr ,'inReplyToUserId':inReplyToUserId ,'inReplyToUserIdStr':inReplyToUserIdStr ,'inReplyToScreenName':inReplyToScreenName ,'twitterUserId':twitterUserId ,'twitterGeo':twitterGeo ,'coordinates':coordinates ,'place':place ,'contributors':contributors ,'retweetedStatus':retweetedStatus ,'isQuoteStatus':isQuoteStatus ,'retweetCount':retweetCount ,'favoriteCount':favoriteCount ,'favorited':favorited ,'retweeted':retweeted ,'possiblySensitive':possiblySensitive ,'lang':lang ,'version':version ,'createdBy':createdBy ,'updatedBy':updatedBy ,'deletedBy':deletedBy ,'deletedAt':deletedAt ,'createdAt':createdAt ,'updatedAt':updatedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_user_timeline').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-user-timeline/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let id = jqXHRData.id;let tweetCreatedAt = jqXHRData.tweet_created_at;let tweetId = jqXHRData.tweet_id;let tweetIdStr = jqXHRData.tweet_id_str;let tweetText = jqXHRData.tweet_text;let tweetTruncated = jqXHRData.tweet_truncated;let tweetEntities = jqXHRData.tweet_entities;let tweetSource = jqXHRData.tweet_source;let inReplyToStatusId = jqXHRData.in_reply_to_status_id;let inReplyToStatusIdStr = jqXHRData.in_reply_to_status_id_str;let inReplyToUserId = jqXHRData.in_reply_to_user_id;let inReplyToUserIdStr = jqXHRData.in_reply_to_user_id_str;let inReplyToScreenName = jqXHRData.in_reply_to_screen_name;let twitterUserId = jqXHRData.twitter_user_id;let twitterGeo = jqXHRData.twitter_geo;let coordinates = jqXHRData.coordinates;let place = jqXHRData.place;let contributors = jqXHRData.contributors;let retweetedStatus = jqXHRData.retweeted_status;let isQuoteStatus = jqXHRData.is_quote_status;let retweetCount = jqXHRData.retweet_count;let favoriteCount = jqXHRData.favorite_count;let favorited = jqXHRData.favorited;let retweeted = jqXHRData.retweeted;let possiblySensitive = jqXHRData.possibly_sensitive;let lang = jqXHRData.lang;let version = jqXHRData.version;let createdBy = jqXHRData.created_by;let updatedBy = jqXHRData.updated_by;let deletedBy = jqXHRData.deleted_by;let deletedAt = jqXHRData.deleted_at;let createdAt = jqXHRData.created_at;let updatedAt = jqXHRData.updated_at;

            $('#id').val(id);$('#tweet-created-at').val(tweetCreatedAt);$('#tweet-id').val(tweetId);$('#tweet-id-str').val(tweetIdStr);$('#tweet-text').val(tweetText);$('#tweet-truncated').val(tweetTruncated);$('#tweet-entities').val(tweetEntities);$('#tweet-source').val(tweetSource);$('#in-reply-to-status-id').val(inReplyToStatusId);$('#in-reply-to-status-id-str').val(inReplyToStatusIdStr);$('#in-reply-to-user-id').val(inReplyToUserId);$('#in-reply-to-user-id-str').val(inReplyToUserIdStr);$('#in-reply-to-screen-name').val(inReplyToScreenName);$('#twitter-user-id').val(twitterUserId);$('#twitter-geo').val(twitterGeo);$('#coordinates').val(coordinates);$('#place').val(place);$('#contributors').val(contributors);$('#retweeted-status').val(retweetedStatus);$('#is-quote-status').val(isQuoteStatus);$('#retweet-count').val(retweetCount);$('#favorite-count').val(favoriteCount);$('#favorited').val(favorited);$('#retweeted').val(retweeted);$('#possibly-sensitive').val(possiblySensitive);$('#lang').val(lang);$('#version').val(version);$('#created-by').val(createdBy);$('#updated-by').val(updatedBy);$('#deleted-by').val(deletedBy);$('#deleted-at').val(deletedAt);$('#created-at').val(createdAt);$('#updated-at').val(updatedAt);

            $("#twitter_user_timeline").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let id = $('#id').val();let tweetCreatedAt = $('#tweet-created-at').val();let tweetId = $('#tweet-id').val();let tweetIdStr = $('#tweet-id-str').val();let tweetText = $('#tweet-text').val();let tweetTruncated = $('#tweet-truncated').val();let tweetEntities = $('#tweet-entities').val();let tweetSource = $('#tweet-source').val();let inReplyToStatusId = $('#in-reply-to-status-id').val();let inReplyToStatusIdStr = $('#in-reply-to-status-id-str').val();let inReplyToUserId = $('#in-reply-to-user-id').val();let inReplyToUserIdStr = $('#in-reply-to-user-id-str').val();let inReplyToScreenName = $('#in-reply-to-screen-name').val();let twitterUserId = $('#twitter-user-id').val();let twitterGeo = $('#twitter-geo').val();let coordinates = $('#coordinates').val();let place = $('#place').val();let contributors = $('#contributors').val();let retweetedStatus = $('#retweeted-status').val();let isQuoteStatus = $('#is-quote-status').val();let retweetCount = $('#retweet-count').val();let favoriteCount = $('#favorite-count').val();let favorited = $('#favorited').val();let retweeted = $('#retweeted').val();let possiblySensitive = $('#possibly-sensitive').val();let lang = $('#lang').val();let version = $('#version').val();let createdBy = $('#created-by').val();let updatedBy = $('#updated-by').val();let deletedBy = $('#deleted-by').val();let deletedAt = $('#deleted-at').val();let createdAt = $('#created-at').val();let updatedAt = $('#updated-at').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-user-timeline/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'id':id ,'tweetCreatedAt':tweetCreatedAt ,'tweetId':tweetId ,'tweetIdStr':tweetIdStr ,'tweetText':tweetText ,'tweetTruncated':tweetTruncated ,'tweetEntities':tweetEntities ,'tweetSource':tweetSource ,'inReplyToStatusId':inReplyToStatusId ,'inReplyToStatusIdStr':inReplyToStatusIdStr ,'inReplyToUserId':inReplyToUserId ,'inReplyToUserIdStr':inReplyToUserIdStr ,'inReplyToScreenName':inReplyToScreenName ,'twitterUserId':twitterUserId ,'twitterGeo':twitterGeo ,'coordinates':coordinates ,'place':place ,'contributors':contributors ,'retweetedStatus':retweetedStatus ,'isQuoteStatus':isQuoteStatus ,'retweetCount':retweetCount ,'favoriteCount':favoriteCount ,'favorited':favorited ,'retweeted':retweeted ,'possiblySensitive':possiblySensitive ,'lang':lang ,'version':version ,'createdBy':createdBy ,'updatedBy':updatedBy ,'deletedBy':deletedBy ,'deletedAt':deletedAt ,'createdAt':createdAt ,'updatedAt':updatedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_user_timeline').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'twitter-user-timeline/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('twitter-user-timeline/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}