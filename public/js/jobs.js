$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let attempts = $('#attempts').val();let availableAt = $('#available-at').val();let createdAt = $('#created-at').val();let id = $('#id').val();let payload = $('#payload').val();let queue = $('#queue').val();let reservedAt = $('#reserved-at').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'jobs/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'attempts':attempts ,'availableAt':availableAt ,'createdAt':createdAt ,'id':id ,'payload':payload ,'queue':queue ,'reservedAt':reservedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#jobs').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'jobs/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let attempts = jqXHRData.attempts;let availableAt = jqXHRData.available_at;let createdAt = jqXHRData.created_at;let id = jqXHRData.id;let payload = jqXHRData.payload;let queue = jqXHRData.queue;let reservedAt = jqXHRData.reserved_at;

            $('#attempts').val(attempts);$('#available-at').val(availableAt);$('#created-at').val(createdAt);$('#id').val(id);$('#payload').val(payload);$('#queue').val(queue);$('#reserved-at').val(reservedAt);

            $("#jobs").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let attempts = $('#attempts').val();let availableAt = $('#available-at').val();let createdAt = $('#created-at').val();let id = $('#id').val();let payload = $('#payload').val();let queue = $('#queue').val();let reservedAt = $('#reserved-at').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'jobs/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'attempts':attempts ,'availableAt':availableAt ,'createdAt':createdAt ,'id':id ,'payload':payload ,'queue':queue ,'reservedAt':reservedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#jobs').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'jobs/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('jobs/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}