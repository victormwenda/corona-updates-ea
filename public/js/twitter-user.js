$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let accountCreatedAt = $('#account-created-at').val();let contributorsEnabled = $('#contributors-enabled').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let defaultProfile = $('#default-profile').val();let defaultProfileImage = $('#default-profile-image').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let description = $('#description').val();let displayName = $('#display-name').val();let entities = $('#entities').val();let favouritesCount = $('#favourites-count').val();let followRequestSent = $('#follow-request-sent').val();let followersCount = $('#followers-count').val();let following = $('#following').val();let friendsCount = $('#friends-count').val();let geoEnabled = $('#geo-enabled').val();let hasExtendedProfile = $('#has-extended-profile').val();let id = $('#id').val();let isTranslationEnabled = $('#is-translation-enabled').val();let isTranslator = $('#is-translator').val();let lang = $('#lang').val();let listedCount = $('#listed-count').val();let location = $('#location').val();let notifications = $('#notifications').val();let profileBackgroundColor = $('#profile-background-color').val();let profileBackgroundImageUrl = $('#profile-background-image-url').val();let profileBackgroundImageUrlHttps = $('#profile-background-image-url-https').val();let profileBackgroundTile = $('#profile-background-tile').val();let profileBannerUrl = $('#profile-banner-url').val();let profileImageUrl = $('#profile-image-url').val();let profileImageUrlHttps = $('#profile-image-url-https').val();let profileLinkColor = $('#profile-link-color').val();let profileSidebarBorderColor = $('#profile-sidebar-border-color').val();let profileSidebarFillColor = $('#profile-sidebar-fill-color').val();let profileTextColor = $('#profile-text-color').val();let profileUseBackgroundImage = $('#profile-use-background-image').val();let protected = $('#protected').val();let screenName = $('#screen-name').val();let statusesCount = $('#statuses-count').val();let timeZone = $('#time-zone').val();let translatorType = $('#translator-type').val();let twitterUserId = $('#twitter-user-id').val();let twitterUserIdStr = $('#twitter-user-id-str').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let url = $('#url').val();let utcOffset = $('#utc-offset').val();let verified = $('#verified').val();let version = $('#version').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'twitter-user/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'accountCreatedAt':accountCreatedAt ,'contributorsEnabled':contributorsEnabled ,'createdAt':createdAt ,'createdBy':createdBy ,'defaultProfile':defaultProfile ,'defaultProfileImage':defaultProfileImage ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'description':description ,'displayName':displayName ,'entities':entities ,'favouritesCount':favouritesCount ,'followRequestSent':followRequestSent ,'followersCount':followersCount ,'following':following ,'friendsCount':friendsCount ,'geoEnabled':geoEnabled ,'hasExtendedProfile':hasExtendedProfile ,'id':id ,'isTranslationEnabled':isTranslationEnabled ,'isTranslator':isTranslator ,'lang':lang ,'listedCount':listedCount ,'location':location ,'notifications':notifications ,'profileBackgroundColor':profileBackgroundColor ,'profileBackgroundImageUrl':profileBackgroundImageUrl ,'profileBackgroundImageUrlHttps':profileBackgroundImageUrlHttps ,'profileBackgroundTile':profileBackgroundTile ,'profileBannerUrl':profileBannerUrl ,'profileImageUrl':profileImageUrl ,'profileImageUrlHttps':profileImageUrlHttps ,'profileLinkColor':profileLinkColor ,'profileSidebarBorderColor':profileSidebarBorderColor ,'profileSidebarFillColor':profileSidebarFillColor ,'profileTextColor':profileTextColor ,'profileUseBackgroundImage':profileUseBackgroundImage ,'protected':protected ,'screenName':screenName ,'statusesCount':statusesCount ,'timeZone':timeZone ,'translatorType':translatorType ,'twitterUserId':twitterUserId ,'twitterUserIdStr':twitterUserIdStr ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'url':url ,'utcOffset':utcOffset ,'verified':verified ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_user').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-user/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let accountCreatedAt = jqXHRData.account_created_at;let contributorsEnabled = jqXHRData.contributors_enabled;let createdAt = jqXHRData.created_at;let createdBy = jqXHRData.created_by;let defaultProfile = jqXHRData.default_profile;let defaultProfileImage = jqXHRData.default_profile_image;let deletedAt = jqXHRData.deleted_at;let deletedBy = jqXHRData.deleted_by;let description = jqXHRData.description;let displayName = jqXHRData.display_name;let entities = jqXHRData.entities;let favouritesCount = jqXHRData.favourites_count;let followRequestSent = jqXHRData.follow_request_sent;let followersCount = jqXHRData.followers_count;let following = jqXHRData.following;let friendsCount = jqXHRData.friends_count;let geoEnabled = jqXHRData.geo_enabled;let hasExtendedProfile = jqXHRData.has_extended_profile;let id = jqXHRData.id;let isTranslationEnabled = jqXHRData.is_translation_enabled;let isTranslator = jqXHRData.is_translator;let lang = jqXHRData.lang;let listedCount = jqXHRData.listed_count;let location = jqXHRData.location;let notifications = jqXHRData.notifications;let profileBackgroundColor = jqXHRData.profile_background_color;let profileBackgroundImageUrl = jqXHRData.profile_background_image_url;let profileBackgroundImageUrlHttps = jqXHRData.profile_background_image_url_https;let profileBackgroundTile = jqXHRData.profile_background_tile;let profileBannerUrl = jqXHRData.profile_banner_url;let profileImageUrl = jqXHRData.profile_image_url;let profileImageUrlHttps = jqXHRData.profile_image_url_https;let profileLinkColor = jqXHRData.profile_link_color;let profileSidebarBorderColor = jqXHRData.profile_sidebar_border_color;let profileSidebarFillColor = jqXHRData.profile_sidebar_fill_color;let profileTextColor = jqXHRData.profile_text_color;let profileUseBackgroundImage = jqXHRData.profile_use_background_image;let protected = jqXHRData.protected;let screenName = jqXHRData.screen_name;let statusesCount = jqXHRData.statuses_count;let timeZone = jqXHRData.time_zone;let translatorType = jqXHRData.translator_type;let twitterUserId = jqXHRData.twitter_user_id;let twitterUserIdStr = jqXHRData.twitter_user_id_str;let updatedAt = jqXHRData.updated_at;let updatedBy = jqXHRData.updated_by;let url = jqXHRData.url;let utcOffset = jqXHRData.utc_offset;let verified = jqXHRData.verified;let version = jqXHRData.version;

            $('#account-created-at').val(accountCreatedAt);$('#contributors-enabled').val(contributorsEnabled);$('#created-at').val(createdAt);$('#created-by').val(createdBy);$('#default-profile').val(defaultProfile);$('#default-profile-image').val(defaultProfileImage);$('#deleted-at').val(deletedAt);$('#deleted-by').val(deletedBy);$('#description').val(description);$('#display-name').val(displayName);$('#entities').val(entities);$('#favourites-count').val(favouritesCount);$('#follow-request-sent').val(followRequestSent);$('#followers-count').val(followersCount);$('#following').val(following);$('#friends-count').val(friendsCount);$('#geo-enabled').val(geoEnabled);$('#has-extended-profile').val(hasExtendedProfile);$('#id').val(id);$('#is-translation-enabled').val(isTranslationEnabled);$('#is-translator').val(isTranslator);$('#lang').val(lang);$('#listed-count').val(listedCount);$('#location').val(location);$('#notifications').val(notifications);$('#profile-background-color').val(profileBackgroundColor);$('#profile-background-image-url').val(profileBackgroundImageUrl);$('#profile-background-image-url-https').val(profileBackgroundImageUrlHttps);$('#profile-background-tile').val(profileBackgroundTile);$('#profile-banner-url').val(profileBannerUrl);$('#profile-image-url').val(profileImageUrl);$('#profile-image-url-https').val(profileImageUrlHttps);$('#profile-link-color').val(profileLinkColor);$('#profile-sidebar-border-color').val(profileSidebarBorderColor);$('#profile-sidebar-fill-color').val(profileSidebarFillColor);$('#profile-text-color').val(profileTextColor);$('#profile-use-background-image').val(profileUseBackgroundImage);$('#protected').val(protected);$('#screen-name').val(screenName);$('#statuses-count').val(statusesCount);$('#time-zone').val(timeZone);$('#translator-type').val(translatorType);$('#twitter-user-id').val(twitterUserId);$('#twitter-user-id-str').val(twitterUserIdStr);$('#updated-at').val(updatedAt);$('#updated-by').val(updatedBy);$('#url').val(url);$('#utc-offset').val(utcOffset);$('#verified').val(verified);$('#version').val(version);

            $("#twitter_user").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let accountCreatedAt = $('#account-created-at').val();let contributorsEnabled = $('#contributors-enabled').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let defaultProfile = $('#default-profile').val();let defaultProfileImage = $('#default-profile-image').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let description = $('#description').val();let displayName = $('#display-name').val();let entities = $('#entities').val();let favouritesCount = $('#favourites-count').val();let followRequestSent = $('#follow-request-sent').val();let followersCount = $('#followers-count').val();let following = $('#following').val();let friendsCount = $('#friends-count').val();let geoEnabled = $('#geo-enabled').val();let hasExtendedProfile = $('#has-extended-profile').val();let id = $('#id').val();let isTranslationEnabled = $('#is-translation-enabled').val();let isTranslator = $('#is-translator').val();let lang = $('#lang').val();let listedCount = $('#listed-count').val();let location = $('#location').val();let notifications = $('#notifications').val();let profileBackgroundColor = $('#profile-background-color').val();let profileBackgroundImageUrl = $('#profile-background-image-url').val();let profileBackgroundImageUrlHttps = $('#profile-background-image-url-https').val();let profileBackgroundTile = $('#profile-background-tile').val();let profileBannerUrl = $('#profile-banner-url').val();let profileImageUrl = $('#profile-image-url').val();let profileImageUrlHttps = $('#profile-image-url-https').val();let profileLinkColor = $('#profile-link-color').val();let profileSidebarBorderColor = $('#profile-sidebar-border-color').val();let profileSidebarFillColor = $('#profile-sidebar-fill-color').val();let profileTextColor = $('#profile-text-color').val();let profileUseBackgroundImage = $('#profile-use-background-image').val();let protected = $('#protected').val();let screenName = $('#screen-name').val();let statusesCount = $('#statuses-count').val();let timeZone = $('#time-zone').val();let translatorType = $('#translator-type').val();let twitterUserId = $('#twitter-user-id').val();let twitterUserIdStr = $('#twitter-user-id-str').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let url = $('#url').val();let utcOffset = $('#utc-offset').val();let verified = $('#verified').val();let version = $('#version').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-user/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'accountCreatedAt':accountCreatedAt ,'contributorsEnabled':contributorsEnabled ,'createdAt':createdAt ,'createdBy':createdBy ,'defaultProfile':defaultProfile ,'defaultProfileImage':defaultProfileImage ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'description':description ,'displayName':displayName ,'entities':entities ,'favouritesCount':favouritesCount ,'followRequestSent':followRequestSent ,'followersCount':followersCount ,'following':following ,'friendsCount':friendsCount ,'geoEnabled':geoEnabled ,'hasExtendedProfile':hasExtendedProfile ,'id':id ,'isTranslationEnabled':isTranslationEnabled ,'isTranslator':isTranslator ,'lang':lang ,'listedCount':listedCount ,'location':location ,'notifications':notifications ,'profileBackgroundColor':profileBackgroundColor ,'profileBackgroundImageUrl':profileBackgroundImageUrl ,'profileBackgroundImageUrlHttps':profileBackgroundImageUrlHttps ,'profileBackgroundTile':profileBackgroundTile ,'profileBannerUrl':profileBannerUrl ,'profileImageUrl':profileImageUrl ,'profileImageUrlHttps':profileImageUrlHttps ,'profileLinkColor':profileLinkColor ,'profileSidebarBorderColor':profileSidebarBorderColor ,'profileSidebarFillColor':profileSidebarFillColor ,'profileTextColor':profileTextColor ,'profileUseBackgroundImage':profileUseBackgroundImage ,'protected':protected ,'screenName':screenName ,'statusesCount':statusesCount ,'timeZone':timeZone ,'translatorType':translatorType ,'twitterUserId':twitterUserId ,'twitterUserIdStr':twitterUserIdStr ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'url':url ,'utcOffset':utcOffset ,'verified':verified ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_user').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'twitter-user/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('twitter-user/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}