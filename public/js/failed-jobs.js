$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let connection = $('#connection').val();let exception = $('#exception').val();let failedAt = $('#failed-at').val();let id = $('#id').val();let payload = $('#payload').val();let queue = $('#queue').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'failed-jobs/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'connection':connection ,'exception':exception ,'failedAt':failedAt ,'id':id ,'payload':payload ,'queue':queue },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#failed_jobs').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'failed-jobs/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let connection = jqXHRData.connection;let exception = jqXHRData.exception;let failedAt = jqXHRData.failed_at;let id = jqXHRData.id;let payload = jqXHRData.payload;let queue = jqXHRData.queue;

            $('#connection').val(connection);$('#exception').val(exception);$('#failed-at').val(failedAt);$('#id').val(id);$('#payload').val(payload);$('#queue').val(queue);

            $("#failed_jobs").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let connection = $('#connection').val();let exception = $('#exception').val();let failedAt = $('#failed-at').val();let id = $('#id').val();let payload = $('#payload').val();let queue = $('#queue').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'failed-jobs/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'connection':connection ,'exception':exception ,'failedAt':failedAt ,'id':id ,'payload':payload ,'queue':queue },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#failed_jobs').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'failed-jobs/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('failed-jobs/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}