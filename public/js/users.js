$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let createdAt = $('#created-at').val();let email = $('#email').val();let emailVerifiedAt = $('#email-verified-at').val();let id = $('#id').val();let name = $('#name').val();let password = $('#password').val();let rememberToken = $('#remember-token').val();let updatedAt = $('#updated-at').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'users/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'createdAt':createdAt ,'email':email ,'emailVerifiedAt':emailVerifiedAt ,'id':id ,'name':name ,'password':password ,'rememberToken':rememberToken ,'updatedAt':updatedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#users').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'users/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let createdAt = jqXHRData.created_at;let email = jqXHRData.email;let emailVerifiedAt = jqXHRData.email_verified_at;let id = jqXHRData.id;let name = jqXHRData.name;let password = jqXHRData.password;let rememberToken = jqXHRData.remember_token;let updatedAt = jqXHRData.updated_at;

            $('#created-at').val(createdAt);$('#email').val(email);$('#email-verified-at').val(emailVerifiedAt);$('#id').val(id);$('#name').val(name);$('#password').val(password);$('#remember-token').val(rememberToken);$('#updated-at').val(updatedAt);

            $("#users").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let createdAt = $('#created-at').val();let email = $('#email').val();let emailVerifiedAt = $('#email-verified-at').val();let id = $('#id').val();let name = $('#name').val();let password = $('#password').val();let rememberToken = $('#remember-token').val();let updatedAt = $('#updated-at').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'users/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'createdAt':createdAt ,'email':email ,'emailVerifiedAt':emailVerifiedAt ,'id':id ,'name':name ,'password':password ,'rememberToken':rememberToken ,'updatedAt':updatedAt },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#users').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'users/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('users/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}