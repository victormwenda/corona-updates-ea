$(function () {
    localStorage.clear();
});


$('.delete-item').click(onClickDelete);
$('.edit-item').click(onClickEdit);
$('.save-item').click(onClickSave);
$('.update-item').click(onClickUpdate);

/**
* ---------------------------------------------------------------------------------
* --------------------- SAVE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickSave() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let contributors = $('#contributors').val();let coordinates = $('#coordinates').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let favoriteCount = $('#favorite-count').val();let favorited = $('#favorited').val();let id = $('#id').val();let inReplyToScreenName = $('#in-reply-to-screen-name').val();let inReplyToStatusId = $('#in-reply-to-status-id').val();let inReplyToStatusIdStr = $('#in-reply-to-status-id-str').val();let inReplyToUserId = $('#in-reply-to-user-id').val();let inReplyToUserIdStr = $('#in-reply-to-user-id-str').val();let isQuoteStatus = $('#is-quote-status').val();let lang = $('#lang').val();let place = $('#place').val();let possiblySensitive = $('#possibly-sensitive').val();let retweetCount = $('#retweet-count').val();let retweeted = $('#retweeted').val();let retweetedStatus = $('#retweeted-status').val();let tweetCreatedAt = $('#tweet-created-at').val();let tweetEntities = $('#tweet-entities').val();let tweetId = $('#tweet-id').val();let tweetIdStr = $('#tweet-id-str').val();let tweetSource = $('#tweet-source').val();let tweetText = $('#tweet-text').val();let tweetTruncated = $('#tweet-truncated').val();let twitterGeo = $('#twitter-geo').val();let twitterUserId = $('#twitter-user-id').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + id);

    $.ajax({
        url: 'twitter-bot-retweets/store/',
        type: "POST",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'contributors':contributors ,'coordinates':coordinates ,'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'favoriteCount':favoriteCount ,'favorited':favorited ,'id':id ,'inReplyToScreenName':inReplyToScreenName ,'inReplyToStatusId':inReplyToStatusId ,'inReplyToStatusIdStr':inReplyToStatusIdStr ,'inReplyToUserId':inReplyToUserId ,'inReplyToUserIdStr':inReplyToUserIdStr ,'isQuoteStatus':isQuoteStatus ,'lang':lang ,'place':place ,'possiblySensitive':possiblySensitive ,'retweetCount':retweetCount ,'retweeted':retweeted ,'retweetedStatus':retweetedStatus ,'tweetCreatedAt':tweetCreatedAt ,'tweetEntities':tweetEntities ,'tweetId':tweetId ,'tweetIdStr':tweetIdStr ,'tweetSource':tweetSource ,'tweetText':tweetText ,'tweetTruncated':tweetTruncated ,'twitterGeo':twitterGeo ,'twitterUserId':twitterUserId ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_bot_retweets').modal('hide');
             refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- QUERY ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickEdit() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    localStorage.setItem('data-id', data_id);

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-bot-retweets/edit/' +data_id,
        type: "GET",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);

            let jqXHRData = JSON.parse(jqXHR.responseText);

            let contributors = jqXHRData.contributors;let coordinates = jqXHRData.coordinates;let createdAt = jqXHRData.created_at;let createdBy = jqXHRData.created_by;let deletedAt = jqXHRData.deleted_at;let deletedBy = jqXHRData.deleted_by;let favoriteCount = jqXHRData.favorite_count;let favorited = jqXHRData.favorited;let id = jqXHRData.id;let inReplyToScreenName = jqXHRData.in_reply_to_screen_name;let inReplyToStatusId = jqXHRData.in_reply_to_status_id;let inReplyToStatusIdStr = jqXHRData.in_reply_to_status_id_str;let inReplyToUserId = jqXHRData.in_reply_to_user_id;let inReplyToUserIdStr = jqXHRData.in_reply_to_user_id_str;let isQuoteStatus = jqXHRData.is_quote_status;let lang = jqXHRData.lang;let place = jqXHRData.place;let possiblySensitive = jqXHRData.possibly_sensitive;let retweetCount = jqXHRData.retweet_count;let retweeted = jqXHRData.retweeted;let retweetedStatus = jqXHRData.retweeted_status;let tweetCreatedAt = jqXHRData.tweet_created_at;let tweetEntities = jqXHRData.tweet_entities;let tweetId = jqXHRData.tweet_id;let tweetIdStr = jqXHRData.tweet_id_str;let tweetSource = jqXHRData.tweet_source;let tweetText = jqXHRData.tweet_text;let tweetTruncated = jqXHRData.tweet_truncated;let twitterGeo = jqXHRData.twitter_geo;let twitterUserId = jqXHRData.twitter_user_id;let updatedAt = jqXHRData.updated_at;let updatedBy = jqXHRData.updated_by;let version = jqXHRData.version;

            $('#contributors').val(contributors);$('#coordinates').val(coordinates);$('#created-at').val(createdAt);$('#created-by').val(createdBy);$('#deleted-at').val(deletedAt);$('#deleted-by').val(deletedBy);$('#favorite-count').val(favoriteCount);$('#favorited').val(favorited);$('#id').val(id);$('#in-reply-to-screen-name').val(inReplyToScreenName);$('#in-reply-to-status-id').val(inReplyToStatusId);$('#in-reply-to-status-id-str').val(inReplyToStatusIdStr);$('#in-reply-to-user-id').val(inReplyToUserId);$('#in-reply-to-user-id-str').val(inReplyToUserIdStr);$('#is-quote-status').val(isQuoteStatus);$('#lang').val(lang);$('#place').val(place);$('#possibly-sensitive').val(possiblySensitive);$('#retweet-count').val(retweetCount);$('#retweeted').val(retweeted);$('#retweeted-status').val(retweetedStatus);$('#tweet-created-at').val(tweetCreatedAt);$('#tweet-entities').val(tweetEntities);$('#tweet-id').val(tweetId);$('#tweet-id-str').val(tweetIdStr);$('#tweet-source').val(tweetSource);$('#tweet-text').val(tweetText);$('#tweet-truncated').val(tweetTruncated);$('#twitter-geo').val(twitterGeo);$('#twitter-user-id').val(twitterUserId);$('#updated-at').val(updatedAt);$('#updated-by').val(updatedBy);$('#version').val(version);

            $("#twitter_bot_retweets").modal()
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
    }
});

}


/**
* ---------------------------------------------------------------------------------
* --------------------- UPDATE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickUpdate() {
    var tr = $(this).closest("tr");
    var data_id = localStorage.getItem('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    let contributors = $('#contributors').val();let coordinates = $('#coordinates').val();let createdAt = $('#created-at').val();let createdBy = $('#created-by').val();let deletedAt = $('#deleted-at').val();let deletedBy = $('#deleted-by').val();let favoriteCount = $('#favorite-count').val();let favorited = $('#favorited').val();let id = $('#id').val();let inReplyToScreenName = $('#in-reply-to-screen-name').val();let inReplyToStatusId = $('#in-reply-to-status-id').val();let inReplyToStatusIdStr = $('#in-reply-to-status-id-str').val();let inReplyToUserId = $('#in-reply-to-user-id').val();let inReplyToUserIdStr = $('#in-reply-to-user-id-str').val();let isQuoteStatus = $('#is-quote-status').val();let lang = $('#lang').val();let place = $('#place').val();let possiblySensitive = $('#possibly-sensitive').val();let retweetCount = $('#retweet-count').val();let retweeted = $('#retweeted').val();let retweetedStatus = $('#retweeted-status').val();let tweetCreatedAt = $('#tweet-created-at').val();let tweetEntities = $('#tweet-entities').val();let tweetId = $('#tweet-id').val();let tweetIdStr = $('#tweet-id-str').val();let tweetSource = $('#tweet-source').val();let tweetText = $('#tweet-text').val();let tweetTruncated = $('#tweet-truncated').val();let twitterGeo = $('#twitter-geo').val();let twitterUserId = $('#twitter-user-id').val();let updatedAt = $('#updated-at').val();let updatedBy = $('#updated-by').val();let version = $('#version').val();

    console.log("clicked edit " + data_id);

    $.ajax({
        url: 'twitter-bot-retweets/update/' +data_id,
        type: "PUT",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: {'contributors':contributors ,'coordinates':coordinates ,'createdAt':createdAt ,'createdBy':createdBy ,'deletedAt':deletedAt ,'deletedBy':deletedBy ,'favoriteCount':favoriteCount ,'favorited':favorited ,'id':id ,'inReplyToScreenName':inReplyToScreenName ,'inReplyToStatusId':inReplyToStatusId ,'inReplyToStatusIdStr':inReplyToStatusIdStr ,'inReplyToUserId':inReplyToUserId ,'inReplyToUserIdStr':inReplyToUserIdStr ,'isQuoteStatus':isQuoteStatus ,'lang':lang ,'place':place ,'possiblySensitive':possiblySensitive ,'retweetCount':retweetCount ,'retweeted':retweeted ,'retweetedStatus':retweetedStatus ,'tweetCreatedAt':tweetCreatedAt ,'tweetEntities':tweetEntities ,'tweetId':tweetId ,'tweetIdStr':tweetIdStr ,'tweetSource':tweetSource ,'tweetText':tweetText ,'tweetTruncated':tweetTruncated ,'twitterGeo':twitterGeo ,'twitterUserId':twitterUserId ,'updatedAt':updatedAt ,'updatedBy':updatedBy ,'version':version },
        success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            $('#twitter_bot_retweets').modal('hide');
            refreshTableItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });

}

/**
* ---------------------------------------------------------------------------------
* --------------------- DELETE ITEM -----------------------------------------------
* ---------------------------------------------------------------------------------
*/
function onClickDelete() {
    var tr = $(this).closest("tr");
    var data_id = $(this).attr('data-id');
    var csrfToken = $("meta[name='csrf-token']").attr("content");

    $.ajax({
        url: 'twitter-bot-retweets/destroy/' +data_id,
        type: "DELETE",
        headers: {"X-CSRF-TOKEN": csrfToken},
        data: null,
            success: function (data, textStatus, jqXHR) {
            console.log(jqXHR);
            tr.remove();
        },
        error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        }
    });

    console.log("clicked delete " + id);
}


function refreshTableItems() {

    $(".table-container").load('twitter-bot-retweets/refresh', function () {
        $('.delete-item').click(onClickDelete);
        $('.edit-item').click(onClickEdit);
    });
}